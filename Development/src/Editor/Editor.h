#include "../Include/XSystemEngine.h"
#include "FilePanel.h"
using namespace XSystemEngine;

/*
File panel
*/
class MaterialEditor {
public:
	FilePanel();
	void events();

	WidgetButton *openButton;
	WidgetButton *saveButton;
	WidgetButton *exitButton;

	String openPath;
	String savePath;
};

/*
File panel
*/
class FilePanel {
public:
	FilePanel();
	void events();

	WidgetButton *openButton;
	WidgetButton *saveButton;
	WidgetButton *exitButton;

	String openPath;
	String savePath;
};

/*
Main editor class
*/
class Editor {
public:
	/*
	Properties window
	*/
	WidgetWindow *propertiesWindow;
	
	/*
	*/
	FilePanel *filePanel;

	/*
	*/
	Editor::Editor();

	/*
	*/
	void events();

	/*
	*/
	void openScene();

	/*
	*/
	void saveScene();

	TCHAR currentDir[_MAX_PATH];
};

extern Editor *editor;
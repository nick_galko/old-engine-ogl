#include "Editor.h"
#include "FilePanel.h"

using namespace XSystemEngine;

Editor *editor;

/*
*/
Editor::Editor() {
	WindowSystem::Get()->setTitle("XSystem editor");
	
	//initializing loading screen
	LoadingScreen *lscreen = LoadingScreen::Create("data/textures/logos/background.jpg");
	lscreen->show();

	//initialize GUI
	GUI::Create("editor_media");
	
	GUI::Get()->setAlpha(0.8);
		
	GUI::Get()->addWidget(propertiesWindow = WidgetWindow::Create());
	propertiesWindow->setPosition(0, 100);
	propertiesWindow->setSize(200, 800);
	propertiesWindow->setMoveable(false);

	filePanel = new FilePanel();
};

/*
*/
void Editor::events() {
	filePanel->events();

	if(WindowSystem::Get()->isKeyUp(WindowSystem::KEY_ESC)) {
		Engine::Get()->quit();
	};
};

/*
*/
void Editor::openScene() {
};

/*
*/
void Editor::saveScene() {
};



/*
File panel
*/
FilePanel::FilePanel() {
	GUI::Get()->addWidget(openButton = WidgetButton::Create("Open"));
	openButton->setPosition(0, 0);

	GUI::Get()->addWidget(saveButton = WidgetButton::Create("Save"));
	saveButton->setPosition(90, 0);

	GUI::Get()->addWidget(exitButton = WidgetButton::Create("Exit"));
	exitButton->setPosition(180, 0);
};

void FilePanel::events() {
	if(exitButton->isClicked()) {
		Engine::Get()->quit();
	};

	if(openButton->isClicked()) {
		GUI::Get()->openFileDialog("*.xs", "Open scene file:", openPath);
		SetCurrentDirectory(editor->currentDir);
	};

	if(saveButton->isClicked()) {
		GUI::Get()->openFileDialog(".xs", "Save scene file:", savePath);
		SetCurrentDirectory(editor->currentDir);
	};
};

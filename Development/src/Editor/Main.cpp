#include "Editor.h"
#pragma comment(lib, "../Lib/XSystemEngine.lib")

using namespace XSystemEngine;

void events() { editor->events(); };

int main(int argc, char **argv) {
	Engine *engine = Engine::Create();
	
	editor = new Editor();

	GetCurrentDirectory(sizeof(TCHAR)*_MAX_PATH, editor->currentDir);

	engine->eventsCallback(events);
	engine->mainLoop();

	engine->Destroy();

	return 0;
};

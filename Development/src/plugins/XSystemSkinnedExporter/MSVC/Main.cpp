/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#include <max.h>
#include <stdmat.h>
#include <vector>
#include <iparamm2.h>
#include <modstack.h>
#include <iskin.h>
#include "String.h"

#pragma comment(lib, "core.lib") 
#pragma comment(lib, "maxutil.lib")
#pragma comment(lib, "geom.lib")
#pragma comment(lib, "mesh.lib")
#pragma comment(lib, "comctl32.lib")

struct Weight {
	int bone;
	float weight;
	Point3 position;
	Point3 normal;
};

struct Vertex {
	Point3 position;
	Point3 normal;
	Point2 texcoord;

	int numWeights;
	Weight weights[32];

	int id;
};

struct Subset {
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
	String name;

	Point3 min;
	Point3 max;

	INode *node;
};

/*
XSystem ENUM PROC
*/
class XSystemEnumProc : public ITreeEnumProc {
public:
	XSystemEnumProc(Interface *i) { iface = i; };

	int callback(INode *node);
	void exportAll(const char *name);

	TriObject *NODE2OBJ(INode *node, int &deleteIt);

	Interface *iface;
	bool selected;

	std::vector<INode*> bones;
	std::vector<INode*> nodes;
	std::vector<Modifier*> modifiers;
	std::vector<Subset> subsets;
};

/*
MAIN EXPORT
*/
void XSystemEnumProc::exportAll(const char *name) {
	/*
	Get model bones
	*/
	for(int i = 0; i < (int)modifiers.size(); i++) {
		ISkin *skin = (ISkin*)modifiers[i]->GetInterface(I_SKIN);
	
		for(int i = 0; i < skin->GetNumBones(); i++) {
			INode *bone = skin->GetBone(i);
			int j;
			for(j = 0; j < (int)bones.size(); j++) {
				if(bones[j] == bone) break;	
			};

			if(j != bones.size()) continue;
			bones.push_back(bone);
		};
	};
		
	/*
	Get number of subsets
	*/
	int numObjects = 0;
	for(int n = 0; n < nodes.size(); n++) {
		INode *node = nodes[n];
		int d; TriObject *object = NODE2OBJ(node, d); 
		if (!object) continue;
		numObjects++;
	};
	
	/*
	process subsets
	*/
	for(int n = 0; n < nodes.size(); n++) {
		INode *node = nodes[n];

		Subset subset;
		subset.node = node;	

		subset.name = strlwr(node->GetName());
		replaceSpaces(subset.name);

		int d; 
		TriObject *object = NODE2OBJ(node, d); 
		if (!object) continue;

		ISkin *skin = (ISkin*)modifiers[n]->GetInterface(I_SKIN);
		ISkinContextData *skinData = skin->GetContextInterface(nodes[n]);

		//read model data
		Matrix3 tm = node->GetObjTMAfterWSM(iface->GetTime());
		Matrix3 nm = tm;
		nm.NoScale();
		nm.NoTrans();

		/*Get position*/
		Box3 box;
		object->mesh.buildBoundingBox();
		box = object->mesh.getBoundingBox(&tm);
		subset.min = box.Min();
		subset.max = box.Max();

		object->mesh.buildNormals();
			
		for(int i = 0; i < object->mesh.numFaces; i++) {
			Face *f = &object->mesh.faces[i];
			TVFace *tf = &object->mesh.tvFace[i];

			for(int j = 0; j < 3; j++) {
				Vertex vertex;

				Point3 v = tm * object->mesh.verts[f->v[j]];

				vertex.position.x = v.x;
				vertex.position.y = v.y;
				vertex.position.z = v.z;
				
				vertex.id = f->v[j];

				vertex.texcoord.x = object->mesh.tVerts[tf->t[j]].x;
				vertex.texcoord.y = object->mesh.tVerts[tf->t[j]].y;

				//read normal
				bool specifiedNormal = true;

				RVertex *rv = object->mesh.getRVertPtr(f->v[j]);
				int nnormals;
				
				if(rv->rFlags & SPECIFIED_NORMAL) {
					 Point3 normal = nm * rv->rn.getNormal();
					 vertex.normal.x = normal.x;
					 vertex.normal.y = normal.y;
					 vertex.normal.z = normal.z;
				} else {
					if((nnormals = rv->rFlags & NORCT_MASK) && f->smGroup) {
						if(nnormals == 1) {
							Point3 normal = nm * rv->rn.getNormal();
							vertex.normal.x = normal.x;
							vertex.normal.y = normal.y;
							vertex.normal.z = normal.z;
						} else {
							for(int l = 0; l < nnormals; l++) {
								if(rv->ern[l].getSmGroup() & f->smGroup) {
									Point3 normal = nm * rv->ern[l].getNormal();
									vertex.normal.x = normal.x;
									vertex.normal.y = normal.y;
									vertex.normal.z = normal.z;
								};
							};
						};
					} else {
						specifiedNormal = false;
						Point3 normal = nm * object->mesh.getFaceNormal(i);
						vertex.normal.x = normal.x;
						vertex.normal.y = normal.y;
						vertex.normal.z = normal.z;
					};
				};
			
				//make indices
				int index = -1;
				for(int k = 0; k < subset.vertices.size(); k++) {
					if((subset.vertices[k].position == vertex.position) &&
						(subset.vertices[k].texcoord == vertex.texcoord)) {
							if(specifiedNormal) {
								if(subset.vertices[k].normal == vertex.normal) {
									index = k;
								};
							} else {
								index = k;
								subset.vertices[k].normal += vertex.normal;
							};
					};
				};

				if(index > -1) {
					subset.indices.push_back(index);
				} else {
					subset.indices.push_back(subset.vertices.size());
					subset.vertices.push_back(vertex);
				};
			};
		};

		//normalize normals
		for(int k = 0; k < subset.vertices.size(); k++) {
			subset.vertices[k].normal = subset.vertices[k].normal.Normalize();
		};

		//process weights
		for(int j = 0; j < subset.vertices.size(); j++) {
			Vertex &vert = subset.vertices[j];

			int v = vert.id;
			vert.numWeights = skinData->GetNumAssignedBones(v);

			for(int k = 0; k < vert.numWeights; k++) {
				INode *bone = skin->GetBone(skinData->GetAssignedBone(v, k));
				
				int l;
				for(l = 0; l < (int)bones.size(); l++) {
					if(bones[l] == bone) break;
				};
				vert.weights[k].bone = l;
				vert.weights[k].weight = skinData->GetBoneWeight(v, k);

				Matrix3 transform = bone->GetObjTMAfterWSM(iface->GetTime());
				if(transform.Parity()) {
					transform.Invert();
					Matrix3 m;
					m = transform;
					transform.Zero();
					transform -= m;
				} else {
					transform.Invert();
				};

				Matrix3 rotate = transform;
				rotate.Orthogonalize();
				rotate.NoScale();
				rotate.NoTrans();

				vert.weights[k].position = transform * vert.position;
				vert.weights[k].normal = rotate * vert.normal;
			};
		};

		subsets.push_back(subset);
	};
	
	//get position
	Point3 pos;
	Point3 smin = subsets[0].min;
	Point3 smax = subsets[0].max;
	
	for(int s = 1; s < subsets.size(); s++) {
		smin.x = min(smin.x, subsets[0].min.x);
		smin.y = min(smin.y, subsets[0].min.y);
		smin.z = min(smin.z, subsets[0].min.z);

		smax.x = max(smax.x, subsets[0].max.x);
		smax.y = max(smax.y, subsets[0].max.y);
		smax.z = max(smax.z, subsets[0].max.z);
	};

	pos = (smin + smax);
	pos.x = pos.x * 0.5;
	pos.y = pos.y * 0.5;
	pos.z = pos.z * 0.5;

	/*
	open material list file for writing
	*/
	String ml = String((char*)name);
	cutFileExt(ml);
	ml = ml + ".xsmtrlst";
	FILE *fmtrlst = fopen(ml.c_str(), "wt");

	/*
	Save skinned mesh
	*/
	FILE *fmdl = fopen(name, "wt");
	fprintf(fmdl, "#XSYSTEM_ENGINE_SKINNED_MESH\n\n");

	//write bones
	fprintf(fmdl, "num_bones %i\n", bones.size());

	for(int i = 0; i < (int)bones.size(); i++) {
		INode *parent = bones[i]->GetParentNode();
		int j;
		for(j = 0; j < (int)bones.size(); j++) {
			if(bones[j] == parent) break;
		};
		String boneName = bones[i]->GetName();
		replaceSpaces(boneName);

		if(j == bones.size()) {
			fprintf(fmdl, "\"%s\" -1\n", boneName.c_str());
		} else {
			fprintf(fmdl, "\"%s\" %i\n", boneName.c_str(), j);
		};
	};
	fprintf(fmdl, "\n");

	//write number of subsets
	fprintf(fmdl, "num_subsets %i\n\n", subsets.size());

	for(int s = 0; s < subsets.size(); s++) {
		Subset &st = subsets[s];

		fprintf(fmdl, "subset \"%s\"\n", st.name.c_str());
	
		fprintf(fmdl, "num_vertices %i\n", st.vertices.size());
				
		for(int v = 0; v < st.vertices.size(); v++)	{
			fprintf(fmdl, "num_weights %i\n", st.vertices[v].numWeights);

			for(int w = 0; w < st.vertices[v].numWeights; w++)	{
				fprintf(fmdl, "%i %f %f %f %f %f %f %f\n", 
					st.vertices[v].weights[w].bone,
					st.vertices[v].weights[w].weight,
					st.vertices[v].weights[w].position.x - pos.x,
					st.vertices[v].weights[w].position.z - pos.z, 
					st.vertices[v].weights[w].position.y - pos.y ,
					-st.vertices[v].weights[w].normal.x,
					-st.vertices[v].weights[w].normal.z,
					-st.vertices[v].weights[w].normal.y);
			};
			fprintf(fmdl, "%f %f\n",
					st.vertices[v].texcoord.x,
					1.0 - st.vertices[v].texcoord.y);
		};

		fprintf(fmdl, "num_faces %i\n", st.indices.size() / 3); 
		for(int f = 0; f <  st.indices.size() / 3; f++)
		{
			fprintf(fmdl, "%i %i %i\n", 
				st.indices[3*f+0],
				st.indices[3*f+2],
				st.indices[3*f+1]);
		};

		fprintf(fmdl, "\n");

		//Save materials
		Mtl *m = st.node->GetMtl();
		if(!m || m->ClassID() != Class_ID(DMTL_CLASS_ID, 0)) {
		} else {
			String tex0;
			Texmap *tmap = m->GetSubTexmap(ID_DI);

			if (!tmap || tmap->ClassID() != Class_ID(BMTEX_CLASS_ID, 0)) { 
				tex0 = "";
			} else {
				BitmapTex *bmt = (BitmapTex *)tmap;
				tex0 = strlwr((char *)bmt->GetMapName()); 
				getFileName(tex0);
				replaceSpaces(tex0);
				
				FILE *fmtr = fopen(String(st.name + ".xsmtr").c_str(), "wt");
				
				fprintf(fmtr, "ambient_shader \"data/shaders/mesh_ambient.xsshd\"\n");
				fprintf(fmtr, "point_shader \"data/shaders/mesh_point.xsshd\"\n");
				fprintf(fmtr, "spot_shader \"data/shaders/mesh_spot.xsshd\"\n");
				fprintf(fmtr, "direct_shader \"data/shaders/mesh_direct.xsshd\"\n");
			
				fprintf(fmtr, "texture_0 2D \"data/textures/%s\"\n", tex0.c_str());
				fprintf(fmtr, "texture_1 2D normal_map \"data/textures/%s\"\n", tex0.c_str());
				
				fprintf(fmtr, "param_0 0.5 24 1");

				fclose(fmtr);
			};
		};

		//Save material_list
		fprintf(fmtrlst, "\n%s %s\n\n", st.name.c_str(), String(st.name + ".xsmtr").c_str());
	};

	/*
	process and save animation frames to file
	*/
	Interval interval = iface->GetAnimRange();
	int startTime = interval.Start();
	int endTime = interval.End();
	int ticksPerFrame = GetTicksPerFrame();
	
	fprintf(fmdl, "num_frames %i\n", (endTime - startTime) / ticksPerFrame);

	for(int time = startTime; time < endTime; time += ticksPerFrame) {
		for(int i = 0; i < (int)bones.size(); i++) {
			INode *bone = bones[i];
			Matrix3 transform = bone->GetObjTMAfterWSM(time);
			Point3 xyz = Point3(0,0,0) * transform;
			transform.Orthogonalize();
			transform.NoScale();
			transform.NoTrans();
			transform.Invert();
			Quat rot(transform);
			fprintf(fmdl, "%f %f %f %f %f %f %f\n",
				xyz.x - pos.x,
				xyz.z - pos.z,
				xyz.y - pos.y,
				-rot.x,
				-rot.z,
				-rot.y,
				rot.w);
		};
	};

	fclose(fmdl);
	fclose(fmtrlst);
};

/*
CALLBACK
*/
int XSystemEnumProc::callback(INode *node) {
	if(selected && node->Selected() == FALSE) return TREE_CONTINUE;

	Object *obj = node->GetObjectRef();
	if(!obj) return TREE_CONTINUE;
	if(obj->SuperClassID() != GEN_DERIVOB_CLASS_ID) return TREE_CONTINUE;
	
	IDerivedObject *dobj = static_cast<IDerivedObject*>(obj);
	for(int i = 0; i < dobj->NumModifiers(); i++) {
		Modifier *modifier = dobj->GetModifier(i);
		if(modifier->ClassID() == SKIN_CLASSID) {
			nodes.push_back(node);
			modifiers.push_back(modifier);
			return TREE_CONTINUE;
		};
	};

	return TREE_CONTINUE;
};

/*
CONVERTS NODE TO TRIOBJ
*/
TriObject *XSystemEnumProc::NODE2OBJ(INode *node, int &deleteIt) {
	deleteIt = false;
	Object *obj = node->EvalWorldState(iface->GetTime()).obj;
	if (obj->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID,0)))
	{
		TriObject *tri = (TriObject *) obj->ConvertToType(iface->GetTime(), 
			Class_ID(TRIOBJ_CLASS_ID, 0));
		if (obj != tri) deleteIt = true;
		return tri;
	}
	else return NULL;
};


/*
XSystemEXPORTER
*/
class XSystemExporter : public SceneExport {
public:
	 int          ExtCount()   { return 1; };
	 const TCHAR* Ext(int i)   { if (i == 0) return _T("xssmsh"); else return _T(""); };
	 const TCHAR* LongDesc()   { return _T("XSystemEngine skinned mesh exporter"); };
	 const TCHAR* ShortDesc()  { return _T("XSystemSkinnedExporter"); };
	 const TCHAR* AuthorName() { return _T("Tsyplyaev Alexander"); };
	 const TCHAR* CopyrightMessage() { return _T("Copyright (C) 2007"); };
	 const TCHAR* OtherMessage1()    { return _T(""); };
	 const TCHAR* OtherMessage2()    { return _T(""); };
	 unsigned int Version()          { return 0.1; };

	 void ShowAbout(HWND hWnd) { MessageBox(hWnd, "About", "XSystemEngine skinned mesh exporter", MB_OK); }
	 BOOL SupportsOptions(int ext, DWORD options) { return (options == SCENE_EXPORT_SELECTED) ? TRUE : FALSE; }

	 int DoExport(const TCHAR *name, ExpInterface *ei, Interface *i, BOOL suppressPromts = FALSE, DWORD options = 0);
	 
	 XSystemExporter() {};
	 virtual ~XSystemExporter() {};
};

/*
DO EXPORT
*/
int XSystemExporter::DoExport(const TCHAR *name, ExpInterface *ei, Interface *i, BOOL suppressPromts, DWORD options) {
	XSystemEnumProc xProc(i);
	xProc.selected = (options & SCENE_EXPORT_SELECTED) ? TRUE : FALSE;

	ei->theScene->EnumTree(&xProc);

	xProc.exportAll(name);

	MessageBox(0, "Skinned mesh was succesfully exported!", "Succses!", MB_OK);
	return 1;
};

/*
plugin entry
*/
HINSTANCE hInstance;
int controlsInit = FALSE;

BOOL WINAPI DllMain(HINSTANCE hinstDLL, ULONG fdwReason, LPVOID lpvReserved) {
	hInstance = hinstDLL;
	if(!controlsInit) {
		controlsInit = TRUE;
		InitCustomControls(hInstance);
		InitCommonControls();
	};
	return TRUE;
};

/*
Class description
*/
class XSystemClassDesc : public ClassDesc {
public:
	int IsPublic() { return 1; }
	void *Create(BOOL loading = FALSE) { return new XSystemExporter; };

	const TCHAR *ClassName() { return "XSystemEngine mesh exporter"; };
	SClass_ID SuperClassID() { return SCENE_EXPORT_CLASS_ID; };
	Class_ID ClassID()       { return Class_ID(0x150c16c4, 0x51732a4f); };
	const TCHAR *Category()  { return ""; };
};

/*
XSystemClassDesc
*/
static XSystemClassDesc xsDesc;

/*
Number of plugins
*/
__declspec(dllexport) int LibNumberClasses() {
	return 1;
};

/*
LibClassDesc
*/
__declspec(dllexport) ClassDesc *LibClassDesc(int i) {
	return (i == 0) ? &xsDesc : 0;
};

/*
LibDesc
*/
__declspec(dllexport) const TCHAR *LibDescription() {
	return _T("XSystem engine export plugin");
};

/*
LibVersion
*/
__declspec(dllexport) ULONG LibVersion() {
	return VERSION_3DSMAX;
};

/*
*/
__declspec(dllexport )ULONG CanAutoDefer() {
	return 1;
};


cd Bin\
del *.lib
del *.exp
del *.pdb
cd ..\MSVC\
erase /q Release
rd Release
erase /q debug
rd Debug
del *.opt
del *.plg
del *.log
del *.ncb
del *.suo
del *.ilk
del *.user
del *.aps
pause
/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//***************************************************************************
#include "EngineAPI.h"
//***************************************************************************

namespace Vega {

	/*
	*/
	ALSoundSource *ALSoundSource::Create(ALSound *sound) {
		ALSoundSource *source = new ALSoundSource();

		alGenSources(1, &source->alID);
		alSourcei(source->alID, AL_BUFFER, sound->buffID);
		alSourcef(source->alID, AL_MIN_GAIN, 0.0f);
		alSourcef(source->alID, AL_MAX_GAIN, 1.0f);

		return source;
	}

	/*
	*/
	void ALSoundSource::Destroy() {
		alDeleteSources(1, &alID);
		delete this;
	}

	/*
	*/
	void ALSoundSource::play() {
		alSourcePlay(alID);
	}

	/*
	*/
	void ALSoundSource::stop() {
		alSourceStop(alID);
	}

	/*
	*/
	void ALSoundSource::pause() {
		alSourcePause(alID);
	}

	/*
	*/
	bool ALSoundSource::isPlaying() {
		ALint state;
		alGetSourcei(alID, AL_SOURCE_STATE, &state);
		return (state == AL_PLAYING);
	}

	/*
	*/
	void ALSoundSource::setLooping(bool loop) {
		alSourcei(alID, AL_LOOPING, loop);
	}

	/*
	*/
	void ALSoundSource::setRelative(bool relative) {
		alSourcei(alID, AL_SOURCE_RELATIVE, relative);
	}

	/*
	*/
	void ALSoundSource::setGain(float gain) {
		alSourcef(alID, AL_GAIN, gain);
	}

	/*
	*/
	void ALSoundSource::setPosition(const Vec3 &position) {
		alSourcefv(alID, AL_POSITION, position);
	}

	/*
	*/
	void ALSoundSource::setRolloffFactor(float rolloffFactor) {
		alSourcef(alID, AL_ROLLOFF_FACTOR, rolloffFactor);
	}

	/*
	*/
	void ALSoundSource::setReferenceDistance(float referenceDistance) {
		alSourcef(alID, AL_REFERENCE_DISTANCE, referenceDistance);
	}

	/*
	*/
	void ALSoundSource::setMaxDistance(float maxDistance) {
		;
		alSourcef(alID, AL_MAX_DISTANCE, maxDistance);
	}

}

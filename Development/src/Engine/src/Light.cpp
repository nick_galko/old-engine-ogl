/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//**************************************
#include "EngineAPI.h"
//**************************************

namespace Vega {

	//**************************************************************************
	//Light
	//**************************************************************************
	/*
	*/
	void Light::setEnabled(bool enabled) {
		this->enabled = enabled;
	}

	/*
	*/
	bool Light::getEnabled() {
		return enabled;
	}

	//**************************************************************************
	//LightPoint
	//**************************************************************************
	/*
	*/
	LightPoint *LightPoint::Create() {
		LightPoint *light = new LightPoint();
		light->initNode();

		light->color = Vec3(1, 1, 1);
		light->radius = 200;

		light->castShadows = true;

		int size = Config::Get()->getInt(CONF_SHADOW_SIZE);
		light->shadowMap = GLTexture::CreateCube(size, size, GLTexture::RGBA);
		light->shadowMap->setFilter(GLTexture::LINEAR);
		light->shadowMap->setWrap(GLTexture::CLAMP_TO_EDGE);

		light->enabled = true;
		Scene::Get()->addLight(light);
		return light;
	}

	/*
	*/
	void LightPoint::Destroy() {
		shadowMap->Destroy();
		delete this;
	}

	/*
	*/
	const Vec3 &LightPoint::getColor() {
		return color;
	}

	/*
	*/
	void LightPoint::setColor(const Vec3 &color) {
		this->color = color;
	}

	/*
	*/
	float LightPoint::getRadius() {
		return radius;
	}

	/*
	*/
	float LightPoint::getIRadius() {
		return 1 / radius;
	}

	/*
	*/
	void LightPoint::setRadius(float radius) {
		this->radius = radius;
	}

	/*
	*/
	bool LightPoint::getShadows() {
		return castShadows;
	}

	/*
	*/
	void LightPoint::setShadows(bool shadows) {
		castShadows = shadows;
	}

	/*
	*/
	bool LightPoint::isVisible() {
		Frustum *frustum = Scene::Get()->frustum;
		Camera *camera = Scene::Get()->camera;
		GLOcclusionQuery *query = Scene::Get()->query;
		Mesh *sphereMesh = Scene::Get()->sphereMesh;

		frustum->Get();
		if (!frustum->isInside(getPosition(), getRadius())) {
			return false;
		}

		if ((getPosition() - camera->getPosition()).length() > getRadius()) {
			GLSystem::Get()->colorMask(false, false, false, false);
			GLSystem::Get()->depthMask(false);

			query->beginRendering();

			GLSystem::Get()->push();
			GLSystem::Get()->multMatrix(Mat4::translate(getPosition()) *
				Mat4::scale(Vec3(getRadius(), getRadius(), getRadius())));

			sphereMesh->drawSubset(0);

			GLSystem::Get()->pop();

			query->endRendering();

			GLSystem::Get()->depthMask(true);
			GLSystem::Get()->colorMask(true, true, true, true);

			return (query->getResult() > 2);
		}
		return true;
	}

	/*
	*/
	GLTexture *LightPoint::getShadowMap() {
		return shadowMap;
	}



	//**************************************************************************
	//LightSpot
	//**************************************************************************
	/*
	*/
	LightSpot *LightSpot::Create() {
		LightSpot *light = new LightSpot();
		light->initNode();

		light->color = Vec3(1, 1, 1);
		light->radius = 200;
		light->fov = 60;

		light->castShadows = true;

		int size = Config::Get()->getInt(CONF_SHADOW_SIZE);

		light->shadowMap = GLTexture::Create2d(size, size, GLTexture::RGBA);
		light->shadowMap->setFilter(GLTexture::LINEAR);
		light->shadowMap->setWrap(GLTexture::CLAMP);

		light->projMap = GLTexture::Create2d("../gamedata/textures/engine_textures/spot.jpg");
		light->projMap->setWrap(GLTexture::CLAMP);

		light->enabled = true;

		Scene::Get()->addLight(light);
		return light;
	}

	/*
	*/
	void LightSpot::Destroy() {
		shadowMap->Destroy();
		projMap->Destroy();
		delete this;
	}

	/*
	*/
	const Vec3 &LightSpot::getColor() {
		return color;
	}

	/*
	*/
	void LightSpot::setColor(const Vec3 &color) {
		this->color = color;
	}

	/*
	*/
	float LightSpot::getRadius() {
		return radius;
	}

	/*
	*/
	float LightSpot::getIRadius() {
		return 1 / radius;
	}

	/*
	*/
	void LightSpot::setRadius(float radius) {
		this->radius = radius;
	}

	/*
	*/
	float LightSpot::getFOV() {
		return fov;
	}

	/*
	*/
	void LightSpot::setFOV(float fov) {
		this->fov = fov;
	}

	/*
	*/
	bool LightSpot::getShadows() {
		return castShadows;
	}

	/*
	*/
	void LightSpot::setShadows(bool shadows) {
		castShadows = shadows;
	}

	/*
	*/
	bool LightSpot::isVisible() {
		Frustum *frustum = Scene::Get()->frustum;
		Camera *camera = Scene::Get()->camera;
		GLOcclusionQuery *query = Scene::Get()->query;
		Mesh *sphereMesh = Scene::Get()->sphereMesh;

		frustum->Get();
		if (!frustum->isInside(getPosition(), getRadius())) {
			return false;
		}

		if ((getPosition() - camera->getPosition()).length() > getRadius()) {
			GLSystem::Get()->colorMask(false, false, false, false);
			GLSystem::Get()->depthMask(false);

			query->beginRendering();

			GLSystem::Get()->push();
			GLSystem::Get()->multMatrix(Mat4::translate(getPosition()) *
				Mat4::scale(Vec3(getRadius(), getRadius(), getRadius())));

			sphereMesh->drawSubset(0);

			GLSystem::Get()->pop();

			query->endRendering();

			GLSystem::Get()->depthMask(true);
			GLSystem::Get()->colorMask(true, true, true, true);

			return (query->getResult() > 2);
		}
		return true;
	}

	/*
	*/
	GLTexture *LightSpot::getShadowMap() {
		return shadowMap;
	}

	/*
	*/
	GLTexture *LightSpot::getSpotMap() {
		return projMap;
	}




	//**************************************************************************
	//LightDirect
	//**************************************************************************
	/*
	*/
	LightDirect *LightDirect::Create() {
		LightDirect *light = new LightDirect();
		light->initNode();

		light->color = Vec3(1, 1, 1);

		light->enabled = true;
		Scene::Get()->addLight(light);
		return light;
	}

	/*
	*/
	void LightDirect::Destroy() {
		delete this;
	}

	/*
	*/
	const Vec3 &LightDirect::getColor() {
		return color;
	}

	/*
	*/
	void LightDirect::setColor(const Vec3 &color) {
		this->color = color;
	}

	/*
	*/
	bool LightDirect::isVisible() {
		return true;
	}
}
/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/
#include "EngineAPI.h"
//***************************************************************************

#ifdef WIN32
#pragma comment(lib, "src/XSLibs/OpenAL32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "src/XSLibs/ilu.lib")
#pragma comment(lib, "src/XSLibs/devil.lib")
#pragma comment(lib, "Common.lib")
#endif
//***************************************************
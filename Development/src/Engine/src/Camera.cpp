/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//**************************************
#include "EngineAPI.h"
//**************************************

namespace Vega {

	//***************************************************************************
	//Camera class
	//***************************************************************************
	/*
	*/
	float Camera::getFOV() {
		return fov;
	}

	/*
	*/
	void Camera::setFOV(float fov) {
		buildProjection();
		this->fov = fov;
	}

	/*
	*/
	float Camera::getAspect() {
		return aspect;
	}

	/*
	*/
	void Camera::setAspect(float aspect) {
		buildProjection();
		this->aspect = aspect;
	}

	/*
	*/
	float Camera::getZNear() {
		return zNear;
	}

	/*
	*/
	void Camera::setZNear(float znear) {
		buildProjection();
		this->zNear = znear;
	}

	/*
	*/
	float Camera::getZFar() {
		return zFar;
	}

	/*
	*/
	void Camera::setZFar(float zfar) {
		buildProjection();
		this->zFar = zfar;
	}

	/*
	*/
	const Mat4 &Camera::getView() {
		return view;
	}

	/*
	*/
	void Camera::setView(const Mat4 &view) {
		this->view = view;
	}

	/*
	*/
	const Mat4 &Camera::getProjection() {
		return projection;
	}

	/*
	*/
	void Camera::setProjection(const Mat4 &projection) {
		this->projection = projection;
	}

	/*
	*/
	PhysBody *Camera::getPhysBody() {
		return NULL;
	}

	/*
	*/
	void Camera::buildProjection() {
		projection = Mat4::perspective(fov, aspect, zNear, zFar);
	}

	/*
	*/
	void Camera::update() {
	}

	//***************************************************************************
	//FPS Camera class ENGINE_API
	//***************************************************************************
	/*
	*/
	CameraFPS *CameraFPS::Create(bool _activate) {
		CameraFPS *camera = new CameraFPS();

		camera->initNode();

		camera->position = Vec3(0.0, 0.0, 0.0);
		camera->maxVelocity = 1500.0;

		camera->fov = 60;
		camera->aspect = 4.0 / 3.0;
		camera->zNear = 0.1;
		camera->zFar = 1e4;
		camera->buildProjection();

		camera->pBody = NULL;
		if (_activate)
			Scene::Get()->setCamera(camera);
		return camera;
	}

	/*
	*/
	void CameraFPS::Destroy() {
		SAFE_DELETE(pBody);
		SAFE_DELETE(pJoint);
		delete this;
	}

	/*
	*/
	float CameraFPS::getMaxVelocity() {
		return maxVelocity;
	}

	/*
	*/
	void CameraFPS::setMaxVelocity(float maxVelocity) {
		this->maxVelocity = maxVelocity;
	}

	/*
	*/
	void CameraFPS::setPhysics(const Vec3 &size, float mass) {
		pBody = PhysBody::CreateCapsule(size.x,size.y, mass);
		pBody->setTransform(getTransform());
		this->size = size;
		pJoint = new PhysJointUpVector(Vec3(0, 1, 0), pBody);

		if (parent) parent->deattach(this);
	}

	/*
	*/
	PhysBody *CameraFPS::getPhysBody() {
		return pBody;
	}

	/*
	*/
	void CameraFPS::update() {
		if (pBody) {
			position = pBody->getTransform().getTranslation() + Vec3(0, 7, 0);
			pBody->setVelocity(Vec3(0, pBody->getVelocity().y, 0));//pBody->getVelocity().y, 0));
		}

		/*Vec3 forwardVec = Vec3::normalize(Vec3(sinf(angle[0]) * cosf(angle[1]),
								  sinf(angle[1]),
								  cosf(angle[0]) * cosf(angle[1])));*/
		if (WindowSystem::Get()->isMouseMoved() && WindowSystem::Get()->isMouseGrabed()) {
			angle[0] = -0.4 * WindowSystem::Get()->getMouseX();
			angle[1] = -0.4 * WindowSystem::Get()->getMouseY();
		}

		if (angle[1] > 80) angle[1] = 75;
		if (angle[1] < -80) angle[1] = -75;

		Vec3 forwardVec = Vec3(sinf(DEG_TO_RAD * angle[0]), 0, cosf(DEG_TO_RAD * angle[0]));
		Vec3 leftVec = Vec3(sinf(DEG_TO_RAD * (angle[0] + 90)), 0, cosf(DEG_TO_RAD * (angle[0] + 90)));
		Vec3 movement = Vec3(0, 0, 0);

		direction.x = sinf(DEG_TO_RAD * angle[0]) * cosf(DEG_TO_RAD * angle[1]);
		direction.y = sinf(DEG_TO_RAD * angle[1]);
		direction.z = cosf(DEG_TO_RAD * angle[0]) * cosf(DEG_TO_RAD * angle[1]);
		direction = Vec3::normalize(direction);

		bool inTheAir = false;
		Vec3 point;
		if (Engine::Get()->physSystem->intersectWorldByRay(position - Vec3(0, size.y, 0),
			position - Vec3(0, size.y + 10, 0), Vec3(),
			point)) {
			if (point.y <= position.y - size.y - 10)
				inTheAir = true;
		}

		if (WindowSystem::Get()->isKeyPressed(WindowSystem::KEY_W)) {
			movement += forwardVec;
		}
		if (WindowSystem::Get()->isKeyPressed(WindowSystem::KEY_S)) {
			movement -= forwardVec;
		}
		if (WindowSystem::Get()->isKeyPressed(WindowSystem::KEY_A)) {
			movement += leftVec;
		}
		if (WindowSystem::Get()->isKeyPressed(WindowSystem::KEY_D)) {
			movement -= leftVec;
		}

		if (inTheAir) {
			movement = movement * 0.5;
		}

		if (movement.length() > EPSILON) {
			movement = Vec3::normalize(movement);
		}

		if (WindowSystem::Get()->isKeyDown(WindowSystem::KEY_Q) && !inTheAir) {
			movement += Vec3(0, 1.5, 0);
		}

		if (pBody) {
			pBody->addTorque(movement * maxVelocity);
		}
		else {
			position += movement * maxVelocity * 0.01;
		}

		transform = Mat4::translate(position) * Mat4::rotate(90, direction);
		view = Mat4::lookAt(position, position + direction, Vec3(0, 1, 0));
	}




	//***************************************************************************
	//CameraFree class ENGINE_API
	//***************************************************************************
	/*
	*/
	CameraFree *CameraFree::Create(bool _activate) {
		CameraFree *camera = new CameraFree();
		camera->initNode();

		camera->position = Vec3(0.0, 0.0, 0.0);
		camera->maxVelocity = 1500.0;

		camera->fov = 60;
		camera->aspect = 4.0 / 3.0;
		camera->zNear = 0.1;
		camera->zFar = 1e4;
		camera->buildProjection();

		camera->pBody = NULL;
		if (_activate)
			Scene::Get()->setCamera(camera);
		return camera;
	}

	/*
	*/
	void CameraFree::Destroy() {
		SAFE_DELETE(pBody);
		delete this;
	}

	/*
	*/
	float CameraFree::getMaxVelocity() {
		return maxVelocity;
	}

	/*
	*/
	void CameraFree::setMaxVelocity(float maxVelocity) {
		this->maxVelocity = maxVelocity;
	}

	/*
	*/
	void CameraFree::setPhysics(const Vec3 &size, float mass) {
		mass = 0.001;
		pBody = PhysBody::CreateSphere(size.y, mass);
		pBody->setTransform(getTransform());

		if (parent) parent->deattach(this);
	}

	/*
	*/
	PhysBody *CameraFree::getPhysBody() {
		return NULL;
	}

	/*
	*/
	void CameraFree::update() {
		if (pBody) {
			position = pBody->getTransform().getTranslation();
			pBody->setVelocity(Vec3(0, 0, 0));
		}

		/*Vec3 forwardVec = Vec3::normalize(Vec3(sinf(angle[0]) * cosf(angle[1]),
								  sinf(angle[1]),
								  cosf(angle[0]) * cosf(angle[1])));*/
		if (WindowSystem::Get()->isMouseMoved() && WindowSystem::Get()->isMouseGrabed()) {
			angle[0] = -0.4 * WindowSystem::Get()->getMouseX();
			angle[1] = -0.4 * WindowSystem::Get()->getMouseY();
		}

		if (angle[1] > 80) angle[1] = 75;
		if (angle[1] < -80) angle[1] = -75;

		Vec3 forwardVec = Vec3(sinf(DEG_TO_RAD * angle[0]) * cosf(DEG_TO_RAD * angle[1]),
			sinf(DEG_TO_RAD * angle[1]),
			cosf(DEG_TO_RAD * angle[0]) * cosf(DEG_TO_RAD * angle[1]));

		Vec3 leftVec = Vec3(sinf(DEG_TO_RAD * (angle[0] + 90)), 0, cosf(DEG_TO_RAD * (angle[0] + 90)));
		Vec3 movement = Vec3(0, 0, 0);

		direction.x = sinf(DEG_TO_RAD * angle[0]) * cosf(DEG_TO_RAD * angle[1]);
		direction.y = sinf(DEG_TO_RAD * angle[1]);
		direction.z = cosf(DEG_TO_RAD * angle[0]) * cosf(DEG_TO_RAD * angle[1]);
		direction = Vec3::normalize(direction);

		if (WindowSystem::Get()->isKeyPressed(WindowSystem::KEY_W)) {
			movement += forwardVec;
		}
		if (WindowSystem::Get()->isKeyPressed(WindowSystem::KEY_S)) {
			movement -= forwardVec;
		}
		if (WindowSystem::Get()->isKeyPressed(WindowSystem::KEY_A)) {
			movement += leftVec;
		}
		if (WindowSystem::Get()->isKeyPressed(WindowSystem::KEY_D)) {
			movement -= leftVec;
		}

		if (movement.length() > EPSILON) {
			movement = Vec3::normalize(movement);
		}

		if (pBody) {
			pBody->addTorque(movement * maxVelocity);
		}
		else {
			position += movement * maxVelocity * 0.01;
		}

		transform = Mat4::translate(position) * Mat4::rotate(90, direction);
		view = Mat4::lookAt(position, position + direction, Vec3(0, 1, 0));
	}



	//***************************************************************************
	//CameraDummy class ENGINE_API
	//***************************************************************************
	/*
	*/
	CameraDummy *CameraDummy::Create(bool _activate) {
		CameraDummy *camera = new CameraDummy();
		camera->initNode();

		camera->fov = 60;
		camera->aspect = 4.0 / 3.0;
		camera->zNear = 0.1;
		camera->zFar = 1e4;
		camera->buildProjection();
		if (_activate)
			Scene::Get()->setCamera(camera);

		return camera;
	}

	/*
	*/
	void CameraDummy::Destroy() {
		delete this;
	}

}
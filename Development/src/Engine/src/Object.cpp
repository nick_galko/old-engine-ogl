/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//**************************************
#include "EngineAPI.h"
//**************************************

namespace Vega {

	//**************************************************************************
	//Object
	//**************************************************************************
	PhysBody *Object::getPhysBody() {
		return NULL;
	}

	/*
	*/
	Material *Object::getMaterial(int s) {
		return NULL;
	}

	//**************************************************************************
	//ActorMesh
	//**************************************************************************
	/*
	*/
	ActorMesh::ActorMesh(const String &path) {
		create(path);
	}

	/*
	*/
	void ActorMesh::create(const String &path) {
		initNode();

		mesh = Cache::Get()->loadMesh(path);
		materials = new Material*[getNumSubsets()];

		transform.identity();
		pBody = NULL;
		Scene::Get()->addObject(this);
	}

	/*
	*/
	void ActorMesh::Destroy() {
		Cache::Get()->deleteMesh(mesh);
		for (int i = 0; i < getNumSubsets(); i++) {
			Cache::Get()->deleteMaterial(materials[i]);
		}
		Cache::Get()->deleteSound(impactSound);
		SAFE_DELETE(pBody);
		delete this;
	}

	/*
	*/
	void ActorMesh::drawSubset(int s) {
		mesh->drawSubset(s);
	}

	/*
	*/
	int ActorMesh::getNumSubsets() {
		return mesh->getNumSubsets();
	}

	/*
	*/
	const Vec3 &ActorMesh::getMax() {
		return mesh->max;
	}

	/*
	*/
	const Vec3 &ActorMesh::getMin() {
		return mesh->min;
	}

	/*
	*/
	const Vec3 &ActorMesh::getCenter() {
		return mesh->center;
	}

	/*
	*/
	float ActorMesh::getRadius() {
		return mesh->radius;
	}

	/*
	*/
	const Vec3 &ActorMesh::getMax(int s) {
		return mesh->subsets[s]->max;
	}

	/*
	*/
	const Vec3 &ActorMesh::getMin(int s) {
		return mesh->subsets[s]->min;
	}

	/*
	*/
	const Vec3 &ActorMesh::getCenter(int s) {
		return mesh->subsets[s]->center;
	}

	/*
	*/
	float ActorMesh::getRadius(int s) {
		return mesh->subsets[s]->radius;
	}

	/*
	*/
	Material *ActorMesh::getMaterial(int s) {
		return materials[s];
	}

	/*
	*/
	void ActorMesh::setMaterial(const String &name, const String &path) {
		Material *material = Cache::Get()->loadMaterial(path);

		if (name == "*")	{
			for (int s = 0; s < mesh->getNumSubsets(); s++) {
				materials[s] = material;
			}
		}
		else {
			int s = mesh->getSubset(name);
			materials[s] = material;
		}
	}

	/*
	*/
	void ActorMesh::setMaterialList(const String &path) {
		FStream *file = FStream::Create(path, FStream::READ_TEXT);

		//Check if exist
		if (!file) {
			Error("ActorMesh::setMaterialList() error: material list file '%s' not found", path);
			return;
		}

		while (!file->eof()) {
			String line = file->gets();
			setMaterial(line.getWord(1), line.getWord(3));
		}
		delete file;
	}


	/*
	*/
	void ActorMesh::setTransform(const Mat4 &transform) {
		if (pBody == NULL) {
			this->transform = transform;
		}
		else {
			pBody->setTransform(transform);
		}
	}

	/*
	*/
	void ActorMesh::update() {
		if (pBody) transform = pBody->getTransform();
	}

	/*
	*/
	PhysBody *ActorMesh::getPhysBody() {
		return pBody;
	}

	/*
	*/
	void ActorMesh::setPhysicsBox(const Vec3 &size, float mass) {
		pBody = PhysBody::CreateBox(size, mass);
		setTransform(transform);

		if (parent) parent->deattach(this);
	}

	/*
	*/
	void ActorMesh::setPhysicsSphere(const Vec3 &size, float mass) {
		pBody = PhysBody::CreateSphere(size.y, mass);
		setTransform(transform);

		if (parent) parent->deattach(this);
	}

	/*
	*/
	void ActorMesh::setPhysicsCylinder(float radius, float height, float mass) {
		pBody = PhysBody::CreateCylinder(radius, height, mass);
		setTransform(transform);

		if (parent) parent->deattach(this);
	}

	/*
	*/
	void ActorMesh::setPhysicsCone(float radius, float height, float mass) {
		pBody = PhysBody::CreateCone(radius, height, mass);
		setTransform(transform);

		if (parent) parent->deattach(this);
	}

	/*
	*/
	void ActorMesh::setPhysicsCapsule(float radius, float height, float mass)  {
		pBody = PhysBody::CreateCapsule(radius, height, mass);
		setTransform(transform);

		if (parent) parent->deattach(this);
	}

	/*
	*/
	void ActorMesh::setPhysicsChampferCylinder(float radius, float height, float mass) {
		pBody = PhysBody::CreateChampferCylinder(radius, height, mass);
		setTransform(transform);

		if (parent) parent->deattach(this);
	}

	/*
	*/
	void ActorMesh::setPhysicsConvexHull(float mass) {
		int numPos = 0;
		for (int i = 0; i < mesh->getNumSubsets(); i++) {
			numPos += mesh->subsets[i]->numVertices;
		}
		Vec3 *pos = new Vec3[numPos];

		int k = 0;
		for (int i = 0; i < mesh->getNumSubsets(); i++) {

			for (int v = 0; v < mesh->subsets[i]->numVertices; v++) {
				pos[k] = mesh->subsets[i]->vertices[v].position;
				k++;
			}
		}

		pBody = PhysBody::CreateConvexHull(pos, numPos, mass);
		setTransform(transform);

		delete[] pos;

		if (parent) parent->deattach(this);
	}

	/*
	*/
	void ActorMesh::setPhysicsStaticMesh() {
		int numPos = 0;
		for (int i = 0; i < mesh->getNumSubsets(); i++) {
			numPos += mesh->subsets[i]->numIndices;
		}
		Vec3 *pos = new Vec3[numPos];

		int k = 0;
		for (int i = 0; i < mesh->getNumSubsets(); i++) {

			for (int v = 0; v < mesh->subsets[i]->numIndices / 3; v++) {
				pos[k * 3 + 0] = mesh->subsets[i]->vertices[mesh->subsets[i]->indices[v * 3 + 0]].position;
				pos[k * 3 + 1] = mesh->subsets[i]->vertices[mesh->subsets[i]->indices[v * 3 + 1]].position;
				pos[k * 3 + 2] = mesh->subsets[i]->vertices[mesh->subsets[i]->indices[v * 3 + 2]].position;
				k++;
			}
		}

		pBody = PhysBody::CreateStaticMesh(pos, numPos, true);
		setTransform(transform);

		delete[] pos;

		if (parent) parent->deattach(this);
	}

	void ActorMesh::setImpactSound(const String &path) {
		impactSound = Cache::Get()->loadSound(path);
		pBody->setImpactSound(impactSound);
	}







	//**************************************************************************
	//ObjectSkinnedMesh
	//**************************************************************************
	/*
	*/
	ObjectSkinnedMesh *ObjectSkinnedMesh::Create(const String &path) {
		ObjectSkinnedMesh *object = new ObjectSkinnedMesh();
		object->initNode();

		object->mesh = SkinnedMesh::Create(path);
		object->materials = new Material*[object->getNumSubsets()];
		TODO("������� ���");
		/*Material *defaultMtr = Cache::Get()->loadMaterial("../gamedata/materials/engine_materials/default.xsmtr");
		for(int i = 0; i < materials.size(); i++) {
		materials[i] = defaultMtr;
		}*/

		object->transform.identity();
		Scene::Get()->addObject(object);
		return object;
	}

	/*
	*/
	void ObjectSkinnedMesh::Destroy() {
		mesh->Destroy();
		for (int i = 0; i < getNumSubsets(); i++) {
			Cache::Get()->deleteMaterial(materials[i]);
		}
		delete this;
	}

	/*
	*/
	void ObjectSkinnedMesh::drawSubset(int s) {
		mesh->drawSubset(s);
	}

	/*
	*/
	int ObjectSkinnedMesh::getNumSubsets() {
		return mesh->getNumSubsets();
	}

	/*
	*/
	const Vec3 &ObjectSkinnedMesh::getMax() {
		return mesh->max;
	}

	/*
	*/
	const Vec3 &ObjectSkinnedMesh::getMin() {
		return mesh->min;
	}

	/*
	*/
	const Vec3 &ObjectSkinnedMesh::getCenter() {
		return mesh->center;
	}

	/*
	*/
	float ObjectSkinnedMesh::getRadius() {
		return mesh->radius;
	}

	/*
	*/
	const Vec3 &ObjectSkinnedMesh::getMax(int s) {
		return mesh->subsets[s]->max;
	}

	/*
	*/
	const Vec3 &ObjectSkinnedMesh::getMin(int s) {
		return mesh->subsets[s]->min;
	}

	/*
	*/
	const Vec3 &ObjectSkinnedMesh::getCenter(int s) {
		return mesh->subsets[s]->center;
	}

	/*
	*/
	float ObjectSkinnedMesh::getRadius(int s) {
		return mesh->subsets[s]->radius;
	}

	/*
	*/
	Material *ObjectSkinnedMesh::getMaterial(int s) {
		return materials[s];
	}

	/*
	*/
	void ObjectSkinnedMesh::setMaterial(const String &name, const String &path) {
		Material *material = Cache::Get()->loadMaterial(path);

		if (name == "*")	{
			for (int s = 0; s < mesh->getNumSubsets(); s++) {
				materials[s] = material;
			}
		}
		else {
			int s = mesh->getSubset(name);
			materials[s] = material;
		}
	}

	/*
	*/
	void ObjectSkinnedMesh::setMaterialList(const String &path) {
		FStream *file = FStream::Create(path, FStream::READ_TEXT);

		//Check if exist
		if (!file) {
			Error("ActorMesh::setMaterialList() error: material list file '%s' not found", path);
			return;
		}

		while (!file->eof()) {
			String line = file->gets();
			setMaterial(line.getWord(1), line.getWord(3));
		}
		delete file;
	}

	/*
	*/
	void ObjectSkinnedMesh::setAnimationFrame(int frame, int from, int to) {
		mesh->setFrame(frame, from, to);
	}




	//**************************************************************************
	//ObjectParticleSystem
	//**************************************************************************
	/*
	*/
	ObjectParticleSystem *ObjectParticleSystem::Create(const String &path, int numParticles) {
		ObjectParticleSystem *object = new ObjectParticleSystem();
		object->initNode();
		object->system = ParticleSystem::Create(path, numParticles);
		Scene::Get()->addObject(object);
		return object;
	}

	/*
	*/
	void ObjectParticleSystem::Destroy() {
		system->Destroy();
		delete this;
	}

	/*
	*/
	void ObjectParticleSystem::drawSubset(int s) {
		system->draw();
	}

	/*
	*/
	int ObjectParticleSystem::getNumSubsets() {
		return 1;
	}

	/*
	*/
	const Vec3 &ObjectParticleSystem::getMax() {
		return system->max;
	}

	/*
	*/
	const Vec3 &ObjectParticleSystem::getMin() {
		return system->min;
	}

	/*
	*/
	const Vec3 &ObjectParticleSystem::getCenter() {
		return system->center;
	}

	/*
	*/
	float ObjectParticleSystem::getRadius() {
		return system->radius;
	}

	/*
	*/
	const Vec3 &ObjectParticleSystem::getMax(int s) {
		return system->max;
	}

	/*
	*/
	const Vec3 &ObjectParticleSystem::getMin(int s) {
		return system->min;
	}

	/*
	*/
	const Vec3 &ObjectParticleSystem::getCenter(int s) {
		return system->center;
	}

	/*
	*/
	float ObjectParticleSystem::getRadius(int s) {
		return system->radius;
	}

	/*
	*/
	Material *ObjectParticleSystem::getMaterial(int s) {
		return NULL;
	}

	/*
	*/
	void ObjectParticleSystem::update() {
		system->update();
	}

	const Vec3 &ObjectParticleSystem::getColor() {
		return system->color;
	}

	void ObjectParticleSystem::setColor(const Vec3 &color) {
		system->color = color;
	}

	void ObjectParticleSystem::setParticleLifeTime(int time) {
		system->lifeTime = time;
	}

	int ObjectParticleSystem::getParticleLifeTime() {
		return system->lifeTime;
	}

	void ObjectParticleSystem::setVelocity(const Vec3 &velocity) {
		system->velocity = velocity;
	}

	const Vec3 &ObjectParticleSystem::getVelocity() {
		return system->velocity;
	}

	void ObjectParticleSystem::setForce(const Vec3 &force) {
		system->force = force;
	}

	const Vec3 &ObjectParticleSystem::getForce() {
		return system->force;
	}

	void ObjectParticleSystem::setDispersion(float dispersion) {
		system->dispersion = dispersion;
	}

	float ObjectParticleSystem::getDispersion() {
		return system->dispersion;
	}

	void ObjectParticleSystem::setSize(float size) {
		system->size = size;
	}

	float ObjectParticleSystem::getSize() {
		return system->size;
	}
}
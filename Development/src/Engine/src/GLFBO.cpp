/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//***************************************************************************
#include "EngineAPI.h"
//***************************************************************************

namespace Vega {

	/*
	*/
	GLFBO *GLFBO::Create(int width, int height) {
		GLFBO *fbo = new GLFBO();

		fbo->width = width;
		fbo->height = height;

		fbo->glColorID = fbo->glStencilID = fbo->glDepthID = 0;
		fbo->colorTarget = fbo->depthTarget = NULL;

		glGenFramebuffersEXT(1, &fbo->glID);

		return fbo;
	}

	/*
	*/
	void GLFBO::CreateColorAttachment() {
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, glID);

		glGenTextures(1, &glColorID);
		glBindTexture(GL_TEXTURE_2D, glColorID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	}

	/*
	*/
	void GLFBO::CreateDepthAttachment() {
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, glID);

		glGenRenderbuffersEXT(1, &glDepthID);
		glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, glDepthID);
		glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT24, width, height);

		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	}

	/*
	*/
	void GLFBO::CreateStencilAttachment() {
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, glID);

		glGenRenderbuffersEXT(1, &glStencilID);
		glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, glStencilID);
		glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_STENCIL_INDEX, width, height);

		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	}

	/*
	*/
	void GLFBO::Destroy() {
		glDeleteFramebuffersEXT(1, &glID);
		if (glColorID) glDeleteRenderbuffersEXT(1, &glColorID);
		if (glDepthID) glDeleteRenderbuffersEXT(1, &glDepthID);
		if (glStencilID) glDeleteRenderbuffersEXT(1, &glStencilID);
		delete this;
	}

	/*
	*/
	void GLFBO::setColorTarget(GLTexture *texture, int face) {
		if (texture) {
			if (face < 0) {
				glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, texture->target, texture->glID, 0);
			}
			else {
				glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB + face, texture->glID, 0);
			}
			colorTarget = texture;
		}
		else {
			glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, glColorID, 0);
		}
		glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT);
		glReadBuffer(GL_COLOR_ATTACHMENT0_EXT);
	}

	/*
	*/
	void GLFBO::setDepthTarget(GLTexture *texture) {
		if (texture) {
			glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, texture->target, texture->glID, 0);
			depthTarget = texture;
		}
		else {
			glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, GL_RENDERBUFFER_EXT, glDepthID);
		}
	}

	/*
	*/
	void GLFBO::set() {
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, glID);

		if (glColorID && !colorTarget) glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, glColorID, 0);
		if (glDepthID && !depthTarget) glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, glDepthID);
		if (glStencilID) glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, glStencilID);

		//GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
		//if(status == GL_FRAMEBUFFER_UNSUPPORTED_EXT) {
		//	LogPrintf("GLFBO::set() error: framebuffer unsupported");
		//}

		glViewport(0, 0, width, height);
	}

	/*
	*/
	void GLFBO::unset() {
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
		glDrawBuffer(GL_BACK);
		glReadBuffer(GL_BACK);
		glViewport(0, 0, WindowSystem::Get()->getWidth(), WindowSystem::Get()->getHeight());
	}

	/*
	*/
	void GLFBO::clear() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	}

	/*
	*/
	void GLFBO::flush() {
		glFlush();
	}

}
/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//**************************************
#include "EngineAPI.h"
//**************************************

namespace Vega {

	Cache *cache;

	Cache::Cache() {
		cache = this;
	}

	Cache *Cache::Get() {
		return cache;
	}

	Material *Cache::loadMaterial(const String &path) {
		std::map<String, std::pair<Material*, int>>::iterator it = materials.find(path);
		if (it == materials.end() || it->second.first == NULL) {
			Material *material = Material::Create(path);
			materials[path].first = material;
			materials[path].second = 1;
			return material;
		}

		it->second.second++;
		return it->second.first;
	}

	Mesh *Cache::loadMesh(const String &path) {
		std::map<String, std::pair<Mesh*, int>>::iterator it = meshs.find(path);
		if (it == meshs.end() || it->second.first == NULL) {
			Mesh *mesh = Mesh::Create(path);
			meshs[path].first = mesh;
			meshs[path].second = 1;
			return mesh;
		}
		it->second.second++;
		return it->second.first;
	}

	ALSound *Cache::loadSound(const String &path) {
		std::map<String, std::pair<ALSound*, int>>::iterator it = sounds.find(path);
		if (it == sounds.end() || it->second.first == NULL) {
			ALSound *sound = ALSound::Create(path);
			sounds[path].first = sound;
			sounds[path].second = 1;
			return sound;
		}
		it->second.second++;
		return it->second.first;
	}

	GLTexture *Cache::loadTexture2d(const String &path) {
		std::map<String, std::pair<GLTexture*, int>>::iterator it = textures.find(path);
		if (it == textures.end() || it->second.first == NULL) {
			GLTexture *texture = GLTexture::Create2d(path);
			textures[path].first = texture;
			textures[path].second = 1;
			return texture;
		}
		it->second.second++;
		return it->second.first;
	}

	GLTexture *Cache::loadTexture2d(ILImage *image, const String &path) {
		std::map<String, std::pair<GLTexture*, int>>::iterator it = textures.find(path);
		if (it == textures.end() || it->second.first == NULL) {
			GLTexture *texture = GLTexture::Create2d(image);
			textures[path].first = texture;
			textures[path].second = 1;
			return texture;
		}
		it->second.second++;
		return it->second.first;
	}

	GLTexture *Cache::loadTextureCube(const String &path) {
		std::map<String, std::pair<GLTexture*, int>>::iterator it = textures.find(path);
		if (it == textures.end() || it->second.first == NULL) {
			GLTexture *texture = GLTexture::CreateCube(path);
			textures[path].first = texture;
			textures[path].second = 1;
			return texture;
		}
		it->second.second++;
		return it->second.first;
	}

	GLShader *Cache::loadShader(const String &path, const String &defines) {
		std::map<String, std::pair<GLShader*, int>>::iterator it = shaders.find(path);
		if (it == shaders.end() || it->second.first == NULL) {
			String defines = "";
			if (Config::Get()->getBool(CONF_REFLECTIONS) == true) defines += "#define REFLECTIONS\n";
			if (Config::Get()->getBool(CONF_SPECULAR) == true) defines += "#define SPECULAR\n";
			if (Config::Get()->getBool(CONF_PARALLAX) == true) defines += "#define PARALLAX\n";
			if (Config::Get()->getInt(CONF_SHADOW_TYPE) == 1) defines += "#define SM_SHADOWS\n";
			if (Config::Get()->getInt(CONF_SHADOW_TYPE) == 2) defines += "#define VSM_SHADOWS\n";

			GLShader *shader = GLShader::Create(path, defines);
			shaders[path].first = shader;
			shaders[path].second = 1;
			return shader;
		}
		it->second.second++;
		return it->second.first;
	}

	void Cache::reloadShaders() {
		String defines = "";
		if (Config::Get()->getBool(CONF_REFLECTIONS) == true) defines += "#define REFLECTIONS\n";
		if (Config::Get()->getBool(CONF_SPECULAR) == true) defines += "#define SPECULAR\n";
		if (Config::Get()->getBool(CONF_PARALLAX) == true) defines += "#define PARALLAX\n";
		if (Config::Get()->getInt(CONF_SHADOW_TYPE) == 1) defines += "#define SM_SHADOWS\n";
		if (Config::Get()->getInt(CONF_SHADOW_TYPE) == 2) defines += "#define VSM_SHADOWS\n";

		std::map<String, std::pair<GLShader*, int>>::iterator it;
		for (it = shaders.begin(); it != shaders.end(); it++) {
			it->second.first->Destroy();

			GLShader *shader = GLShader::Create(it->first, defines);
			memmove(it->second.first, shader, sizeof(GLShader));
		}
	}



	void Cache::deleteMaterial(Material *material) {
		std::map<String, std::pair<Material*, int>>::iterator it;
		for (it = materials.begin(); it != materials.end(); it++) {
			if (it->second.first == material && it->second.second <= 1) {
				it->second.first->Destroy();
				it->second.first = NULL;
			}
		}
	}

	void Cache::deleteMesh(Mesh *mesh) {
		std::map<String, std::pair<Mesh*, int>>::iterator it;
		for (it = meshs.begin(); it != meshs.end(); it++) {
			if (it->second.first == mesh && it->second.second <= 1) {
				it->second.first->Destroy();
				it->second.first = NULL;
			}
		}
	}

	void Cache::deleteSound(ALSound *sound) {
		std::map<String, std::pair<ALSound*, int>>::iterator it;
		for (it = sounds.begin(); it != sounds.end(); it++) {
			if (it->second.first == sound && it->second.second <= 1) {
				it->second.first->Destroy();
				it->second.first = NULL;
			}
		}
	}

	void Cache::deleteTexture(GLTexture *texture) {
		std::map<String, std::pair<GLTexture*, int>>::iterator it;
		for (it = textures.begin(); it != textures.end(); it++) {
			if (it->second.first == texture && it->second.second <= 1) {
				it->second.first->Destroy();
				it->second.first = NULL;
			}
		}
	}

	void Cache::deleteShader(GLShader *shader) {
		std::map<String, std::pair<GLShader*, int>>::iterator it;
		for (it = shaders.begin(); it != shaders.end(); it++) {
			if (it->second.first == shader && it->second.second <= 1) {
				it->second.first->Destroy();
				it->second.first = NULL;
			}
		}
	}

}

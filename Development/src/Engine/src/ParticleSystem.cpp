/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//**************************************
#include "EngineAPI.h"
#include <math.h>
//**************************************

namespace Vega {

	/*
	*/
	ParticleSystem *ParticleSystem::Create(const String &path, int numParticles) {
		ParticleSystem *system = new ParticleSystem();

		system->texture = GLTexture::Create2d(path);

		system->color = Vec3(1, 1, 1);
		system->velocity = Vec3(0, 1, 0);
		system->size = 1.0;

		system->lifeTime = 5000;
		system->dispersion = 0.5;

		system->numParticles = numParticles;

		system->particles = new Particle[numParticles];
		system->vertices = new Vertex[numParticles * 4];

		for (int i = 0; i < system->numParticles; i++) {
			system->particles[i].lifeTime = 0;
			system->particles[i].velocity = system->velocity;
			system->particles[i].position = Vec3(0, 0, 0);

			system->vertices[i * 4 + 0].texcoord = Vec2(0, 0);
			system->vertices[i * 4 + 1].texcoord = Vec2(1, 0);
			system->vertices[i * 4 + 2].texcoord = Vec2(1, 1);
			system->vertices[i * 4 + 3].texcoord = Vec2(0, 1);
		}

		return system;
	}

	/*
	*/
	void ParticleSystem::Destroy() {
		texture->Destroy();
		delete this;
	}

	/*
	*/
	void ParticleSystem::update() {
		//calculate particle parameters
		float vlength = velocity.length();
		for (int i = 0; i < numParticles; i++) {
			if (lifeTime < particles[i].lifeTime || particles[i].lifeTime == 0) {
				particles[i].position = Vec3(0, 0, 0);
				particles[i].velocity.x = velocity.x + sinf(rand()) * dispersion * vlength;
				particles[i].velocity.y = velocity.y + sinf(rand()) * dispersion * vlength;
				particles[i].velocity.z = velocity.z + sinf(rand()) * dispersion * vlength;
				particles[i].lifeTime = ((float)i / (float)numParticles) * lifeTime;
			}
			particles[i].velocity += force * WindowSystem::Get()->getDTime() * 0.001;
			particles[i].position += particles[i].velocity * WindowSystem::Get()->getDTime() * 0.001;
			particles[i].lifeTime += WindowSystem::Get()->getDTime();
		}

		min = particles[0].position;
		max = particles[0].position;

		for (int i = 1; i < numParticles; i++) {
			min.x = min(min.x, particles[i].position.x);
			min.y = min(min.y, particles[i].position.y);
			min.z = min(min.z, particles[i].position.z);

			max.x = max(max.x, particles[i].position.x);
			max.y = max(max.y, particles[i].position.y);
			max.z = max(max.z, particles[i].position.z);
		}

		center = (min + max) * 0.5;
		radius = (center - min).length();
	}

	/*
	*/
	void ParticleSystem::draw() {
		//setup state
		texture->set(0);
		GLSystem::Get()->enableBlending(GLSystem::ONE, GLSystem::ONE);
		GLSystem::Get()->setColor(color);
		GLSystem::Get()->depthMask(false);

		//setup vertices
		Mat4 mdl = GLSystem::Get()->getMatrix_Modelview();

		Vec3 vRight(mdl.e[0], mdl.e[4], mdl.e[8]);
		Vec3 vUp(mdl.e[1], mdl.e[5], mdl.e[9]);

		Vec3 qv0 = (-vRight - vUp) * size;
		Vec3 qv1 = (vRight - vUp) * size;
		Vec3 qv2 = (vRight + vUp) * size;
		Vec3 qv3 = (-vRight + vUp) * size;

		for (int i = 0; i < numParticles; i++) {
			vertices[i * 4 + 0].position = particles[i].position + qv0;
			vertices[i * 4 + 1].position = particles[i].position + qv1;
			vertices[i * 4 + 2].position = particles[i].position + qv2;
			vertices[i * 4 + 3].position = particles[i].position + qv3;
		}

		//draw it
		glClientActiveTextureARB(GL_TEXTURE0_ARB);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), vertices->texcoord);

		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer(3, GL_FLOAT, sizeof(Vertex), vertices->position);

		glDrawArrays(GL_QUADS, 0, numParticles * 4);

		glDisableClientState(GL_VERTEX_ARRAY);

		glClientActiveTextureARB(GL_TEXTURE0_ARB);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		//unset state
		GLSystem::Get()->depthMask(true);
		GLSystem::Get()->disableBlending();
		texture->unset(0);
		GLSystem::Get()->setColor(Vec4(1, 1, 1, 1));
	}

}



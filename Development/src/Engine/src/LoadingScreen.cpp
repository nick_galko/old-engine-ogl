/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//***************************************************************************
#include "EngineAPI.h"
//***************************************************************************

namespace Vega {

	/*
	*/
	LoadingScreen *LoadingScreen::Create(const String &path) {
		LoadingScreen *screen = new LoadingScreen();

		screen->background = GLTexture::Create2d(path);
		screen->background->setFilter(GLTexture::LINEAR);
		return screen;
	}

	void LoadingScreen::Destroy() {
		delete this;
	}

	void LoadingScreen::show() {
		GLSystem::Get()->clear(GLSystem::COLOR_BUFFER | GLSystem::DEPTH_BUFFER);
		GLSystem::Get()->enable2d(true);

		background->set(0);
		GLSystem::Get()->drawRect(0, 0, 1, 1, 0, 0, 1, 1);
		background->unset(0);

		GLSystem::Get()->enable3d();
		GLSystem::Get()->flush();
		WindowSystem::Get()->swapBuffers();
	}

}

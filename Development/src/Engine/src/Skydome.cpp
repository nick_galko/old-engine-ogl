/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//************************************
#include "EngineAPI.h"
//************************************

namespace Vega {

	//**************************************************************************
	//Water
	//**************************************************************************
	/*
	*/
	Skydome *Skydome::Create() {
		Skydome *sky = new Skydome();

		sky->material = Material::Create("../gamedata/materials/engine_materials/skybox.xsmtr");
		sky->sphereMesh = Cache::Get()->loadMesh("../gamedata/meshes/engine_meshes/sphere.xsmsh");
		Scene::Get()->setSkydome(sky);
		return sky;
	}

	/*
	*/
	void Skydome::Destroy() {
		material->Destroy();
		Cache::Get()->deleteMesh(sphereMesh);
		delete this;
	}

	/*
	*/
	void Skydome::draw() {
		sphereMesh->drawSubset(0);
	}

	/*
	*/
	void Skydome::serialize(FStream *file) {
	}
}
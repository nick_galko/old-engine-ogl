/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//**************************************
#include "EngineAPI.h"
#include <cstdlib>
#include <stdarg.h>
//**************************************

namespace Vega {

	FStream *FStream::Create(const String &path, Mode mode) {
		FStream *fstream = new FStream();
		if (mode == READ_TEXT) fstream->file = fopen(path.c_str(), "rt");
		if (mode == WRITE_TEXT) fstream->file = fopen(path.c_str(), "wt");
		if (mode == APPEND_TEXT) fstream->file = fopen(path.c_str(), "a+t");

		if (!fstream->file) return NULL;

		return fstream;
	}

	void FStream::Destroy() {
		if (file) fclose(file);
		delete this;
	}

	void FStream::close() {
		if (file) fclose(file);
	}

	bool FStream::eof() {
		return feof(file);
	}

	char FStream::getc() {
		return fgetc(file);
	}

	String FStream::gets() {
		if (feof(file)) return "";

		String output = "";
		unsigned char h = fgetc(file);

		while ((h != '\n') && !feof(file)) {
			output += h;
			h = fgetc(file);
		}
		return output;
	}

	void FStream::printf(const char *format, ...) {
		va_list arg;
		va_start(arg, format);

		vfprintf(file, format, arg);

		va_end(arg);
	}

}
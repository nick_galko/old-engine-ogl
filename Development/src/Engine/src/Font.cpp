/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//**************************************
#include "EngineAPI.h"
//**************************************

namespace Vega {

	Font* Font::Create(const String &path) {
		Font *font = new Font();

		font->fontTex = GLTexture::Create2d(path);
		font->fontTex->setFilter(GLTexture::LINEAR);

		for (int x = 0; x < 16; x++) {
			for (int y = 0; y < 16; y++) {

				float cx = 0.0625f;
				float cy = 0.0625f;

				font->list[y * 16 + x] = GLDisplayList::Create();
				font->list[y * 16 + x]->beginBuild();

				GLSystem::Get()->drawRect(0, 0, 1, 1, x*cx, y*cy - 0.0625f, x*cx + 0.0625f, y*cy);
				GLSystem::Get()->translate(Vec3(0.8, 0, 0));

				font->list[y * 16 + x]->endBuild();
			}
		}
		return font;
	}

	void Font::Destroy() {
		for (int i = 0; i < 256; i++) {
			list[i]->Destroy();
		}

		fontTex->Destroy();
		delete this;
	}


	void Font::print(int x, int y, int size, const String &text, const Vec3 &color, float alpha) {
		GLSystem::Get()->enable2d(false);
		GLSystem::Get()->disableCulling();

		GLSystem::Get()->translate(Vec3(x, y, 0));
		GLSystem::Get()->scale(Vec3(size, size, 1));

		GLSystem::Get()->enableBlending(GLSystem::ONE, GLSystem::ONE_MINUS_SRC_ALPHA);

		GLSystem::Get()->setColor(Vec4(color * alpha, alpha));
		fontTex->set(0);

		for (int p = 0; p < text.length(); p++) {
			list[text.data()[p] + 16]->call();
		}

		fontTex->unset(0);
		GLSystem::Get()->setColor(Vec4(1, 1, 1, 1));

		GLSystem::Get()->disableBlending();

		GLSystem::Get()->enableCulling();
		GLSystem::Get()->enable3d();
	}

}




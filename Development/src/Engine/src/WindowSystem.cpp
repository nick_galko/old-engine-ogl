/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//***************************************************
#include "EngineAPI.h"
#include "../../Platform/inc/glfw3.h"
#include "../../Platform/inc/glfw3native.h"
#include "../inc/WindowEvents.h"
//***************************************************
#ifdef WIN32
#include <windows.h>
#endif
//***************************************************

namespace Vega {
	
	/*
	*/
	WindowSystem::WindowSystem(int _width, int _height, int _bpp, int _zdepth, bool _fullscreen) {
		//-----read-config-values-----------------------------------
		width = _width;
		height = _height;
		bpp = _bpp;
		zdepth = _zdepth;
		fullscreen = _fullscreen;
	}

	/*
	*/
	void WindowSystem::Initialize() {
		LogHeader("WindowSystem::Initialize()");

		glfwSetErrorCallback(error_callback);

		if (!glfwInit())
			exit(EXIT_FAILURE);
		GLFWmonitor *mMonitor = NULL;
		if (fullscreen)
			mMonitor = glfwGetPrimaryMonitor();
		window = glfwCreateWindow(width, height, "Vega Engine", mMonitor, NULL);
		if (!window)
		{
			glfwTerminate();
			exit(EXIT_FAILURE);
		}

		glfwMakeContextCurrent(window);

		glfwSetKeyCallback(window, key_callback);
		glfwSetMouseButtonCallback(window, mouse_button_callback);
		glfwSetCursorPosCallback(window, cursor_position_callback);
		glfwSetWindowSizeCallback(window, window_size_callback);

		hWnd = GetCrossPlatformHWND(window);

		TODO("Remove this!-Not crossplatfrom!");
		if (!(hDC = GetDC((HWND)hWnd)))	{
			Error("WindowSystem::Initialize() error: can't Create a GL device context");
			return;
		}

		if (!(hRC = glfwGetWGLContext(window)))	{
			Error("WindowSystem::Initialize() error: can't Create a GL rendering context");
			return;
		}

		for (int i = 0; i < 3; i++)
			mouseButtons[i] = false;

		for (int i = 0; i < 315; i++)
			keys[i] = false;

		mouseGrabed = false;
		TODO("Replace me");
		LARGE_INTEGER freq;
		QueryPerformanceFrequency(&freq);
		frequency = freq.QuadPart;
	}

	/*
	*/
	WindowSystem *WindowSystem::Get() {
		return GetEngine()->windowSystem;
	}

	/*
	*/
	void WindowSystem::Destroy() {
		glfwDestroyWindow(window);

		TODO("Replace me");
		if (hDC) {
			ReleaseDC((HWND)hWnd, hDC);
			hDC = NULL;
		}

		glfwTerminate();
		delete this;
	}

	/*
	*/
	void WindowSystem::setTitle(const String &title) {
		glfwSetWindowTitle(window, title);
	}

	/*
	*/
	void WindowSystem::update() {
		updateTimer();

		memcpy(oldKeys, keys, sizeof(keys));
		memcpy(oldMouseButtons, mouseButtons, sizeof(mouseButtons));
		mousing = false;

		TODO("Replace me");
		MSG	msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))	{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (mouseGrabed) {
			oldMouseX = mouseX;
			oldMouseY = mouseY;
			mouseX += mx - width / 2;
			mouseY += my - height / 2;
			setMousePos(width / 2, height / 2);
		}
		else {
			oldMouseX = mouseX;
			oldMouseY = mouseY;
			mouseX = mx;
			mouseY = my;
		}
	}

	/*
	*/
	bool WindowSystem::isKeyPressed(Key key) {
		return keys[key];
	}

	/*
	*/
	bool WindowSystem::isKeyDown(Key key) {
		return (keys[key] && !oldKeys[key]);
	}

	/*
	*/
	bool WindowSystem::isKeyUp(Key key) {
		return (!keys[key] && oldKeys[key]);
	}

	/*
	*/
	bool WindowSystem::isMouseButtonPressed(MouseButton mb) {
		return mouseButtons[mb];
	}

	/*
	*/
	bool WindowSystem::isMouseButtonDown(MouseButton mb) {
		return (mouseButtons[mb] && !oldMouseButtons[mb]);
	}

	/*
	*/
	bool WindowSystem::isMouseButtonUp(MouseButton mb) {
		return (!mouseButtons[mb] && oldMouseButtons[mb]);
	}

	/*
	*/
	void WindowSystem::showCursor(bool show) {
		int value = GLFW_CURSOR_NORMAL;
		if (show)
			value = GLFW_CURSOR_NORMAL;
		else
			value = GLFW_CURSOR_HIDDEN;
		glfwSetInputMode(window, GLFW_CURSOR, value);
		cursorVisible = show;
	}

	/*
	*/
	void WindowSystem::setMousePos(int x, int y) {
		TODO("Replace this");
		//������ ��� �� ����������
		POINT pt;
		pt.x = x;
		pt.y = y;
		ClientToScreen((HWND)hWnd, &pt);
		SetCursorPos(pt.x, pt.y);
	}

	/*
	*/
	void WindowSystem::grabMouse(bool grab) {
		mouseX = oldMouseX = width / 2;
		mouseY = oldMouseY = height / 2;

		if (grab) { setMousePos(width / 2, height / 2); }
		showCursor(!grab);
		mouseGrabed = grab;
	}

	/*
	*/
	void WindowSystem::swapBuffers() {
		glfwSwapBuffers(window);
	}

	/*
	*/
	void WindowSystem::updateTimer() {
		TODO("Replace me");
		if (!tPause) {
			LARGE_INTEGER counter;
			QueryPerformanceCounter(&counter);
			int ticks = counter.QuadPart * 1000 / frequency;

			dTime = ticks - eTime;
			eTime = ticks;
		}
	}

	/*
	*/
	int WindowSystem::getTime() {
		TODO("Replace me");
		LARGE_INTEGER counter;
		QueryPerformanceCounter(&counter);
		int ticks = counter.QuadPart * 1000 / frequency;

		return ticks;
	}

}
/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//***************************************************************************
#include "EngineAPI.h"
//***************************************************************************

namespace Vega {


	/*
	*/
	GLShader *GLShader::Create(const String &path, const String &defines) {
		GLShader *shader = new GLShader();

		shader->vs = NULL;
		shader->fs = NULL;
		shader->program = NULL;
		shader->source = "";

		FStream *file = FStream::Create(path, FStream::READ_TEXT);

		if (!file) {
			Error("GLShader::create() error: shader file '%s'  was not found", path);
			return NULL;
		}

		String line, vsCode, fsCode;

		while (!file->eof()) {
			line = file->gets();


			//find GLSL vertex shader
			if (line == "[GLSL_VERTEX_SHADER]") {
				vsCode = "";

				while (!file->eof()) {
					line = file->gets();
					if (line == "[GLSL_FRAGMENT_SHADER]") break;
					vsCode = vsCode + line + "\n";
				}

				vsCode = defines + vsCode;

				shader->source += vsCode;

				const char *vsString[1];
				vsString[0] = (char*)vsCode.c_str();

				shader->vs = glCreateShaderObjectARB(GL_VERTEX_SHADER_ARB);
				glShaderSourceARB(shader->vs, 1, vsString, NULL);
				glCompileShaderARB(shader->vs);

				int compiled;
				glGetObjectParameterivARB(shader->vs, GL_OBJECT_COMPILE_STATUS_ARB, &compiled);

				if (!compiled) {
					char errorString[4096];
					glGetInfoLogARB(shader->vs, sizeof(errorString), NULL, errorString);
					Error("GLShader::create() error: shader file '%s' vs compiling error: '%s'", path, String(errorString));
					return NULL;
				}
			}


			//find GLSL fragment shader
			if (line == "[GLSL_FRAGMENT_SHADER]") {
				fsCode = "";
				while (!file->eof()) {
					line = file->gets();
					if (line == "[GLSL_VERTEX_SHADER]") break;
					fsCode = fsCode + line + "\n";
				}

				fsCode = defines + fsCode;

				shader->source += fsCode;

				const char *fsString[1];
				fsString[0] = (char*)fsCode.c_str();

				shader->fs = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
				glShaderSourceARB(shader->fs, 1, fsString, NULL);
				glCompileShaderARB(shader->fs);

				int compiled;
				glGetObjectParameterivARB(shader->fs, GL_OBJECT_COMPILE_STATUS_ARB, &compiled);

				if (!compiled) {
					char errorString[4096];
					glGetInfoLogARB(shader->fs, sizeof(errorString), NULL, errorString);
					Error("GLShader::create() error: shader file '%s' fs compiling error: '%s'", path, String(errorString));;
					return NULL;
				}
			}
		}

		file->Destroy();

		//create
		shader->program = glCreateProgramObjectARB();
		if (shader->vs) glAttachObjectARB(shader->program, shader->vs);
		if (shader->fs) glAttachObjectARB(shader->program, shader->fs);

		int linked;
		glLinkProgramARB(shader->program);
		glGetObjectParameterivARB(shader->program, GL_OBJECT_LINK_STATUS_ARB, &linked);

		if (!linked) {
			char errorString[4096];
			glGetInfoLogARB(shader->program, sizeof(errorString), NULL, errorString);
			Error("GLShader::create() error: shader file '%s' shader linking error: '%s'", path, String(errorString));
		}
		return shader;
	}

	/*
	*/
	void GLShader::Destroy() {
		if (vs) glDeleteObjectARB(vs);
		if (fs) glDeleteObjectARB(fs);
		glDeleteObjectARB(program);
		//delete this;
	}

	/*
	*/
	void GLShader::set() {
		glUseProgramObjectARB(program);
	}

	/*
	*/
	void GLShader::unset() {
		glUseProgramObjectARB(NULL);
	}

	/*
	*/
	void GLShader::sendMat4(const String &name, const Mat4 &value) {
		GLint param = glGetUniformLocationARB(program, name.c_str());
		glUniformMatrix4fvARB(param, 1, false, value);
	}

	/*
	*/
	void GLShader::sendVec4(const String &name, const Vec4 &value) {
		GLint param = glGetUniformLocationARB(program, name.c_str());
		glUniform4fvARB(param, 1, value);
	}

	/*
	*/
	void GLShader::sendVec3(const String &name, const Vec3 &value) {
		GLint param = glGetUniformLocationARB(program, name.c_str());
		glUniform3fvARB(param, 1, value);
	}

	/*
	*/
	void GLShader::sendVec2(const String &name, const Vec2 &value) {
		GLint param = glGetUniformLocationARB(program, name.c_str());
		glUniform2fvARB(param, 1, value);
	}

	/*
	*/
	void GLShader::sendFloat(const String &name, float value) {
		int param = glGetUniformLocationARB(program, name.c_str());
		glUniform1fARB(param, value);
	}

	/*
	*/
	void GLShader::sendInt(const String &name, int value) {
		int param = glGetUniformLocationARB(program, name.c_str());
		glUniform1iARB(param, value);
	}

}

/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//**************************************
#include "../Include/Scene.h"
#include "../Include/Config.h"
#include "../Include/Log.h"
#include "../Include/ALSystem.h"
#include "../Include/WindowSystem.h"
#include "../Include/Cache.h"
#include "../Include/Error.h"
//**************************************

namespace XSystemEngine {

Scene *scene;
/*
*/
Scene *Scene::Create() {
	if(scene) { return scene; };

	scene = new Scene();

	/*
	default nodes 
	*/
	scene->skydome = NULL;
	scene->water = NULL;
	scene->terrain = NULL;
	scene->camera = NULL;

	/*
	System requirements
	*/
	if(!GLSystem::Get()->vboSupported()) {
		Error::showAndExit("GLSystem: your videocard does not support vertex buffer object.");
	};

	if(!GLSystem::Get()->occlusionQuerySupported() ||
		!GLSystem::Get()->glslSupported() ||
		!GLSystem::Get()->pBufferSupported()) {
		scene->render = OLD_RENDER;
	} else {
		scene->render = OLD_RENDER;
	};

	/*
	PBO and RTargets
	*/
//_USE_NEW_RENDER
	scene->viewportPBO = GLPBuffer::Create(512, 512);

	int size = Config::Get()->getInt(CONF_SHADOW_SIZE);
	scene->shadowPBO = GLPBuffer::Create(size, size);

	scene->viewportCopy = GLTexture::Create2d(512, 512, GLTexture::RGBA);
	scene->viewportCopy->setWrap(GLTexture::CLAMP_TO_EDGE);
	scene->viewportCopy->setFilter(GLTexture::LINEAR);

	scene->reflectionMap = GLTexture::Create2d(512, 512, GLTexture::RGBA);
	scene->reflectionMap->setWrap(GLTexture::CLAMP_TO_EDGE);
	scene->reflectionMap->setFilter(GLTexture::LINEAR);

	scene->viewportCopy_brightPass = GLTexture::Create2d(512, 512, GLTexture::RGBA);
	scene->viewportCopy_brightPass->setWrap(GLTexture::CLAMP_TO_EDGE);
	scene->viewportCopy_brightPass->setFilter(GLTexture::LINEAR);

	scene->viewportCopy_brightPass_blured = GLTexture::Create2d(512, 512, GLTexture::RGBA);
	scene->viewportCopy_brightPass_blured->setWrap(GLTexture::CLAMP_TO_EDGE);
	scene->viewportCopy_brightPass_blured->setFilter(GLTexture::LINEAR);

	/*
	default query and frustum
	*/
	scene->query = GLOcclusionQuery::Create();
	scene->frustum = new Frustum();
	scene->sphereMesh = Mesh::Create("data/meshes/engine_meshes/sphere.xsmsh");

	/*
	default materials
	*/
	scene->depthPass = Material::Create("data/materials/engine_materials/depth_pass.xsmtr");
	scene->hdr = Material::Create("data/materials/engine_materials/hdr.xsmtr");
//_END_USE

	return scene;
};

/*
*/
Scene *Scene::Get() {
	return scene;
};

/*
*/
void Scene::Destroy() {
	clear();
};

/*
*/
void Scene::clear() {
	objects.clear();
	lights.clear();

	delete terrain;
	delete water;
};

/*
*/
void Scene::reloadShaders() {
	Cache::Get()->reloadShaders();
};

/*
*/
Object *Scene::intersectObjects(const Vec3 &src, const Vec3 &dst, Vec3 &normal, Vec3 &point) {
	PhysBody *pBody = NULL;
	pBody = PhysSystem::Get()->intersectWorldByRay(src, dst, normal, point);

	if(!pBody) { 
		return NULL;
	};

	for(int i = 0; i > objects.size(); i++) {
		if(pBody == objects[i]->getPhysBody()) {
			return objects[i];
		};
	};

	return NULL;
};

/*
*/
bool Scene::intersectTerrain(const Vec3 &src, const Vec3 &dst, Vec3 &normal, Vec3 &point) {
	PhysBody *pBody = NULL;
	pBody = PhysSystem::Get()->intersectWorldByRay(src, dst, normal, point);

	if(!pBody) {
		return false;
	};

	if(terrain && terrain->pBody == pBody) {
		return true;
	};
	return false;
};



/*
*/
void Scene::setGravity(const Vec3 &gravity) {
	this->gravity = gravity;
};

/*
*/
void Scene::setAmbient(const Vec3 &color) {
	ambient = color;
};

/*
*/
void Scene::setCamera(Camera *camera) {
	delete this->camera;
	this->camera = camera;
};

/*
*/
void Scene::setWater(Water *water) {
	this->water = water;
};

/*
*/
void Scene::setTerrain(Terrain *terrain) {
	this->terrain = terrain;
};

/*
*/
void Scene::setSkydome(Skydome *skydome) {
	this->skydome = skydome;
};

/*
*/
void Scene::addLight(Light *light) {
	lights.push_back(light);
};


/*
*/
void Scene::addObject(Object *object) {
	objects.push_back(object);
};


/*
*/
void Scene::deleteLight(Light *light) {
	for(int i = 0; i < (int)lights.size(); i++) {
		if(light == lights[i]) {
			lights[i] = NULL;
		};
	};
};

/*
*/
void Scene::deleteObject(Object *object) {
	for(int i = 0; i < objects.size(); i++) {
		if(object == objects[i]) {
			objects[i] = NULL;
		};
	};
};












/*
*/
void Scene::drawAmbient(bool blended) {
_USE_NEW_RENDER
	/*
	draw terrain
	*/
	if(terrain && !blended) {
		Material *mtr = terrain->getMaterial();
		
		if(mtr && mtr->setPass(Material::AMBIENT)) {
			mtr->setFloat(U_TIME, WindowSystem::Get()->getETime());
			mtr->setVec3(U_LIGHT_COLOR, ambient);
			mtr->setVec3(U_VIEW_POSITION, camera->getPosition());
			mtr->setMat4(U_MVP_TRANSFORM, GLSystem::Get()->getMatrix_MVP());

			frustum->Get();

			for(int n = 0; n < terrain->getNumNodes(); n++)	{
				if(!frustum->isInside(terrain->getMin(n), terrain->getMax(n)))
					continue;
					
				terrain->drawNode(n, camera->getPosition());
			};

			mtr->unsetPass();
		};
	};

	/*
	draw objects
	*/
	for(int m = 0; m < objects.size(); m++) {
		if(!objects[m]) {
			continue;
		};

		Object *object = objects[m];
				
		GLSystem::Get()->push();
		GLSystem::Get()->multMatrix(object->getTransform());
		
		frustum->Get();
		if(!frustum->isInside(object->getCenter(), object->getRadius())) {
			GLSystem::Get()->pop();
			continue;
		};
	
		for(int s = 0; s < object->getNumSubsets(); s++) {
			if(!frustum->isInside(object->getCenter(s), object->getRadius(s)))
				continue;
			
			Material *mtr = object->getMaterial(s);
			if(!mtr) continue;
						
			if(blended) {
				if(!mtr->hasBlending()) continue;
				mtr->setBlending();
			} else {
				if(mtr->hasBlending()) continue;
			};
			mtr->setAlphaTest();

			if(!mtr->setPass(Material::AMBIENT)) continue;
			mtr->setFloat(U_TIME, WindowSystem::Get()->getETime());
			mtr->setVec3(U_LIGHT_COLOR, ambient);
			mtr->setVec3(U_VIEW_POSITION, camera->getPosition());
			mtr->setMat4(U_MVP_TRANSFORM, GLSystem::Get()->getMatrix_MVP());
			mtr->setMat4(U_WORLD_TRANSFORM, object->getTransform());
			
			object->drawSubset(s);
			
			mtr->unsetAlphaTest();
			if(mtr->hasBlending() && blended) {
				mtr->unsetBlending();
			};
			mtr->unsetPass();
		};

		GLSystem::Get()->pop();
	};

	for(int m = 0; m < objects.size(); m++) {
		if(!objects[m]) {
			continue;
		};
		Object *object = objects[m];

		if(blended && object->getType() == SceneNode::SCENE_NODE_OBJECT_PARTICLE_SYSTEM) {

			GLSystem::Get()->push();
			GLSystem::Get()->multMatrix(object->getTransform());
		
			frustum->Get();
			if(!frustum->isInside(object->getCenter(), object->getRadius())) {
				GLSystem::Get()->pop();
				continue;
			};

			object->drawSubset(0);

			GLSystem::Get()->pop();
		};
	};
	
_END_USE

_USE_OLD_RENDER
	/*
	draw terrain
	*/
	if(terrain && !blended) {
		Material *mtr = terrain->getMaterial();
		
		if(mtr) {
			mtr->texture0->set(0);
			frustum->Get();

			for(int n = 0; n < terrain->getNumNodes(); n++)	{
				if(!frustum->isInside(terrain->getMin(n), terrain->getMax(n)))
					continue;
					
				terrain->drawNode(n, camera->getPosition());
			};
			mtr->texture0->unset(0);
		};
	};

	/*
	draw objects
	*/
	for(int m = 0; m < objects.size(); m++) {
		if(!objects[m]) {
			continue;
		};

		Object *object = objects[m];
				
		GLSystem::Get()->push();
		GLSystem::Get()->multMatrix(object->getTransform());
		
		frustum->Get();
		if(!frustum->isInside(object->getCenter(), object->getRadius())) {
			GLSystem::Get()->pop();
			continue;
		};
	
		for(int s = 0; s < object->getNumSubsets(); s++) {
			if(!frustum->isInside(object->getCenter(s), object->getRadius(s)))
				continue;
			
			Material *mtr = object->getMaterial(s);
			if(!mtr) continue;
						
			if(blended) {
				if(!mtr->hasBlending()) continue;
				mtr->setBlending();
			} else {
				if(mtr->hasBlending()) continue;
			};
			mtr->setAlphaTest();
			mtr->texture0->set(0);
			
			object->drawSubset(s);
			
			mtr->unsetAlphaTest();
			if(mtr->hasBlending() && blended) {
				mtr->unsetBlending();
			};
			mtr->texture0->unset(0);
		};

		GLSystem::Get()->pop();
	};

	for(int m = 0; m < objects.size(); m++) {
		if(!objects[m]) {
			continue;
		};
		Object *object = objects[m];

		if(blended && object->getType() == SceneNode::SCENE_NODE_OBJECT_PARTICLE_SYSTEM) {

			GLSystem::Get()->push();
			GLSystem::Get()->multMatrix(object->getTransform());
		
			frustum->Get();
			if(!frustum->isInside(object->getCenter(), object->getRadius())) {
				GLSystem::Get()->pop();
				continue;
			};

			object->drawSubset(0);

			GLSystem::Get()->pop();
		};
	};
_END_USE
};



/*
*/
void Scene::drawLights(bool blended) {
	for(int i = 0; i < visibleLights.size(); i++) {
		if(visibleLights[i]->getType() == SceneNode::SCENE_NODE_LIGHT_POINT) {
			drawPoint((LightPoint*)visibleLights[i], blended);
		};
		
		if(visibleLights[i]->getType() == SceneNode::SCENE_NODE_LIGHT_SPOT) {
			drawSpot((LightSpot*)visibleLights[i], blended);
		};

		if(visibleLights[i]->getType() == SceneNode::SCENE_NODE_LIGHT_DIRECT) {
			drawDirect((LightDirect*)visibleLights[i], blended);
		};
	};
};

/*
*/
void Scene::checkLightsVisibility() {
	visibleLights.clear();
	for(int i = 0; i < lights.size(); i++) {
		if(!lights[i]->enabled) continue;

		if(lights[i]->getType() == SceneNode::SCENE_NODE_LIGHT_POINT) {
			checkPointVisibility((LightPoint*)lights[i]);
		};
		
		if(lights[i]->getType() == SceneNode::SCENE_NODE_LIGHT_SPOT) {
			checkSpotVisibility((LightSpot*)lights[i]);
		};

		if(lights[i]->getType() == SceneNode::SCENE_NODE_LIGHT_DIRECT) {
			visibleLights.push_back(lights[i]);
		};
	};
};

/*
*/
void Scene::getLightsShadowMaps() {
	for(int i = 0; i < visibleLights.size(); i++) {
		if(visibleLights[i]->getType() == SceneNode::SCENE_NODE_LIGHT_POINT) {
			getPointShadowMap((LightPoint*)visibleLights[i]);
		};
		
		if(visibleLights[i]->getType() == SceneNode::SCENE_NODE_LIGHT_SPOT) {
			getSpotShadowMap((LightSpot*)visibleLights[i]);
		};
	};
};





/*
*/
void Scene::drawPoint(LightPoint *light, bool blended) {
	/*
	Draw terrain
	*/
	if(terrain && !blended) {
		Material *mtr = terrain->getMaterial();
		if(mtr && mtr->setPass(Material::POINT)) {
			mtr->setFloat(U_TIME, WindowSystem::Get()->getETime());
			mtr->setFloat(U_LIGHT_IRADIUS, light->getIRadius());
			mtr->setFloat(U_LIGHT_RADIUS, light->getRadius());
			mtr->setVec3(U_LIGHT_COLOR, light->getColor());
			mtr->setVec3(U_LIGHT_POSITION, light->getPosition());
			mtr->setVec3(U_VIEW_POSITION, camera->getPosition());
			mtr->setMat4(U_MVP_TRANSFORM, GLSystem::Get()->getMatrix_MVP());
			mtr->setTexture(U_SHADOW_MAP, light->getShadowMap(), 4);

			frustum->Get();
			
			for(int n = 0; n < terrain->getNumNodes(); n++)	{
				if(!frustum->isInside(terrain->getCenter(n), terrain->getRadius(n))) {
					continue;
				};
				if((light->getPosition() - terrain->getCenter(n)).length() > 
					light->getRadius() + terrain->getRadius(n)) {
					continue;
				};
						
				terrain->drawNode(n, camera->getPosition());
			};

			mtr->unsetTexture(light->getShadowMap(), 4);
			mtr->unsetPass();
		};
	};

	/*
	draw objects
	*/
	for(int m = 0; m < objects.size(); m++) {
		if(!objects[m]) {
			continue;
		};

		Object *object = objects[m];

		GLSystem::Get()->push();
		GLSystem::Get()->multMatrix(object->getTransform());
					
		frustum->Get();
		if(!frustum->isInside(object->getCenter(), object->getRadius())) {
			GLSystem::Get()->pop();
			continue;
		};
		if((light->getPosition() - object->getPosition()).length() > 
			light->getRadius() + object->getRadius()) {
			GLSystem::Get()->pop();
			continue;
		};

		/*
		draw subsets
		*/
		for(int s = 0; s < object->getNumSubsets(); s++) {
			frustum->Get();
			if(!frustum->isInside(object->getCenter(s), object->getRadius(s)))
				continue;
							
			Material *mtr = object->getMaterial(s);
			if(!mtr) continue;
				
			if(blended) {
				if(!mtr->hasBlending()) continue;
			} else {
				if(mtr->hasBlending()) continue;
			};
			if(!mtr->setPass(Material::POINT)) continue;
			mtr->setFloat(U_TIME, WindowSystem::Get()->getETime());
			mtr->setFloat(U_LIGHT_IRADIUS, light->getIRadius());
			mtr->setFloat(U_LIGHT_RADIUS, light->getRadius());
			mtr->setVec3(U_LIGHT_COLOR, light->getColor());
			mtr->setVec3(U_LIGHT_POSITION, light->getPosition());
			mtr->setVec3(U_VIEW_POSITION, camera->getPosition());
			mtr->setMat4(U_MVP_TRANSFORM, GLSystem::Get()->getMatrix_MVP());
			mtr->setMat4(U_WORLD_TRANSFORM, object->getTransform());
			mtr->setTexture(U_SHADOW_MAP, light->getShadowMap(), 4);

			mtr->setAlphaTest();
			
			object->drawSubset(s);

			mtr->unsetAlphaTest();

			mtr->unsetTexture(light->getShadowMap(), 4);
			mtr->unsetPass();
		};
		GLSystem::Get()->pop();
	};
};

/*
*/
void Scene::getPointShadowMap(LightPoint *light) {
	if(!light->getShadows() || !Config::Get()->getInt(CONF_SHADOW_TYPE)) {
		return;
	};
		
	shadowPBO->set();

	GLSystem::Get()->setMatrixMode_Projection();
	GLSystem::Get()->loadMatrix(Mat4::perspective(90, 1, 1, light->getRadius()));
	GLSystem::Get()->setMatrixMode_Modelview();
				
	for(int f = 0; f < 6; f++) {
		shadowPBO->clear();

		GLSystem::Get()->clearColor(Vec3(1.0, 1.0, 1.0));
		GLSystem::Get()->loadMatrix(Mat4::cube(light->getPosition(), f));
							
		/*
		draw objects
		*/
		for(int m = 0; m < objects.size(); m++) {
			if(!objects[m]) {
				continue;
			};

			Object *object = objects[m];

			GLSystem::Get()->push();
			GLSystem::Get()->multMatrix(object->getTransform());
										
			frustum->Get();
			if(!frustum->isInside(object->getCenter(), object->getRadius())) {
				GLSystem::Get()->pop();
				continue;
			};
			if((light->getPosition() - object->getPosition()).length() > 
				light->getRadius() + object->getRadius()) {
				GLSystem::Get()->pop();
				continue;
			};

			/*
			draw subsets
			*/
			for(int s = 0; s < object->getNumSubsets(); s++) {
				Material *mtr = object->getMaterial(s);
				if(!mtr) continue;
				
				frustum->Get();
				if(!frustum->isInside(object->getCenter(s), object->getRadius(s))) {
					continue;
				};
										
				depthPass->setPass(Material::CUSTOM_0);
				depthPass->setMat4(U_MVP_TRANSFORM, GLSystem::Get()->getMatrix_MVP());
				depthPass->setMat4(U_WORLD_TRANSFORM, object->getTransform());
				depthPass->setVec3(U_LIGHT_POSITION, light->getPosition());
				depthPass->setFloat(U_LIGHT_IRADIUS, light->getIRadius());

				mtr->setAlphaTest();

				object->drawSubset(s);

				mtr->unsetAlphaTest();

				depthPass->unsetPass();
			};
			GLSystem::Get()->pop();
		};
		light->getShadowMap()->copy(f);
	};
	shadowPBO->unset();
};

/*
*/
void Scene::drawSpot(LightSpot *light, bool blended) {
	Mat4 projTransform = Mat4::texBias() * 
		Mat4::perspective(light->getFOV(), 1, 1, light->getRadius()) *
		Mat4::lookAt(light->getPosition(), light->getPosition() + light->getOrientation(), Vec3(0, 1, 0));

	/*
	draw terrain
	*/
	if(terrain && !blended) {
		Material *mtr = terrain->getMaterial();
		
		if(mtr && mtr->setPass(Material::SPOT)) {
			mtr->setFloat(U_TIME, WindowSystem::Get()->getETime());
			mtr->setFloat(U_LIGHT_IRADIUS, light->getIRadius());
			mtr->setFloat(U_LIGHT_RADIUS, light->getRadius());
			mtr->setVec3(U_LIGHT_COLOR, light->getColor());
			mtr->setVec3(U_LIGHT_POSITION, light->getPosition());
			mtr->setVec3(U_LIGHT_DIRECTION, light->getOrientation());
			mtr->setVec3(U_VIEW_POSITION, camera->getPosition());
			mtr->setMat4(U_MVP_TRANSFORM, GLSystem::Get()->getMatrix_MVP());
			mtr->setMat4(U_SPOT_TRANSFORM, projTransform);
			mtr->setTexture(U_SHADOW_MAP, light->getShadowMap(), 4);
			mtr->setTexture(U_SPOT_MAP, light->getSpotMap(), 5);

			frustum->Get();

			for(int n = 0; n < terrain->getNumNodes(); n++)	{
				
				if(!frustum->isInside(terrain->getCenter(n), terrain->getRadius(n))) {
					continue;
				};
				if((light->getPosition() - terrain->getCenter(n)).length() > 
					light->getRadius() + terrain->getRadius(n)) {
					continue;
				};

				terrain->drawNode(n, camera->getPosition());
			};

			mtr->unsetTexture(light->getShadowMap(), 4);
			mtr->unsetTexture(light->getSpotMap(), 5);
			mtr->unsetPass();
		};
	};

	/*
	draw objects
	*/
	for(int m = 0; m < objects.size(); m++) {
		if(!objects[m]) {
			continue;
		};

		Object *object = objects[m];

		GLSystem::Get()->push();
		GLSystem::Get()->multMatrix(object->getTransform());
				
		frustum->Get();
		if(!frustum->isInside(object->getCenter(), object->getRadius())) {
			GLSystem::Get()->pop();
			continue;
		};
		if((light->getPosition() - object->getPosition()).length() > 
			light->getRadius() + object->getRadius()) {
			GLSystem::Get()->pop();
			continue;
		};

		/*
		draw subsets
		*/
		for(int s = 0; s < object->getNumSubsets(); s++) {
			frustum->Get();
			if(!frustum->isInside(object->getCenter(s), object->getRadius(s))) {
				continue;
			};

			Material *mtr = object->getMaterial(s);
			if(!mtr) continue;

			//set material params
			if(blended) {
				if(!mtr->hasBlending()) continue;
			} else {
				if(mtr->hasBlending()) continue;
			};
			if(!mtr->setPass(Material::SPOT)) continue;
			mtr->setFloat(U_TIME, WindowSystem::Get()->getETime());
			mtr->setFloat(U_LIGHT_IRADIUS, light->getIRadius());
			mtr->setFloat(U_LIGHT_RADIUS, light->getRadius());
			mtr->setVec3(U_LIGHT_COLOR, light->getColor());
			mtr->setVec3(U_LIGHT_POSITION, light->getPosition());
			mtr->setVec3(U_LIGHT_DIRECTION, light->getOrientation());
			mtr->setVec3(U_VIEW_POSITION, camera->getPosition());
			mtr->setMat4(U_MVP_TRANSFORM, GLSystem::Get()->getMatrix_MVP());
			mtr->setMat4(U_WORLD_TRANSFORM, object->getTransform());
			mtr->setMat4(U_SPOT_TRANSFORM, projTransform);
			mtr->setTexture(U_SHADOW_MAP, light->getShadowMap(), 4);
			mtr->setTexture(U_SPOT_MAP, light->getSpotMap(), 5);

			mtr->setAlphaTest();

			object->drawSubset(s);

			mtr->unsetAlphaTest();

			mtr->unsetTexture(light->getShadowMap(), 4);
			mtr->unsetTexture(light->getSpotMap(), 5);
			mtr->unsetPass();
		};
		GLSystem::Get()->pop();
	};
};

/*
*/
void Scene::getSpotShadowMap(LightSpot *light) {
	if(!light->getShadows() || !Config::Get()->getInt(CONF_SHADOW_TYPE)) {
		return;
	};

	shadowPBO->set();
	shadowPBO->clear();
	GLSystem::Get()->clearColor(Vec3(1.0, 1.0, 1.0));

	GLSystem::Get()->setMatrixMode_Projection();
	GLSystem::Get()->loadMatrix(Mat4::perspective(light->getFOV(), 1, 1, light->getRadius()));
					
	GLSystem::Get()->setMatrixMode_Modelview();
	GLSystem::Get()->loadMatrix(Mat4::lookAt(light->getPosition(), light->getPosition() + light->getOrientation(), Vec3(0, 1, 0)));
									
	//DRAW OBJECTS
	for(int m = 0; m < objects.size(); m++) {
		if(!objects[m]) {
			continue;
		};

		Object *object = objects[m];

		GLSystem::Get()->push();
		GLSystem::Get()->multMatrix(object->getTransform());
											
		frustum->Get();
		if(!frustum->isInside(object->getCenter(), object->getRadius())) {
			GLSystem::Get()->pop();
			continue;
		};
		if((light->getPosition() - object->getPosition()).length() >
			light->getRadius() + object->getRadius()) {
			GLSystem::Get()->pop();
			continue;
		};
			
		//DRAW OBJECT SUBSETS
		for(int s = 0; s < object->getNumSubsets(); s++) {
			Material *mtr = object->getMaterial(s);
			
			if(!mtr) continue;
			
			frustum->Get();
			if(!frustum->isInside(object->getCenter(s), object->getRadius(s))) {
				continue;
			};
									
			depthPass->setPass(Material::CUSTOM_0);
			depthPass->setMat4(U_MVP_TRANSFORM, GLSystem::Get()->getMatrix_MVP());
			depthPass->setMat4(U_WORLD_TRANSFORM, object->getTransform());
			depthPass->setVec3(U_LIGHT_POSITION, light->getPosition());
			depthPass->setFloat(U_LIGHT_IRADIUS, light->getIRadius());

			mtr->setAlphaTest();

			object->drawSubset(s);
				
			mtr->unsetAlphaTest();

			depthPass->unsetPass();
		};
		GLSystem::Get()->pop();
	};

	light->getShadowMap()->copy();
	shadowPBO->unset();
};

/*
*/
void Scene::drawDirect(LightDirect *light, bool blended) {
	//DRAW TERRAIN
	if(terrain && !blended) {
		Material *mtr = terrain->getMaterial();
		
		if(mtr && mtr->setPass(Material::DIRECT)) {
			mtr->setFloat(U_TIME, WindowSystem::Get()->getETime());
			mtr->setVec3(U_LIGHT_COLOR, light->getColor());
			mtr->setVec3(U_LIGHT_DIRECTION, light->getOrientation());
			mtr->setVec3(U_VIEW_POSITION, camera->getPosition());
			mtr->setMat4(U_MVP_TRANSFORM, GLSystem::Get()->getMatrix_MVP());
					
			frustum->Get();
					
			for(int n = 0; n < terrain->getNumNodes(); n++)	{
				if(!frustum->isInside(terrain->getCenter(n), terrain->getRadius(n))) {
					continue;
				};
					
				terrain->drawNode(n, camera->getPosition());
			};

			mtr->unsetPass();
		};
	};

	//DRAW OBJECTS
	for(int m = 0; m < objects.size(); m++) {
		if(!objects[m]) {
			continue;
		};

		Object *object = objects[m];

		GLSystem::Get()->push();
		GLSystem::Get()->multMatrix(object->getTransform());
			
		frustum->Get();
		if(!frustum->isInside(object->getCenter(), object->getRadius())) {
			GLSystem::Get()->pop();
			continue;
		};
		
		//DRAW OBJECT SUBSETS
		for(int s = 0; s < object->getNumSubsets(); s++) {
			if(!frustum->isInside(object->getCenter(s), object->getRadius(s))) {
				continue;
			};
					
			Material *mtr = object->getMaterial(s);
			if(!mtr) continue;

			if(blended) {
				if(!mtr->hasBlending()) continue;
			} else {
				if(mtr->hasBlending()) continue;
			};

			if(!mtr->setPass(Material::DIRECT)) continue;	
			mtr->setFloat(U_TIME, WindowSystem::Get()->getETime());
			mtr->setVec3(U_LIGHT_COLOR, light->getColor());
			mtr->setVec3(U_LIGHT_DIRECTION, light->getOrientation());
			mtr->setVec3(U_VIEW_POSITION, camera->getPosition());
			mtr->setMat4(U_MVP_TRANSFORM, GLSystem::Get()->getMatrix_MVP());
			mtr->setMat4(U_WORLD_TRANSFORM, object->getTransform());

			mtr->setAlphaTest();

			object->drawSubset(s);

			mtr->unsetAlphaTest();
			mtr->unsetPass();
		};
		GLSystem::Get()->pop();
	};
};

/*
*/
void Scene::checkPointVisibility(LightPoint *light) {
	if(light->isVisible()) {
		visibleLights.push_back(light);
	};
};

/*
*/
void Scene::checkSpotVisibility(LightSpot *light) {
	if(light->isVisible()) {
		visibleLights.push_back(light);
	};
};

/*
*/
void Scene::drawWater() {
	if(!water) return;

_USE_NEW_RENDER

	Material *mtr = water->getMaterial();
	if(mtr && mtr->setPass(Material::AMBIENT)) {
		mtr->setFloat(U_TIME, WindowSystem::Get()->getETime());
		mtr->setVec3(U_VIEW_POSITION, camera->getPosition());
		mtr->setMat4(U_MVP_TRANSFORM, GLSystem::Get()->getMatrix_MVP());
		mtr->setMat4(U_VIEWPORT_TRANSFORM, Mat4::texBias() * GLSystem::Get()->getMatrix_MVP());
		mtr->setTexture(U_VIEWPORT_MAP, viewportCopy, 4);
		mtr->setTexture(U_REFLECTION_MAP, reflectionMap, 5);

		water->draw();
		
		mtr->unsetTexture(viewportCopy, 4);
		mtr->unsetTexture(reflectionMap, 5);
		mtr->unsetPass();
	};
_END_USE

_USE_OLD_RENDER
	Material *mtr = water->getMaterial();
	if(mtr) {
		mtr->texture1->set(0);

		GLSystem::Get()->enableBlending(GLSystem::DST_COLOR, GLSystem::SRC_COLOR);
		GLSystem::Get()->depthMask(false);

		water->draw();

		GLSystem::Get()->depthMask(true);
		GLSystem::Get()->disableBlending();

		mtr->texture1->unset(1);
	};
_END_USE
};

/*
*/
void Scene::drawSkydome() {
	if(!skydome) return;

_USE_NEW_RENDER
	GLSystem::Get()->push();
	GLSystem::Get()->multMatrix(Mat4::translate(camera->getPosition()) * Mat4::scale(Vec3(1e4, 1e4, 1e4)));
	GLSystem::Get()->disableCulling();//(GLSystem::FRONT_FACE);
	
	Material *mtr = skydome->getMaterial();
	if(mtr && mtr->setPass(Material::AMBIENT)) {
		mtr->setFloat(U_TIME, WindowSystem::Get()->getETime());
		mtr->setVec3(U_VIEW_POSITION, camera->getPosition());
		mtr->setMat4(U_MVP_TRANSFORM, GLSystem::Get()->getMatrix_MVP());
		
		skydome->draw();

		mtr->unsetPass();
	};
	
	GLSystem::Get()->enableCulling();
	GLSystem::Get()->pop();
_END_USE

_USE_OLD_RENDER
	GLSystem::Get()->push();
	GLSystem::Get()->multMatrix(Mat4::translate(camera->getPosition()) * Mat4::scale(Vec3(1e4, 1e4, 1e4)));
	GLSystem::Get()->disableCulling();//(GLSystem::FRONT_FACE);

	Material *mtr = skydome->getMaterial();
	if(mtr) {
		mtr->texture0->set(0);
		
		glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP_ARB);
		glTexGenf(GL_T, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP_ARB);
		glTexGenf(GL_R, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP_ARB);
		
		glEnable(GL_TEXTURE_GEN_S);
		glEnable(GL_TEXTURE_GEN_T);
		glEnable(GL_TEXTURE_GEN_R);

		mtr->texture0->set(0);
		skydome->draw();

		glDisable(GL_TEXTURE_GEN_S);
		glDisable(GL_TEXTURE_GEN_T);
		glDisable(GL_TEXTURE_GEN_R);
		mtr->texture0->unset(0);
	};

	GLSystem::Get()->enableCulling();
	GLSystem::Get()->pop();
_END_USE
};

/*
*/
void Scene::update() {
	//---------update-camera-----------------------------------
	camera->update();

	//---------update-objects-----------------------------------
	for(int i = 0; i < objects.size(); i++) {
		objects[i]->update();
	};

	//---------update-OpenAL-listiner-----------------------------------
	ALSystem::Get()->setListener(camera->getPosition(), camera->getOrientation());

	//---------set-default-forces-----------------------------------
	for(int k = 0; k < objects.size(); k++) {
		if(objects[k]->getPhysBody()) {
			//gravity
			objects[k]->getPhysBody()->addTorque(gravity);
			
			//water-physics
			if(water) {
				Object *object = objects[k];
				float r = objects[k]->getRadius();
				float y = objects[k]->getPosition().y;
				float k_underwater = (y + r - water->getDepth()) / (2.0*r);
				k_underwater = 0.5 - k_underwater;
				k_underwater = Math::clamp(k_underwater, 0.0, 1.0);
								
				if(k_underwater) {
					float volume = 4.0/3.0 * PI * r * r * r;
					float density = object->getPhysBody()->getMass() / volume;
					object->getPhysBody()->addForce(Vec3(0, 1, 0) / density * k_underwater);
				};
			};
		};
		
		if(camera->getPhysBody()) {
			camera->getPhysBody()->addTorque(gravity);
		};
	};
};

/*
*/
void Scene::draw() {
_USE_NEW_RENDER
	//---------set-camera-matrices--------------------------------
	GLSystem::Get()->setMatrixMode_Projection();
	GLSystem::Get()->loadMatrix(camera->getProjection());

	GLSystem::Get()->setMatrixMode_Modelview();
	GLSystem::Get()->loadMatrix(camera->getView());

	//----draw-skybox----------------------------------
	drawSkydome();

	//---------draw-ambient-without-blending-----------------------------------
	drawAmbient(false);
	
	//---------check-lights-visibility-----------------------------------
	checkLightsVisibility();

	//---------get-lights-shadowmaps-----------------------------------
	getLightsShadowMaps();

	//---------set-multipass-----------------------------------
	GLSystem::Get()->enableBlending(GLSystem::ONE, GLSystem::ONE);
	GLSystem::Get()->depthMask(false);

	//---------draw-lights-----------------------------------
	drawLights(false);

	//---------unset-multipass-----------------------------------
	GLSystem::Get()->depthMask(true);
	GLSystem::Get()->disableBlending();

	//---------draw-ambient-with-blending-----------------------------------
	drawAmbient(true);

	//---------set-multipass-----------------------------------
	GLSystem::Get()->enableBlending(GLSystem::ONE, GLSystem::ONE);
	GLSystem::Get()->depthMask(false);

	//---------draw-lights-----------------------------------
	drawLights(true);

	//---------unset-multipass-----------------------------------
	GLSystem::Get()->depthMask(true);
	GLSystem::Get()->disableBlending();

	//---------check-water-visibility-----------------------------
	bool waterVisible = true;
	if(water) {
		waterVisible = water->isVisible();
	};

	if(Config::Get()->getBool(CONF_REFLECTIONS) && water && waterVisible) {
		//---------draw-scene-into-viewport-copy-------------------------------
		viewportPBO->set();
		viewportPBO->clear();

		GLSystem::Get()->cullFunc(GLSystem::CW);

		//---------set-camera--------------------------------
		Vec4 plain;

		if(camera->getPosition().y > water->getDepth())
			plain = Vec4(Vec3(0, -1, 0) * camera->view.getRotation(), water->getDepth() - camera->getPosition().y);
		else
			plain = Vec4(Vec3(0, 1, 0) * camera->view.getRotation(), -water->getDepth() + camera->getPosition().y);
		
		GLSystem::Get()->setMatrixMode_Projection();
		GLSystem::Get()->loadMatrix(Mat4::reflectProjection(camera->getProjection(), plain));

		GLSystem::Get()->setMatrixMode_Modelview();
		GLSystem::Get()->loadMatrix(camera->getView() * Mat4::reflect(Vec4(0, 1, 0, -water->getDepth())));

		//----draw-sky----------------------------------
		drawSkydome();
		
		//matViewPosition = (camera->getTransform() * Mat4::reflect(Vec4(0, 1, 0, -water->getDepth()))).getTranslation();

		//---------draw-ambient-----------------------------------
		drawAmbient(false);
			
		GLSystem::Get()->enableBlending(GLSystem::ONE, GLSystem::ONE);
		GLSystem::Get()->depthMask(false);

		//---------draw-lights-----------------------------------
		drawLights(false);

		//---------draw-ambient-with-blending-----------------------------------
		drawAmbient(true);
		
		//---------draw-lights-----------------------------------
		drawLights(true);

		//---------unset-multipass-----------------------------------
		GLSystem::Get()->depthMask(true);
		GLSystem::Get()->disableBlending();

		GLSystem::Get()->cullFunc(GLSystem::CCW);

		//---------unset-fbo-------------------------------
		reflectionMap->copy();
		viewportPBO->unset();
	};


	
	//---------draw-scene-into-viewport-copy-------------------------------
	viewportPBO->set();
	viewportPBO->clear();

	//---------set-camera--------------------------------
	GLSystem::Get()->setMatrixMode_Projection();
	GLSystem::Get()->loadMatrix(camera->getProjection());

	GLSystem::Get()->setMatrixMode_Modelview();
	GLSystem::Get()->loadMatrix(camera->getView());

	//----draw-sky----------------------------------
	drawSkydome();

	//---------draw-ambient-----------------------------------
	drawAmbient(false);
		
	GLSystem::Get()->enableBlending(GLSystem::ONE, GLSystem::ONE);
	GLSystem::Get()->depthMask(false);

	//---------draw-lights-----------------------------------
	drawLights(false);

	//---------draw-ambient-with-blending-----------------------------------
	drawAmbient(true);
	
	//---------draw-lights-----------------------------------
	drawLights(true);

	//---------unset-multipass-----------------------------------
	GLSystem::Get()->depthMask(true);
	GLSystem::Get()->disableBlending();

	viewportCopy->copy();

	//---------draw-water------------------------------
	if(water && waterVisible) {
		drawWater();
	};

	//---------unset-fbo-------------------------------
	viewportCopy->copy();
	viewportPBO->unset();




	//---------draw-water-------------------------------
	GLSystem::Get()->setMatrixMode_Projection();
	GLSystem::Get()->loadMatrix(camera->getProjection());

	GLSystem::Get()->setMatrixMode_Modelview();
	GLSystem::Get()->loadMatrix(camera->getView());

	//---------draw-water------------------------------
	if(water && waterVisible) {
		drawWater();
	};

	//---------hdr-staff-------------------------------
	if(Config::Get()->getBool(CONF_HDR)) {
		//---------bright-pass--------------------------------
		viewportPBO->set();
		viewportPBO->clear();

		GLSystem::Get()->enable2d(true);
		
		hdr->setPass(Material::CUSTOM_1);
		hdr->setTexture(U_VIEWPORT_MAP, viewportCopy, 4);
		
		GLSystem::Get()->drawRect(0, 0, 1, 1, 0, 1, 1, 0);

		hdr->unsetTexture(viewportCopy, 4);
		hdr->unsetPass();

		GLSystem::Get()->enable3d();

		viewportCopy_brightPass->copy();
		viewportPBO->clear();

		GLSystem::Get()->enable2d(true);
		
		hdr->setPass(Material::CUSTOM_0);
		hdr->setTexture(U_VIEWPORT_MAP, viewportCopy_brightPass, 4);

		GLSystem::Get()->drawRect(0, 0, 1, 1, 0, 1, 1, 0);
		
		hdr->unsetTexture(viewportCopy_brightPass, 4);
		hdr->unsetPass();

		GLSystem::Get()->enable3d();

		viewportCopy_brightPass_blured->copy();
		viewportPBO->unset();

		//---------draw-bloom-------------------------------
		GLSystem::Get()->enable2d(true);
		GLSystem::Get()->enableBlending(GLSystem::ONE, GLSystem::ONE);

		hdr->setPass(Material::CUSTOM_0);
		hdr->setTexture(U_VIEWPORT_MAP, viewportCopy_brightPass_blured, 4);

		GLSystem::Get()->drawRect(0, 0, 1, 1, 0, 1, 1, 0);
		
		hdr->unsetTexture(viewportCopy_brightPass_blured, 4);
		hdr->unsetPass();

		GLSystem::Get()->disableBlending();
		GLSystem::Get()->enable3d();
	};
_END_USE

_USE_OLD_RENDER
	if(Config::Get()->getBool(CONF_REFLECTIONS) && water) {
		//---------draw-scene-into-viewport-copy-------------------------------
		viewportPBO->set();
		viewportPBO->clear();

		GLSystem::Get()->cullFunc(GLSystem::CW);

		//---------set-camera--------------------------------
		Vec4 plain;

		if(camera->getPosition().y > water->getDepth())
			plain = Vec4(Vec3(0, -1, 0) * camera->view.getRotation(), water->getDepth() - camera->getPosition().y);
		else
			plain = Vec4(Vec3(0, 1, 0) * camera->view.getRotation(), -water->getDepth() + camera->getPosition().y);
		
		GLSystem::Get()->setMatrixMode_Projection();
		GLSystem::Get()->loadMatrix(Mat4::reflectProjection(camera->getProjection(), plain));

		GLSystem::Get()->setMatrixMode_Modelview();
		GLSystem::Get()->loadMatrix(camera->getView() * Mat4::reflect(Vec4(0, 1, 0, -water->getDepth())));

		drawSkydome();
		//---------draw-ambient-without-blending-----------------------------------
		glEnable(GL_LIGHTING);
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient);
		
		for(int l = 0; l < lights.size(); l++) {
			if(lights[l]->getType() == SceneNode::SCENE_NODE_LIGHT_POINT) {
				LightPoint *light = (LightPoint*)lights[l];
				glEnable(GL_LIGHT0 + l);
				glLightfv(GL_LIGHT0 + l, GL_POSITION, Vec4(light->getPosition(), 1.0));
				glLightfv(GL_LIGHT0 + l, GL_DIFFUSE, light->getColor());
				glLightfv(GL_LIGHT0 + l, GL_AMBIENT, ambient);
			};

			if(lights[l]->getType() == SceneNode::SCENE_NODE_LIGHT_SPOT) {
				LightSpot *light = (LightSpot*)lights[l];
				glEnable(GL_LIGHT0 + l);
				glLightfv(GL_LIGHT0 + l, GL_POSITION, Vec4(light->getOrientation(), 0.0));
				glLightfv(GL_LIGHT0 + l, GL_SPOT_DIRECTION, Vec4(light->getOrientation(), 0.0));
				glLightf(GL_LIGHT0 + l, GL_SPOT_CUTOFF, light->getFOV());
				glLightfv(GL_LIGHT0 + l, GL_DIFFUSE, light->getColor());
				glLightfv(GL_LIGHT0 + l, GL_AMBIENT, ambient);
			};

			if(lights[l]->getType() == SceneNode::SCENE_NODE_LIGHT_DIRECT) {
				LightDirect *light = (LightDirect*)lights[l];
				glEnable(GL_LIGHT0 + l);
				glLightfv(GL_LIGHT0 + l, GL_POSITION, Vec4(light->getOrientation(), 0.0));
				glLightfv(GL_LIGHT0 + l, GL_DIFFUSE, light->getColor());
				glLightfv(GL_LIGHT0 + l, GL_AMBIENT, ambient);
			};
		};
		drawAmbient(false);
		drawAmbient(true);
		glDisable(GL_LIGHTING);

		GLSystem::Get()->cullFunc(GLSystem::CCW);

		//---------unset-fbo-------------------------------
		reflectionMap->copy();
		viewportPBO->unset();
	};

	//---------set-camera-matrices--------------------------------
	GLSystem::Get()->setMatrixMode_Projection();
	GLSystem::Get()->loadMatrix(camera->getProjection());

	GLSystem::Get()->setMatrixMode_Modelview();
	GLSystem::Get()->loadMatrix(camera->getView());

	drawSkydome();
	//---------draw-ambient-without-blending-----------------------------------
	glEnable(GL_LIGHTING);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient);
	
	for(int l = 0; l < lights.size(); l++) {
		if(lights[l]->getType() == SceneNode::SCENE_NODE_LIGHT_POINT) {
			LightPoint *light = (LightPoint*)lights[l];
			glEnable(GL_LIGHT0 + l);
			glLightfv(GL_LIGHT0 + l, GL_POSITION, Vec4(light->getPosition(), 1.0));
			glLightfv(GL_LIGHT0 + l, GL_DIFFUSE, light->getColor());
			glLightfv(GL_LIGHT0 + l, GL_AMBIENT, ambient);
		};

		if(lights[l]->getType() == SceneNode::SCENE_NODE_LIGHT_SPOT) {
			LightSpot *light = (LightSpot*)lights[l];
			glEnable(GL_LIGHT0 + l);
			glLightfv(GL_LIGHT0 + l, GL_POSITION, Vec4(light->getOrientation(), 0.0));
			glLightfv(GL_LIGHT0 + l, GL_SPOT_DIRECTION, Vec4(light->getOrientation(), 0.0));
			glLightf(GL_LIGHT0 + l, GL_SPOT_CUTOFF, light->getFOV());
			glLightfv(GL_LIGHT0 + l, GL_DIFFUSE, light->getColor());
			glLightfv(GL_LIGHT0 + l, GL_AMBIENT, ambient);
		};

		if(lights[l]->getType() == SceneNode::SCENE_NODE_LIGHT_DIRECT) {
			LightDirect *light = (LightDirect*)lights[l];
			glEnable(GL_LIGHT0 + l);
			glLightfv(GL_LIGHT0 + l, GL_POSITION, Vec4(light->getOrientation(), 0.0));
			glLightfv(GL_LIGHT0 + l, GL_DIFFUSE, light->getColor());
			glLightfv(GL_LIGHT0 + l, GL_AMBIENT, ambient);
		};
	};
	drawAmbient(false);
	drawAmbient(true);
	glDisable(GL_LIGHTING);

	reflectionMap->set(1);
	glEnable(GL_TEXTURE_GEN_S);
	glEnable(GL_TEXTURE_GEN_T);
	glEnable(GL_TEXTURE_GEN_R);
	glEnable(GL_TEXTURE_GEN_Q);

	glTexGenfv(GL_S, GL_EYE_PLANE, Vec4(1, 0, 0, 0));
	glTexGenfv(GL_T, GL_EYE_PLANE, Vec4(0, 1, 0, 0));
	glTexGenfv(GL_R, GL_EYE_PLANE, Vec4(0, 0, 1, 0));
	glTexGenfv(GL_Q, GL_EYE_PLANE, Vec4(0, 0, 0, 1));

	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    glMatrixMode  ( GL_TEXTURE );
    glPushMatrix  ();

    glLoadIdentity ();
    glTranslatef   ( 0.5, 0.5, 0.5 );     
	glScalef       ( 0.5, 0.5, 0.5 );
    glMultMatrixf  ( camera->getProjection() );
    glMultMatrixf  ( camera->getView() );

	drawWater();

	glMatrixMode ( GL_TEXTURE );
    glPopMatrix  ();

    glMatrixMode ( GL_MODELVIEW );
_END_USE
};

};
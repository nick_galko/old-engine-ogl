/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//***************************************************************************
#include "EngineAPI.h"
//***************************************************************************

namespace Vega {

	/*
	*/
	Material *Material::Create(const String &path) {
		Material *material = new Material();

		//set NULLS
		material->ambientShader = NULL;
		material->pointShader = NULL;
		material->spotShader = NULL;
		material->directShader = NULL;

		material->texture0 = NULL;
		material->texture1 = NULL;
		material->texture2 = NULL;
		material->texture3 = NULL;

		//open file
		FStream *file = FStream::Create(path, FStream::READ_TEXT);

		//Check if exist
		if (!file) {
			Error("Material::Material() error: material file '%s' not found", path);
			return NULL;
		}

		//begin loading
		while (!file->eof()) {
			String line = file->gets();

			material->blending = false;
			material->alphaTest = false;

			//blending
			if (line.getWord(1) == "blending") {
				material->blending = true;

				String s = line.getWord(2);
				String d = line.getWord(3);

				if (s == "ONE") {
					material->src = GLSystem::ONE;
				}
				else if (s == "ZERO") {
					material->src = GLSystem::ZERO;
				}
				else if (s == "SRC_COLOR") {
					material->src = GLSystem::SRC_COLOR;
				}
				else if (s == "DST_COLOR") {
					material->src = GLSystem::DST_COLOR;
				}
				else if (s == "SRC_ALPHA") {
					material->src = GLSystem::SRC_ALPHA;
				}
				else if (s == "DST_ALPHA") {
					material->src = GLSystem::DST_ALPHA;
				}
				else if (s == "ONE_MINUS_SRC_COLOR") {
					material->src = GLSystem::ONE_MINUS_SRC_COLOR;
				}
				else if (s == "ONE_MINUS_DST_COLOR") {
					material->src = GLSystem::ONE_MINUS_DST_COLOR;
				}
				else if (s == "ONE_MINUS_SRC_ALPHA") {
					material->src = GLSystem::ONE_MINUS_SRC_ALPHA;
				}
				else if (s == "ONE_MINUS_DST_ALPHA") {
					material->src = GLSystem::ONE_MINUS_DST_ALPHA;
				}
				else {
					material->blending = false;
				}

				if (d == "ONE") {
					material->dst = GLSystem::ONE;
				}
				else if (d == "ZERO") {
					material->dst = GLSystem::ZERO;
				}
				else if (d == "SRC_COLOR") {
					material->dst = GLSystem::SRC_COLOR;
				}
				else if (d == "DST_COLOR") {
					material->dst = GLSystem::DST_COLOR;
				}
				else if (d == "SRC_ALPHA") {
					material->dst = GLSystem::SRC_ALPHA;
				}
				else if (d == "DST_ALPHA") {
					material->dst = GLSystem::DST_ALPHA;
				}
				else if (d == "ONE_MINUS_SRC_COLOR") {
					material->dst = GLSystem::ONE_MINUS_SRC_COLOR;
				}
				else if (d == "ONE_MINUS_DST_COLOR") {
					material->dst = GLSystem::ONE_MINUS_DST_COLOR;
				}
				else if (d == "ONE_MINUS_SRC_ALPHA") {
					material->dst = GLSystem::ONE_MINUS_SRC_ALPHA;
				}
				else if (d == "ONE_MINUS_DST_ALPHA") {
					material->dst = GLSystem::ONE_MINUS_DST_ALPHA;
				}
				else {
					material->blending = false;
				}
			}

			//alpha test
			if (line.getWord(1) == "alpha_test") {
				material->alphaTest = true;

				String f = line.getWord(2);

				if (f == "NEVER") {
					material->alphaFunc = GLSystem::NEVER;
				}
				else if (f == "LESS") {
					material->alphaFunc = GLSystem::LESS;
				}
				else if (f == "EQUAL") {
					material->alphaFunc = GLSystem::EQUAL;
				}
				else if (f == "LEQUAL") {
					material->alphaFunc = GLSystem::LEQUAL;
				}
				else if (f == "GREATER") {
					material->alphaFunc = GLSystem::GREATER;
				}
				else if (f == "NOTEQUAL") {
					material->alphaFunc = GLSystem::NOTEQUAL;
				}
				else if (f == "GEQUAL") {
					material->alphaFunc = GLSystem::GEQUAL;
				}
				else if (f == "ALWAYS") {
					material->alphaFunc = GLSystem::ALWAYS;
				}
				else {
					material->alphaTest = false;
				}

				material->alphaRef = line.getWord(3).toFloat();
			}

			//shaders
			if (line.getWord(1) == "ambient_shader") {
				String path = line.getQuotedWord(2);
				material->ambientShader = Cache::Get()->loadShader(path, "");
			}

			if (line.getWord(1) == "point_shader") {
				String path = line.getQuotedWord(2);
				material->pointShader = Cache::Get()->loadShader(path, "");
			}

			if (line.getWord(1) == "spot_shader") {
				String path = line.getQuotedWord(2);
				material->spotShader = Cache::Get()->loadShader(path, "");
			}

			if (line.getWord(1) == "direct_shader") {
				String path = line.getQuotedWord(2);
				material->directShader = Cache::Get()->loadShader(path, "");
			}

			if (line.getWord(1) == "custom_shader_0") {
				String path = line.getQuotedWord(2);
				material->custom0 = Cache::Get()->loadShader(path, "");
			}

			if (line.getWord(1) == "custom_shader_1") {
				String path = line.getQuotedWord(2);
				material->custom1 = Cache::Get()->loadShader(path, "");
			}

			//material parameters
			if (line.getWord(1) == "param_0") {
				material->param0.x = line.getWord(2).toFloat();
				material->param0.y = line.getWord(3).toFloat();
				material->param0.z = line.getWord(4).toFloat();
				material->param0.w = line.getWord(5).toFloat();
			}

			if (line.getWord(1) == "param_1") {
				material->param1.x = line.getWord(2).toFloat();
				material->param1.y = line.getWord(3).toFloat();
				material->param1.z = line.getWord(4).toFloat();
				material->param1.w = line.getWord(5).toFloat();
			}

			//texture_0
			if (line.getWord(1) == "texture_0") {
				String word = line.getWord(2);
				String path = line.getQuotedWord(3);

				if (word == "2D") {
					if (path == "normal_map") {
						path = line.getQuotedWord(4);
						ILImage *image = ILImage::Create2d(path);
						image->toNormalMap(4);
						material->texture0 = Cache::Get()->loadTexture2d(image, path + "_normal_map");
						delete image;
					}
					else {
						material->texture0 = Cache::Get()->loadTexture2d(path);
					}
				}

				if (word == "CUBE") {
					material->texture0 = Cache::Get()->loadTextureCube(path);
					material->texture0->setWrap(GLTexture::CLAMP_TO_EDGE);
				}
			}

			//texture_1
			if (line.getWord(1) == "texture_1") {
				String word = line.getWord(2);
				String path = line.getQuotedWord(3);

				if (word == "2D") {
					if (path == "normal_map") {
						path = line.getQuotedWord(4);
						ILImage *image = ILImage::Create2d(path);
						image->toNormalMap(4);
						material->texture1 = Cache::Get()->loadTexture2d(image, path + "_normal_map");
						delete image;
					}
					else {
						material->texture1 = Cache::Get()->loadTexture2d(path);
					}
				}

				if (word == "CUBE") {
					material->texture1 = Cache::Get()->loadTextureCube(path);
					material->texture1->setWrap(GLTexture::CLAMP_TO_EDGE);
				}
			}

			//texture_2
			if (line.getWord(1) == "texture_2") {
				String word = line.getWord(2);
				String path = line.getQuotedWord(3);

				if (word == "2D") {
					if (path == "normal_map") {
						path = line.getQuotedWord(4);
						ILImage *image = ILImage::Create2d(path);
						image->toNormalMap(4);
						material->texture2 = Cache::Get()->loadTexture2d(image, path + "_normal_map");
						delete image;
					}
					else {
						material->texture2 = Cache::Get()->loadTexture2d(path);
					}
				}

				if (word == "CUBE") {
					material->texture2 = Cache::Get()->loadTextureCube(path);
					material->texture2->setWrap(GLTexture::CLAMP_TO_EDGE);
				}
			}

			//texture_3
			if (line.getWord(1) == "texture_3") {
				String word = line.getWord(2);
				String path = line.getQuotedWord(3);

				if (word == "2D") {
					if (path == "normal_map") {
						path = line.getQuotedWord(4);
						ILImage *image = ILImage::Create2d(path);
						image->toNormalMap(4);
						material->texture3 = Cache::Get()->loadTexture2d(image, path + "_normal_map");
						delete image;
					}
					else {
						material->texture3 = Cache::Get()->loadTexture2d(path);
					}
				}

				if (word == "CUBE") {
					material->texture3 = Cache::Get()->loadTextureCube(path);
					material->texture3->setWrap(GLTexture::CLAMP_TO_EDGE);
				}
			}
		}
		file->Destroy();

		material->path = path;
		return material;
	}

	/*
	*/
	void Material::Destroy() {
		if (ambientShader) Cache::Get()->deleteShader(ambientShader);
		if (pointShader) Cache::Get()->deleteShader(pointShader);
		if (spotShader) Cache::Get()->deleteShader(spotShader);
		if (directShader) Cache::Get()->deleteShader(directShader);
		if (custom0) Cache::Get()->deleteShader(custom0);
		if (custom1) Cache::Get()->deleteShader(custom1);

		if (texture0) Cache::Get()->deleteTexture(texture0);
		if (texture1) Cache::Get()->deleteTexture(texture1);
		if (texture2) Cache::Get()->deleteTexture(texture2);
		if (texture3) Cache::Get()->deleteTexture(texture3);

		delete this;
	}

	/*
	*/
	bool Material::setPass(PassType pass) {
		currentShader = NULL;

		//ambient pass
		if (pass == AMBIENT) {
			currentShader = ambientShader;
		}

		//point pass
		if (pass == POINT) {
			currentShader = pointShader;
		}

		//spot pass
		if (pass == SPOT) {
			currentShader = spotShader;
		}

		//direct pass
		if (pass == DIRECT) {
			currentShader = directShader;
		}

		//direct pass
		if (pass == CUSTOM_0) {
			currentShader = custom0;
		}

		//direct pass
		if (pass == CUSTOM_1) {
			currentShader = custom1;
		}

		if (!currentShader) {
			return false;
		}

		//send default params
		currentShader->set();
		currentShader->sendVec4("u_material_param_0", param0);
		currentShader->sendVec4("u_material_param_1", param1);

		//textures
		if (texture0) {
			texture0->set(0);
			currentShader->sendInt("u_texture_0", 0);
		}

		if (texture1) {
			texture1->set(1);
			currentShader->sendInt("u_texture_1", 1);
		}

		if (texture2) {
			texture2->set(2);
			currentShader->sendInt("u_texture_2", 2);
		}

		if (texture3) {
			texture3->set(3);
			currentShader->sendInt("u_texture_3", 3);
		}

		return true;
	}

	/*
	*/
	void Material::unsetPass() {
		if (texture0) {
			texture0->unset(0);
		}

		if (texture1) {
			texture1->unset(1);
		}

		if (texture2) {
			texture2->unset(2);
		}

		if (texture3) {
			texture3->unset(3);
		}

		if (currentShader) {
			currentShader->unset();
		}

		currentShader = NULL;
	}

	/*
	*/
	void Material::setMat4(const String &name, const Mat4 &value) {
		if (currentShader) currentShader->sendMat4(name, value);
	}

	/*
	*/
	void Material::setVec4(const String &name, const Vec4 &value) {
		if (currentShader) currentShader->sendVec4(name, value);
	}

	/*
	*/
	void Material::setVec3(const String &name, const Vec3 &value) {
		if (currentShader) currentShader->sendVec3(name, value);
	}

	/*
	*/
	void Material::setVec2(const String &name, const Vec2 &value) {
		if (currentShader) currentShader->sendVec2(name, value);
	}

	/*
	*/
	void Material::setFloat(const String &name, float value) {
		if (currentShader) currentShader->sendFloat(name, value);
	}

	/*
	*/
	void Material::setTexture(const String &name, GLTexture *texture, int unit) {
		texture->set(unit);
		if (currentShader) currentShader->sendInt(name, unit);
	}

	/*
	*/
	void Material::unsetTexture(GLTexture *texture, int unit) {
		texture->unset(unit);
	}

	/*
	*/
	bool Material::hasBlending() {
		return blending;
	}

	/*
	*/
	void Material::setBlending() {
		if (blending) {
			GLSystem::Get()->depthMask(false);
			GLSystem::Get()->enableBlending(src, dst);
		}
	}

	/*
	*/
	void Material::unsetBlending() {
		if (blending) {
			GLSystem::Get()->disableBlending();
			GLSystem::Get()->depthMask(true);
		}
	}

	/*
	*/
	void Material::setAlphaTest() {
		if (alphaTest) {
			GLSystem::Get()->disableCulling();
			GLSystem::Get()->enableAlphaTest(alphaFunc, alphaRef);
		}
	}


	/*
	*/
	void Material::unsetAlphaTest() {
		if (alphaTest) {
			GLSystem::Get()->disableAlphaTest();
			GLSystem::Get()->enableCulling();
		}
	}

	/*
	*/
	const String &Material::getPath() {
		return path;
	}

}










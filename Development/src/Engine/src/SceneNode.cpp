/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//************************************
#include "EngineAPI.h"
//************************************

namespace Vega {

	/*
	Seialize
	*/
	void SceneNode::serialize(FStream *file) {
	}

	void SceneNode::deserialize(FStream *file) {
	}

	/*
	Name
	*/
	void SceneNode::setName(const String &name) {
		this->name = name;
	}

	const String &SceneNode::getName() {
		return name;
	}

	/*
	Parent/children
	*/
	void SceneNode::attach(SceneNode *node, const Mat4 &offset) {
		node->parent = this;
		node->offset = offset;
		children.push_back(node);
	}

	void SceneNode::deattach(SceneNode *node) {
		for (int i = 0; i < children.size(); i++) {
			if (node == children[i]) {
				children[i]->parent = NULL;
				children[i] = NULL;
			}
		}
	}

	void SceneNode::attachTo(SceneNode *node, const Mat4 &offset) {
		this->offset = offset;
		this->parent = node;
		node->children.push_back(this);
	}

	void SceneNode::deattachFrom(SceneNode *node) {
		for (int i = 0; i < node->children.size(); i++) {
			if (this == node->children[i]) {
				node->children[i]->parent = NULL;
				node->children[i] = NULL;
			}
		}
	}

	/*
	Transforms
	*/
	void SceneNode::setTransform(const Mat4 &transform) {
		this->transform = transform;
	}

	const Mat4 &SceneNode::getTransform() {
		if (parent) {
			transform = parent->transform * offset;
		}
		return transform;
	}

	Vec3 SceneNode::getPosition() {
		if (parent) {
			transform = parent->transform * offset;
		}
		return Vec3(transform.e[12], transform.e[13], transform.e[14]);
	}

	Vec3 SceneNode::getOrientation() {
		if (parent) {
			transform = parent->transform * offset;
		}
		return Vec3(0, 0, 1) * transform.getRotation();
	}
}
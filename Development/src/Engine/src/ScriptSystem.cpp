/***************************************************************************
*   Copyright (C) 2014 by Nick Galko             *
*   nick.galko@vegaengine.com                    *
*   This is a part of work done by Nick Galko.   *
*   If you want to use it, please contact me.    *
***************************************************************************/

//***************************************************
#include "EngineAPI.h"
//***************************************************
#include "../../../Externals/luabind/luabind/luabind.hpp"
#include "../../../Externals/luabind/luabind/operator.hpp"
#include "../../../Externals/lua/src/lua.hpp"
//***************************************************

namespace Vega {

	/*
	*/
	ScriptSystem::ScriptSystem()
		:mLuaState(nullptr)
	{}

	/*
	*/
	ScriptSystem::~ScriptSystem()	{
		lua_close(mLuaState);
	}

	/*
	*/
	void ScriptSystem::Initialize()
	{
		using namespace luabind;
		// Create a new lua state
		mLuaState = luaL_newstate();
		luaL_openlibs(mLuaState);

		// Connect LuaBind to this lua state
		open(mLuaState);
		CoreMathBinding();
	}

	void ScriptSystem::CoreMathBinding()
	{
		using namespace luabind;
		using namespace Vega;

		module(mLuaState)
			[
				//Math
				class_<Math>("Math")
				.def("clamp", &Math::clamp)
				.def("angleBetweenVec", &Math::angleBetweenVec)
				.def("insidePolygon", &Math::insidePolygon)
				.def("intersectPlaneByRay", &Math::intersectPlaneByRay)
				.def("intersectPolygonByRay", &Math::intersectPolygonByRay)
				.def("intersectSphereByRay", &Math::intersectSphereByRay)

				//Vec2
				, class_<Vec2>("Vec2")
				.def(constructor<>())
				.def(constructor<float, float>())
				.def(constructor<const Vec2 &>())
				.def("normalize", &Vec2::normalize)
				.def("dot", &Vec2::dot)
				.def("length", &Vec2::length)
				//operators
				.def(self - Vec2())
				.def(self + Vec2())
				.def(const_self / const_self)
				.def(const_self / other<float>())
				.def(other<float>() / const_self)
				.def(const_self * other<float>())
				.def(const_self * Vec2())//����������,��� ���������
				.def(const_self + const_self)
				.def(const_self - const_self)
				//Added:Right?
				.def(self == const_self)
				.def(const_self == const_self)
				//Vec3
				, class_<Vec3>("Vec3")
				.def(constructor<>())
				.def(constructor<float, float, float>())
				.def(constructor<const Vec3 &>())
				.def(constructor<const Vec4 &>())
				.def("normalize", &Vec3::normalize)
				.def("dot", &Vec3::dot)
				.def("length", &Vec3::length)
				.def("cross", &Vec3::cross)
				//operators
				.def(self - Vec3())
				.def(self + Vec3())
				.def(const_self / const_self)
				.def(const_self / other<float>())
				.def(other<float>() / const_self)
				.def(const_self * other<float>())
				.def(const_self * Vec3())//����������,��� ���������
				.def(const_self + const_self)
				.def(const_self - const_self)
				//Added:Right?
				.def(self == const_self)
				.def(const_self == const_self)
				//Vec4
				, class_<Vec4>("Vec4")
				.def(constructor<>())
				.def(constructor<float, float, float,float>())
				.def(constructor<const Vec4 &>())
				.def(constructor<const Vec3 &>())
				.def(constructor<const Vec3 &,float>())
				.def("normalize", &Vec4::normalize)
				.def("dot", &Vec4::dot)
				.def("length", &Vec4::length)
				//operators
				.def(self - Vec4())
				.def(self + Vec4())
				.def(const_self / const_self)
				.def(const_self / other<float>())
				.def(other<float>() / const_self)
				.def(const_self * other<float>())
				.def(const_self * Vec4())//����������,��� ���������
				.def(const_self + const_self)
				.def(const_self - const_self)
				//Added:Right?
				.def(self == const_self)
				.def(const_self == const_self)
				//TBNComputer
				, class_<TBNComputer>("TBNComputer")
				.def("computeN", &TBNComputer::computeN)
				.def("computeTBN", &TBNComputer::computeTBN)
			];

		/*
		Vec2& operator=(const Vec2 &in);

		float& operator[](int index);
		float operator[](int index) const;

		operator float*();
		operator const float*() const;

		Not Exist in LUA
		Vec2& operator+=(const Vec2 &v);
		Vec2& operator-=(const Vec2 &v);
		Vec2& operator*=(const Vec2 &v);
		Vec2& operator/=(const Vec2 &v);

		bool operator!=(const Vec2 &v) const;


		*/
	}
}
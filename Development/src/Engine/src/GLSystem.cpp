/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//***************************************************************************
#include "EngineAPI.h"
//***************************************************************************

namespace Vega {


	/*
	*/
	GLSystem::GLSystem() {}

	/*
	*/
	GLSystem *GLSystem::Get() {
			return GetEngine()->glSystem;
	}

	/*
	*/
	void GLSystem::Initialize() {

		LogHeader("GLSystem::Initialize()");

		LogPrintf("Vendor: %s", getVendor().c_str());
		LogPrintf("Renderer: %s", getRenderer().c_str());
		LogPrintf("Version: %s", getVersion().c_str());
		LogPrintf("Extensions: %s", getExtensions().c_str());
		LogPrintf("Texture units: %i", getNumTexUnits());
		LogPrintf("Max anisotropy: %i", getMaxAniso());

		defAniso = GLTexture::ANISO_X0;
		defFilter = GLTexture::LINEAR_MIPMAP_LINEAR;

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		glEnable(GL_CULL_FACE);

		glPixelStorei(GL_PACK_ALIGNMENT, 1);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

		reshape(WindowSystem::Get()->getWidth(), WindowSystem::Get()->getHeight());

		GLExtensions::initExtensions();

		wglSwapIntervalEXT(0);
	}

	/*
	*/
	GLSystem::~GLSystem() {}

	/*
	*/
	String GLSystem::getVendor() {
		return (char *)glGetString(GL_VENDOR);
	}

	/*
	*/
	String GLSystem::getRenderer() {
		return (char *)glGetString(GL_RENDERER);
	}

	/*
	*/
	String GLSystem::getVersion() {
		return (char *)glGetString(GL_VERSION);
	}

	/*
	*/
	String GLSystem::getExtensions() {
		return (char *)glGetString(GL_EXTENSIONS);
	}

	/*
	*/
	int GLSystem::getNumTexUnits() {
		int nTexUnits;
		glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS_ARB, (int *)&nTexUnits);
		//glGetIntegerv(GL_MAX_TEXTURE_UNITS, (int *)&nTexUnits);
		return nTexUnits;
	}

	/*
	*/
	int GLSystem::getMaxAniso() {
		int maxAniso;
		glGetIntegerv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAniso);
		return maxAniso;
	}

	/*
	*/
	bool GLSystem::occlusionQuerySupported() {
		return GLExtensions::isExtSupported("GL_ARB_occlusion_query");
	}

	/*
	*/
	bool GLSystem::glslSupported() {
		return (GLExtensions::isExtSupported("GL_ARB_vertex_shader") &&
			GLExtensions::isExtSupported("GL_ARB_fragment_shader") &&
			GLExtensions::isExtSupported("GL_ARB_shader_objects") &&
			GLExtensions::isExtSupported("GL_ARB_shading_language_100"));
	}

	/*
	*/
	bool GLSystem::vboSupported() {
		return GLExtensions::isExtSupported("GL_ARB_vertex_buffer_object");
	}

	/*
	*/
	bool GLSystem::fboSupported() {
		return GLExtensions::isExtSupported("GL_EXT_framebuffer_object");
	}

	bool GLSystem::pBufferSupported() {
		return GLExtensions::isWExtSupported("WGL_ARB_pbuffer");
	}


	/*
	*/
	void GLSystem::reshape(int width, int height) {
		if (height == 0)
			height = 1;

		glViewport(0, 0, width, height);

		glMatrixMode(GL_PROJECTION);
		loadMatrix(Mat4::perspective(60, (float)width / (float)height, 1, 500));

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}

	/*
	*/
	void GLSystem::getViewport(int *viewport) {
		glGetIntegerv(GL_VIEWPORT, viewport);
	}

	/*
	*/
	void GLSystem::clearColor(const Vec3 &color) {
		glClearColor(color.x, color.y, color.z, 1.0);
	}

	/*
	*/
	void GLSystem::colorMask(bool r, bool g, bool b, bool a) {
		glColorMask(r, g, b, a);
	}

	/*
	*/
	void GLSystem::clear(GLbitfield buffers) {
		glClear(buffers);
	}

	/*
	*/
	void GLSystem::flush() {
		glFlush();
	}

	/*
	*/
	void GLSystem::viewport(int x, int y) {
		glViewport(0, 0, x, y);
	}




	/*
	*/
	Mat4 GLSystem::getMatrix_MVP() {
		return getMatrix_Projection() * getMatrix_Modelview();
	}

	/*
	*/
	Mat4 GLSystem::getMatrix_Projection() {
		Mat4 temp;
		glGetFloatv(GL_PROJECTION_MATRIX, temp);
		return temp;
	}

	/*
	*/
	Mat4 GLSystem::getMatrix_Modelview() {
		Mat4 temp;
		glGetFloatv(GL_MODELVIEW_MATRIX, temp);
		return temp;
	}

	/*
	*/
	void GLSystem::multMatrix(const Mat4 &matrix) {
		glMultMatrixf(matrix);
	}

	/*
	*/
	void GLSystem::loadMatrix(const Mat4 &matrix) {
		glLoadMatrixf(matrix);
	}

	/*
	*/
	void GLSystem::setMatrixMode_Projection() {
		glMatrixMode(GL_PROJECTION);
	}

	/*
	*/
	void GLSystem::setMatrixMode_Modelview() {
		glMatrixMode(GL_MODELVIEW);
	}

	/*
	*/
	void GLSystem::push() {
		glPushMatrix();
	}

	/*
	*/
	void GLSystem::pop() {
		glPopMatrix();
	}

	/*
	*/
	void GLSystem::identity() {
		glLoadIdentity();
	}

	/*
	*/
	void GLSystem::translate(const Vec3 &pos) {
		glTranslatef(pos.x, pos.y, pos.z);
	}

	/*
	*/
	void GLSystem::rotate(float angle, const Vec3 &axis) {
		glRotatef(angle, axis.x, axis.y, axis.z);
	}

	/*
	*/
	void GLSystem::scale(const Vec3 &coef) {
		glScalef(coef.x, coef.y, coef.z);
	}



	/*
	*/
	void GLSystem::setColor(const Vec3 &color) {
		glColor3fv(color);
	}

	/*
	*/
	void GLSystem::setColor(const Vec4 &color) {
		glColor4fv(color);
	}




	/*
	*/
	void GLSystem::cullFunc(CullType type) {
		glFrontFace(type);
	}

	/*
	*/
	void GLSystem::cullFace(CullFace face) {
		glFrontFace(face);
	}

	/*
	*/
	void GLSystem::enableCulling(CullType type) {
		glEnable(GL_CULL_FACE);
		glFrontFace(type);
	}

	/*
	*/
	void GLSystem::enableCulling(CullFace face) {
		glEnable(GL_CULL_FACE);
		glFrontFace(face);
	}

	/*
	*/
	void GLSystem::enableCulling() {
		glEnable(GL_CULL_FACE);
	}

	/*
	*/
	void GLSystem::disableCulling() {
		glDisable(GL_CULL_FACE);
	}




	/*
	*/
	void GLSystem::alphaTestFunc(CompareType type, float alphaRef) {
		glAlphaFunc(type, alphaRef);
	}

	/*
	*/
	void GLSystem::enableAlphaTest(CompareType type, float alphaRef) {
		glEnable(GL_ALPHA_TEST);
		glAlphaFunc(type, alphaRef);
	}

	/*
	*/
	void GLSystem::enableAlphaTest() {
		glEnable(GL_ALPHA_TEST);
	}

	/*
	*/
	void GLSystem::disableAlphaTest() {
		glDisable(GL_ALPHA_TEST);
	}





	/*
	*/
	void GLSystem::depthFunc(CompareType type) {
		glDepthFunc(type);
	}

	/*
	*/
	void GLSystem::enableDepth(CompareType type) {
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(type);
	}

	/*
	*/
	void GLSystem::enableDepth() {
		glEnable(GL_DEPTH_TEST);
	}

	/*
	*/
	void GLSystem::disableDepth() {
		glDisable(GL_DEPTH_TEST);
	}

	/*
	*/
	void GLSystem::depthMask(bool mask) {
		glDepthMask(mask);
	}




	void GLSystem::scissorRect(int x, int y, int z, int w) {
		glScissor(x, y, z, w);
	}

	void GLSystem::enableScissor(int x, int y, int z, int w) {
		glEnable(GL_SCISSOR_TEST);
		glScissor(x, y, z, w);
	}

	void GLSystem::enableScissor() {
		glEnable(GL_SCISSOR_TEST);
	}

	void GLSystem::disableScissor() {
		glDisable(GL_SCISSOR_TEST);
	}




	/*
	*/
	void GLSystem::polygonOffsetFill(float a, float b) {
		glPolygonOffset(a, b);
	}

	/*
	*/
	void GLSystem::enablePolygonOffsetFill(float a, float b) {
		glEnable(GL_POLYGON_OFFSET_FILL);
		glPolygonOffset(a, b);
	}

	/*
	*/
	void GLSystem::enablePolygonOffsetFill() {
		glEnable(GL_POLYGON_OFFSET_FILL);
	}

	/*
	*/
	void GLSystem::disablePolygonOffsetFill() {
		glDisable(GL_POLYGON_OFFSET_FILL);
	}



	/*
	*/
	void GLSystem::enableBlending() {
		glEnable(GL_BLEND);
	}

	/*
	*/
	void GLSystem::blendFunc(BlendParam src, BlendParam dst) {
		glBlendFunc(src, dst);
	}

	/*
	*/
	void GLSystem::enableBlending(BlendParam src, BlendParam dst) {
		glEnable(GL_BLEND);
		glBlendFunc(src, dst);
	}

	/*
	*/
	void GLSystem::disableBlending() {
		glDisable(GL_BLEND);
	}




	/*
	*/
	void GLSystem::clipPlane(const Vec4 &plain, int plainNum) {
		double eq[4] = { (double)plain[0], (double)plain[1], (double)plain[2], (double)plain[3] };

		glClipPlane(GL_CLIP_PLANE0 + plainNum, eq);
	}

	/*
	*/
	void GLSystem::enableClipPlane(int plainNum) {
		glEnable(GL_CLIP_PLANE0 + plainNum);
	}

	/*
	*/
	void GLSystem::enableClipPlane(const Vec4 &plain, int plainNum) {
		double eq[4] = { (double)plain[0], (double)plain[1], (double)plain[2], (double)plain[3] };

		glClipPlane(GL_CLIP_PLANE0 + plainNum, eq);
		glEnable(GL_CLIP_PLANE0 + plainNum);
	}

	/*
	*/
	void GLSystem::disableClipPlane(int plainNum) {
		glDisable(GL_CLIP_PLANE0 + plainNum);
	}




	/*
	*/
	void GLSystem::enable2d(bool normalized) {
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();

		if (normalized) {
			loadMatrix(Mat4::ortho(0, 1, 1, 0, 0, 1));
		}
		else {
			loadMatrix(Mat4::ortho(0, WindowSystem::Get()->getWidth(), WindowSystem::Get()->getHeight(), 0, 0, 1));
		}

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadIdentity();
	}

	/*
	*/
	void GLSystem::enable3d() {
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
	}

	/*
	*/
	void GLSystem::drawRect(float x0, float y0, float x3, float y3, float tx0, float ty0, float tx3, float ty3) {
		glBegin(GL_QUADS);
		glTexCoord2f(tx0, ty0);
		glVertex3f(x0, y0, 0);

		glTexCoord2f(tx0, ty3);
		glVertex3f(x0, y3, 0);

		glTexCoord2f(tx3, ty3);
		glVertex3f(x3, y3, 0);

		glTexCoord2f(tx3, ty0);
		glVertex3f(x3, y0, 0);
		glEnd();
	}

	/*
	*/
	void GLSystem::drawIndexedGeometry(void *indices, int indexCount) {
		glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, indices);
	}

	/*
	*/
	void GLSystem::drawGeometry(int vertexCount) {
		glDrawArrays(GL_TRIANGLES, 0, vertexCount);
	}

}

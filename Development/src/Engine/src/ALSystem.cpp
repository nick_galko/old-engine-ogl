/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//***************************************************************************
#include "EngineAPI.h"
//***************************************************************************

namespace Vega {

	/*
	*/
	ALSystem::ALSystem() {}

	/*
	*/
	void ALSystem::Initialize() {

		LogHeader("ALSystem::Initialize()");

		alDevice = alcOpenDevice(NULL);
		alContext = alcCreateContext(alDevice, NULL);
		alcMakeContextCurrent(alContext);

		LogPrintf("Vendor: %s", getVendor());
		LogPrintf("Renderer: %s", getRenderer().c_str());
		LogPrintf("Version: %s", getVersion().c_str());
		LogPrintf("Extensions: %s", getExtensions().c_str());

		int error = alGetError();
		if (error != AL_NO_ERROR) {
			Error("ALSystem::Initialize() error: error while creating");
			return;
		}

		alDistanceModel(AL_INVERSE_DISTANCE_CLAMPED);
	}


	/*
	*/
	ALSystem *ALSystem::Get() {
		return GetEngine()->alSystem;
	}

	/*
	*/
	void ALSystem::Destroy() {
		alcMakeContextCurrent(NULL);
		alcDestroyContext(alContext);
		alcCloseDevice(alDevice);
		delete this;
	}

	/*
	*/
	String ALSystem::getVendor() {
		return (char *)alGetString(AL_VENDOR);
	}

	/*
	*/
	String ALSystem::getRenderer() {
		return (char *)alGetString(AL_RENDERER);
	}

	/*
	*/
	String ALSystem::getVersion() {
		return (char *)alGetString(AL_VERSION);
	}

	/*
	*/
	String ALSystem::getExtensions() {
		return (char *)alGetString(AL_EXTENSIONS);
	}

	/*
	*/
	void ALSystem::setListener(const Vec3 &pos, const Vec3 &dir) {
		float orientation[] = { dir.x, dir.y, dir.z, 0, 1, 0 };
		alListenerfv(AL_POSITION, pos);
		alListenerfv(AL_ORIENTATION, orientation);
	}

}
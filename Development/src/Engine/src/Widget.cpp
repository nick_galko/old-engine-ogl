/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//**************************************
#include "EngineAPI.h"
//**************************************

namespace Vega {

	//**************************************************************************
	//Widget class ENGINE_API
	//**************************************************************************
	/*
	*/
	void Widget::update() {
	}

	/*
	*/
	void Widget::draw() {
	}

	/*
	*/
	void Widget::addWidget(Widget *widget) {
		widget->parent = this;
		children.push_back(widget);
	}

	/*
	*/
	void Widget::removeWidget(Widget *widget) {
		for (int i = 0; i < children.size(); i++) {
			if (children[i] == widget)
				children[i]->parent = NULL;
			children[i] = NULL;
		}
	}




	//**************************************************************************
	//WidgetButton class ENGINE_API
	//**************************************************************************
	/*
	*/
	WidgetButton *WidgetButton::Create(const String &text) {
		WidgetButton *button = new WidgetButton();

		button->text = text;

		button->width = text.size() * 20;
		button->height = 30;

		button->posX = 0;
		button->posY = 0;

		button->parent = NULL;

		button->enabled = true;

		return button;
	}

	/*
	*/
	void WidgetButton::Destroy() {
		delete this;
	}

	/*
	*/
	void WidgetButton::update() {
		for (int w = 0; w < children.size(); w++) {
			if (children[w]) {
				children[w]->update();
			}
		}

		if (!isEnabled()) {
			focusIn = focusOut = focused = false;
			pressed = clicked = false;
			return;
		}

		bool f = 0;

		if (!WindowSystem::Get()->isMouseGrabed()) {
			int mx = WindowSystem::Get()->getMouseX();
			int my = WindowSystem::Get()->getMouseY();

			f = ((getX() < mx) && (getX() + width > mx) &&
				(getY() < my) && (getY() + height > my));
		}

		focusIn = (!focused && f);
		focusOut = (focused && !f);
		focused = f;


		pressed = false;
		clicked = false;
		bool mb = (WindowSystem::Get()->isMouseButtonPressed(WindowSystem::MOUSE_LEFT_BUTTON));

		if (!focused) {
			pressedFlag = -1;
		}
		else {
			if (mb) {
				if (pressedFlag != -1) {
					pressedFlag = 1;
					pressed = true;
				}
			}
			else {
				if (pressedFlag == 1) {
					clicked = true;
				}
				pressedFlag = 0;
			}
		}
	}

	/*
	*/
	void WidgetButton::draw() {
		if (!isEnabled()) {
			return;
		}

		GLSystem::Get()->enableBlending(GLSystem::SRC_ALPHA, GLSystem::ONE_MINUS_SRC_ALPHA);

		if (isFocused()) {
			GUI::Get()->buttonFocusedTex->set(0);
		}
		else {
			GUI::Get()->buttonTex->set(0);
		}

		GLSystem::Get()->setColor(Vec4(1, 1, 1, 1) * GUI::Get()->alpha);

		GLSystem::Get()->enable2d(false);
		GLSystem::Get()->drawRect(getX(), getY(), getX() + width, getY() + height, 0, 0, 1, 1);
		GLSystem::Get()->enable3d();

		GUI::Get()->buttonTex->unset(0);
		GLSystem::Get()->disableBlending();

		int fSize = height*0.5;
		int tPosX = (width - text.length()*fSize*0.8)*0.5 + getX();
		int tPosY = (height - fSize)*0.5 + getY();

		GUI::Get()->font->print(tPosX, tPosY, fSize, text, Vec3(1, 1, 1), GUI::Get()->alpha);

		for (int w = 0; w < children.size(); w++) {
			if (children[w]) {
				children[w]->draw();
			}
		}
	}





	//**************************************************************************
	//WidgetCheckBox class ENGINE_API
	//**************************************************************************
	/*
	*/
	WidgetCheckBox *WidgetCheckBox::Create(const String &text) {
		WidgetCheckBox *checkBox = new WidgetCheckBox();

		checkBox->text = text;

		checkBox->width = 15;
		checkBox->height = 15;

		checkBox->posX = 0;
		checkBox->posY = 0;

		checkBox->parent = NULL;

		checkBox->checked = false;
		checkBox->enabled = true;

		return checkBox;
	}

	/*
	*/
	void WidgetCheckBox::Destroy() {
		delete this;
	}

	/*
	*/
	void WidgetCheckBox::update() {
		for (int w = 0; w < children.size(); w++) {
			if (children[w]) {
				children[w]->update();
			}
		}

		if (!isEnabled()) {
			focusIn = focusOut = focused = false;
			pressed = clicked = false;
			return;
		}

		bool f = 0;
		if (!WindowSystem::Get()->isMouseGrabed()) {
			int mx = WindowSystem::Get()->getMouseX();
			int my = WindowSystem::Get()->getMouseY();

			f = ((getX() < mx) && (getX() + width > mx) &&
				(getY() < my) && (getY() + height > my));
		}
		focusIn = (!focused && f);
		focusOut = (focused && !f);
		focused = f;


		pressed = false;
		clicked = false;
		bool mb = (WindowSystem::Get()->isMouseButtonPressed(WindowSystem::MOUSE_LEFT_BUTTON));
		if (!focused) {
			pressedFlag = -1;
		}
		else {
			if (mb) {
				if (pressedFlag != -1) {
					pressedFlag = 1;
					pressed = true;
				}
			}
			else {
				if (pressedFlag == 1) {
					clicked = true;
				}
				pressedFlag = 0;
			}
		}

		if (clicked) {
			toggleCheck();
		}
	}

	/*
	*/
	void WidgetCheckBox::draw() {
		if (!isEnabled()) {
			return;
		}

		GLSystem::Get()->enableBlending(GLSystem::SRC_ALPHA, GLSystem::ONE_MINUS_SRC_ALPHA);

		if (!checked) {
			GUI::Get()->checkBoxTex->set(0);
		}
		else {
			GUI::Get()->checkBoxCheckedTex->set(0);
		}

		GLSystem::Get()->setColor(Vec4(1, 1, 1, 1) * GUI::Get()->alpha);

		GLSystem::Get()->enable2d(false);
		GLSystem::Get()->drawRect(getX(), getY(), getX() + width, getY() + height, 0, 0, 1, 1);
		GLSystem::Get()->enable3d();

		GUI::Get()->checkBoxTex->unset(0);
		GLSystem::Get()->disableBlending();

		for (int w = 0; w < children.size(); w++) {
			if (children[w]) {
				children[w]->draw();
			}
		}

		int fSize = height*0.8;
		int tPosX = width + getX();
		int tPosY = getY();
		GUI::Get()->font->print(tPosX, tPosY, fSize, text, Vec3(1, 1, 1), GUI::Get()->alpha);
	}




	//**************************************************************************
	//WidgetRadioGroup class ENGINE_API
	//**************************************************************************
	/*
	*/
	WidgetRadioGroup *WidgetRadioGroup::Create() {
		WidgetRadioGroup *group = new WidgetRadioGroup();

		group->posX = 0;
		group->posY = 0;

		group->parent = NULL;

		return group;
	}

	void WidgetRadioGroup::Destroy() {
		delete this;
	}

	void WidgetRadioGroup::addCheckBox(WidgetCheckBox *cbox) {
		cbox->parent = this;
		checkBoxes.push_back(cbox);
	}

	void WidgetRadioGroup::removeCheckBox(WidgetCheckBox *cbox) {
		for (int i = 0; i < checkBoxes.size(); i++) {
			if (checkBoxes[i] == cbox)
				checkBoxes[i]->parent = NULL;
			checkBoxes[i] = NULL;
		}
	}

	void WidgetRadioGroup::update() {
		for (int w = 0; w < checkBoxes.size(); w++) {
			if (checkBoxes[w]) {
				checkBoxes[w]->update();
			}
		}

		focusIn = focusOut = focused = false;
		pressed = clicked = false;
		if (!isEnabled()) {
			return;
		}

		int c = -1;
		for (int w = 0; w < checkBoxes.size(); w++) {
			if (checkBoxes[w]) {
				if (checkBoxes[w]->isChecked() && checkBoxes[w]->isClicked()) {
					checkBoxes[w]->setChecked(true);
					c = w;
					break;
				}
				else if (!checkBoxes[w]->isChecked() && checkBoxes[w]->isClicked()) {
					checkBoxes[w]->setChecked(true);
					c = w;
					break;
				}
				else if (checkBoxes[w]->isChecked()) {
					c = w;
				}
			}
		}

		for (int w = 0; w < checkBoxes.size(); w++) {
			if (c == -1) break;
			if (checkBoxes[w] && w != c) {
				checkBoxes[w]->setChecked(false);
			}
		}
	}

	void WidgetRadioGroup::draw() {
		if (!isEnabled()) {
			return;
		}

		for (int w = 0; w < checkBoxes.size(); w++) {
			if (checkBoxes[w]) {
				checkBoxes[w]->setEnabled(true);
				checkBoxes[w]->draw();
			}
		}
	}



	//**************************************************************************
	//WidgetLabel class ENGINE_API
	//**************************************************************************
	/*
	*/
	WidgetLabel *WidgetLabel::Create(const String &text) {
		WidgetLabel *label = new WidgetLabel();

		label->text = text;

		label->width = 15 * text.size();
		label->height = 15;

		label->posX = 0;
		label->posY = 0;

		label->parent = NULL;

		label->enabled = true;

		return label;
	}

	/*
	*/
	void WidgetLabel::Destroy() {
		delete this;
	}

	/*
	*/
	void WidgetLabel::update() {
		if (!isEnabled()) {
			focusIn = focusOut = focused = false;
			pressed = clicked = false;
			return;
		}

		bool f = 0;
		if (!WindowSystem::Get()->isMouseGrabed()) {
			int mx = WindowSystem::Get()->getMouseX();
			int my = WindowSystem::Get()->getMouseY();

			f = ((getX() < mx) && (getX() + width > mx) &&
				(getY() < my) && (getY() + height > my));
		}
		focusIn = (!focused && f);
		focusOut = (focused && !f);
		focused = f;


		pressed = false;
		clicked = false;
		bool mb = (WindowSystem::Get()->isMouseButtonPressed(WindowSystem::MOUSE_LEFT_BUTTON));
		if (!focused) {
			pressedFlag = -1;
		}
		else {
			if (mb) {
				if (pressedFlag != -1) {
					pressedFlag = 1;
					pressed = true;
				}
			}
			else {
				if (pressedFlag == 1) {
					clicked = true;
				}
				pressedFlag = 0;
			}
		}
	}

	/*
	*/
	void WidgetLabel::draw() {
		if (!isEnabled()) {
			return;
		}

		GUI::Get()->font->print(getX(), getY(), getHeight(), text, Vec3(1, 1, 1), GUI::Get()->alpha);
	}




	//**************************************************************************
	//WidgetHSlider class ENGINE_API
	//**************************************************************************
	/*
	*/
	WidgetHSlider *WidgetHSlider::Create() {
		WidgetHSlider *hslider = new WidgetHSlider();

		hslider->width = 150;
		hslider->height = 15;

		hslider->posX = 0;
		hslider->posY = 0;

		hslider->parent = NULL;

		hslider->setValue(100);

		hslider->enabled = true;

		return hslider;
	}

	/*
	*/
	void WidgetHSlider::Destroy() {
		delete this;
	}

	void WidgetHSlider::update() {
		for (int w = 0; w < children.size(); w++) {
			if (children[w]) {
				children[w]->update();
			}
		}

		if (!isEnabled()) {
			focusIn = focusOut = focused = false;
			pressed = clicked = false;
			return;
		}

		bool f = 0;
		if (!WindowSystem::Get()->isMouseGrabed()) {
			int mx = WindowSystem::Get()->getMouseX();
			int my = WindowSystem::Get()->getMouseY();

			f = ((getX() < mx) && (getX() + width > mx) &&
				(getY() < my) && (getY() + height > my));
		}
		focusIn = (!focused && f);
		focusOut = (focused && !f);
		focused = f;


		pressed = false;
		clicked = false;
		bool mb = (WindowSystem::Get()->isMouseButtonPressed(WindowSystem::MOUSE_LEFT_BUTTON));
		if (!focused && !mb) {
			pressedFlag = -1;
		}
		else {
			if (mb) {
				if (pressedFlag != -1) {
					pressedFlag = 1;
					pressed = true;
				}
			}
			else {
				if (pressedFlag == 1) {
					clicked = true;
				}
				pressedFlag = 0;
			}
		}

		if (pos > width) pos = width;
		if (pos < 0) pos = 0;

		if (pressed) {
			pos = WindowSystem::Get()->getMouseX() - getX();
			if (pos < 0) pos = 0;
			if (pos > width) pos = width;
			value = (int)(100.0 * (float)pos / (float)width);
		}
	}
	/*
	*/
	void WidgetHSlider::draw() {
		if (!isEnabled()) {
			return;
		}

		GLSystem::Get()->enableBlending(GLSystem::SRC_ALPHA, GLSystem::ONE_MINUS_SRC_ALPHA);
		GUI::Get()->hSliderTex->set(0);

		GLSystem::Get()->setColor(Vec4(1, 1, 1, 1) * GUI::Get()->alpha);

		GLSystem::Get()->enable2d(false);
		GLSystem::Get()->drawRect(getX(), getY(), getX() + width, getY() + height, 0, 0, 1, 1);

		GUI::Get()->checkBoxTex->set(0);

		GLSystem::Get()->drawRect(getX() + pos - height*0.5,
			getY(),// - height*0.1, 
			getX() + pos + height*0.5,
			getY() + height*1.0, //1.1, 
			0, 0, 1, 1);
		GLSystem::Get()->enable3d();

		GUI::Get()->buttonTex->unset(0);
		GLSystem::Get()->disableBlending();

		for (int w = 0; w < children.size(); w++) {
			if (children[w]) {
				children[w]->draw();
			}
		}
	}



	//**************************************************************************
	//WidgetWindow class ENGINE_API
	//**************************************************************************
	/*
	*/
	WidgetWindow *WidgetWindow::Create() {
		WidgetWindow *window = new WidgetWindow();

		window->width = 400;
		window->height = 300;

		window->posX = 0;
		window->posY = 0;

		window->parent = NULL;

		window->enabled = true;

		window->moveable = true;
		return window;
	}

	/*
	*/
	void WidgetWindow::Destroy() {
		delete this;
	}

	/*
	*/
	void WidgetWindow::update() {
		for (int w = 0; w < children.size(); w++) {
			if (children[w]) {
				children[w]->update();
			}
		}

		if (!isEnabled()) {
			focusIn = focusOut = focused = false;
			pressed = clicked = false;
			return;
		}

		bool f = 0;

		if (!WindowSystem::Get()->isMouseGrabed()) {
			int mx = WindowSystem::Get()->getMouseX();
			int my = WindowSystem::Get()->getMouseY();

			f = ((getX() < mx) && (getX() + width > mx) &&
				(getY() < my) && (getY() + height > my));
		}

		if (f) {
			for (int w = 0; w < children.size(); w++) {
				if (children[w]) {
					if (children[w]->isFocused())
						f = false;
				}
			}
		}

		focusIn = (!focused && f);
		focusOut = (focused && !f);
		focused = f;

		pressed = false;
		clicked = false;
		bool mb = (WindowSystem::Get()->isMouseButtonPressed(WindowSystem::MOUSE_LEFT_BUTTON));

		if (!focused) {
			pressedFlag = -1;
		}
		else {
			if (mb) {
				if (pressedFlag != -1) {
					pressedFlag = 1;
					pressed = true;
				}
			}
			else {
				if (pressedFlag == 1) {
					clicked = true;
				}
				pressedFlag = 0;
			}
		}

		if (moveable && pressed) {
			posX += WindowSystem::Get()->getMouseDX();
			posY += WindowSystem::Get()->getMouseDY();
		}
		if (posX < 0) posX = 0;
		if (posY < 0) posY = 0;
		if (posX > WindowSystem::Get()->getWidth() - width) posX = WindowSystem::Get()->getWidth() - width;
		if (posY > WindowSystem::Get()->getHeight() - height) posY = WindowSystem::Get()->getHeight() - height;
	}

	/*
	*/
	void WidgetWindow::draw() {
		if (!isEnabled()) {
			return;
		}

		GLSystem::Get()->enableBlending(GLSystem::SRC_ALPHA, GLSystem::ONE_MINUS_SRC_ALPHA);
		GUI::Get()->windowTex->set(0);

		GLSystem::Get()->setColor(Vec4(1, 1, 1, 1) * GUI::Get()->alpha);

		GLSystem::Get()->enable2d(false);
		GLSystem::Get()->drawRect(getX(), getY(), getX() + width, getY() + height, 0, 0, 1, 1);
		GLSystem::Get()->enable3d();

		GUI::Get()->windowTex->unset(0);
		GLSystem::Get()->disableBlending();

		for (int w = 0; w < children.size(); w++) {
			if (children[w]) {
				children[w]->draw();
			}
		}
	}

	void WidgetWindow::setMoveable(bool moveable) {
		this->moveable = moveable;
	}

	bool WidgetWindow::getMoveable() {
		return moveable;
	}

}



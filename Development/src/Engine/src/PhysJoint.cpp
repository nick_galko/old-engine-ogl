/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//**************************************
#include "EngineAPI.h"
#include "../../../Externals/newton/coreLibrary_300/source/newton/newton.h"
//**************************************

namespace Vega {

	PhysJointUpVector::PhysJointUpVector(const Vec3 &direction, PhysBody *body) {
		nJoint = NewtonConstraintCreateUpVector(Engine::Get()->physSystem->nWorld, direction, body->nBody);
	}

	PhysJointUpVector::~PhysJointUpVector() {
		NewtonDestroyJoint(Engine::Get()->physSystem->nWorld, nJoint);
	}

}

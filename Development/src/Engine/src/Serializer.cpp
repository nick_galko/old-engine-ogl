/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//**************************************
#include "EngineAPI.h"
//**************************************

namespace Vega {

	/*
	*/
	Serializer *Serializer::Create() {
		Serializer *serializer = new Serializer();
		return serializer;
	}

	/*
	*/
	void Serializer::Destroy() {
		s_int.clear();
		s_float.clear();
		s_vec2.clear();
		s_vec3.clear();
		s_vec4.clear();
		s_mat4.clear();
		s_string.clear();

		delete this;
	}

	/*
	*/
	void Serializer::save(FStream *file) {
		{
			std::map<String, String>::iterator it;
			for (it = s_string.begin(); it != s_string.end(); it++) {
				file->printf("\"%s\" %s\n", it->second);
			}
		}

		{
			std::map<String, int>::iterator it;
			for (it = s_int.begin(); it != s_int.end(); it++) {
				file->printf("\"%s\" %i\n", it->second);
			}
		}

		{
			std::map<String, float>::iterator it;
			for (it = s_float.begin(); it != s_float.end(); it++) {
				file->printf("\"%s\" %f\n", it->second);
			}
		}

		{
			std::map<String, Vec2>::iterator it;
			for (it = s_vec2.begin(); it != s_vec2.end(); it++) {
				file->printf("\"%s\" %f %f\n", it->second.x, it->second.y);
			}
		}

		{
			std::map<String, Vec3>::iterator it;
			for (it = s_vec3.begin(); it != s_vec3.end(); it++) {
				file->printf("\"%s\" %f %f %f\n", it->second.x, it->second.y, it->second.z);
			}
		}

		{
			std::map<String, Vec4>::iterator it;
			for (it = s_vec4.begin(); it != s_vec4.end(); it++) {
				file->printf("\"%s\" %f %f %f %f\n", it->second.x, it->second.y, it->second.z, it->second.w);
			}
		}

		{
			std::map<String, Mat4>::iterator it;
			for (it = s_mat4.begin(); it != s_mat4.end(); it++) {
				file->printf("\"%s\" %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n", it->second.e[0], it->second.e[1], it->second.e[2], it->second.e[3],
					it->second.e[4], it->second.e[5], it->second.e[6], it->second.e[7],
					it->second.e[8], it->second.e[9], it->second.e[10], it->second.e[11],
					it->second.e[12], it->second.e[13], it->second.e[14], it->second.e[15]);
			}
		}
	}

	/*
	Get parameter
	*/
	int Serializer::getInt(const String &name) {
		std::map<String, int>::iterator it = s_int.find(name);
		if (it == s_int.end()) {
			return 0;
		}
		return it->second;
	}

	/*
	*/
	float Serializer::getFloat(const String &name) {
		std::map<String, float>::iterator it = s_float.find(name);
		if (it == s_float.end()) {
			return 0;
		}
		return it->second;
	}

	/*
	*/
	Vec2 Serializer::getVec2(const String &name) {
		std::map<String, Vec2>::iterator it = s_vec2.find(name);
		if (it == s_vec2.end()) {
			return Vec2();
		}
		return it->second;
	}

	/*
	*/
	Vec3 Serializer::getVec3(const String &name) {
		std::map<String, Vec3>::iterator it = s_vec3.find(name);
		if (it == s_vec3.end()) {
			return Vec3();
		}
		return it->second;
	}

	/*
	*/
	Vec4 Serializer::getVec4(const String &name) {
		std::map<String, Vec4>::iterator it = s_vec4.find(name);
		if (it == s_vec4.end()) {
			return Vec4();
		}
		return it->second;
	}

	/*
	*/
	Mat4 Serializer::getMat4(const String &name) {
		std::map<String, Mat4>::iterator it = s_mat4.find(name);
		if (it == s_mat4.end()) {
			return Mat4();
		}
		return it->second;
	}

	/*
	*/
	String Serializer::getString(const String &name) {
		std::map<String, String>::iterator it = s_string.find(name);
		if (it == s_string.end()) {
			return "";
		}
		return it->second;
	}


	/*
	Set parameters
	*/
	void Serializer::setInt(const String &name, int value) {
		std::map<String, int>::iterator it = s_int.find(name);
		if (it == s_int.end()) {
			s_int[name] = value;
		}
		else {
			it->second = value;
		}
	}

	/*
	*/
	void Serializer::setFloat(const String &name, float value) {
		std::map<String, float>::iterator it = s_float.find(name);
		if (it == s_float.end()) {
			s_float[name] = value;
		}
		else {
			it->second = value;
		}
	}

	/*
	*/
	void Serializer::setVec2(const String &name, const Vec2 &value) {
		std::map<String, Vec2>::iterator it = s_vec2.find(name);
		if (it == s_vec2.end()) {
			s_vec2[name] = value;
		}
		else {
			it->second = value;
		}
	}

	/*
	*/
	void Serializer::setVec3(const String &name, const Vec3 &value) {
		std::map<String, Vec3>::iterator it = s_vec3.find(name);
		if (it == s_vec3.end()) {
			s_vec3[name] = value;
		}
		else {
			it->second = value;
		}
	}

	/*
	*/
	void Serializer::setVec4(const String &name, const Vec4 &value) {
		std::map<String, Vec4>::iterator it = s_vec4.find(name);
		if (it == s_vec4.end()) {
			s_vec4[name] = value;
		}
		else {
			it->second = value;
		}
	}

	/*
	*/
	void Serializer::setMat4(const String &name, const Mat4 &value) {
		std::map<String, Mat4>::iterator it = s_mat4.find(name);
		if (it == s_mat4.end()) {
			s_mat4[name] = value;
		}
		else {
			it->second = value;
		}
	}

	/*
	*/
	void Serializer::setString(const String &name, const String &value) {
		std::map<String, String>::iterator it = s_string.find(name);
		if (it == s_string.end()) {
			s_string[name] = value;
		}
		else {
			it->second = value;
		}
	}

}

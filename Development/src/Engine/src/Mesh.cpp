/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//**************************************
#include "EngineAPI.h"
//**************************************

namespace Vega {

	/*
	*/
	Mesh *Mesh::Create(const String &path) {
		Mesh *mesh = new Mesh();
		mesh->loadXSMSH(path);

		mesh->calculateTBN();
		mesh->createVBO();
		mesh->calculateBoundings();
		return mesh;
	}

	/*
	*/
	void Mesh::Destroy() {
		for (int s = 0; s < numSubsets; s++) {
			Subset *st = subsets[s];

			st->vertBuff->Destroy();

			delete[] st->vertices;
			delete[] st->indices;
		}
		delete this;
	}


	/*
	*/
	void Mesh::save(const String &path) {
		FStream *file = FStream::Create(path, FStream::WRITE_TEXT);

		//number of subsets
		file->printf("#XSYSTEM_ENGINE_MESH\n\n");

		file->printf("num_subsets %i\n\n", numSubsets); //num_subsets

		//process subsets
		for (int s = 0; s < numSubsets; s++) {
			Subset *st = subsets[s];
			file->printf("subset %s\n", st->name);

			file->printf("num_vertices %i\n", st->numVertices);

			//process vertices
			for (int v = 0; v < st->numVertices; v++) {
				file->printf("%f %f %f %f %f %f %f %f\n",
					st->vertices[v].position.x,
					st->vertices[v].position.y,
					st->vertices[v].position.z,
					st->vertices[v].normal.x,
					st->vertices[v].normal.y,
					st->vertices[v].normal.z,
					st->vertices[v].texcoord.x,
					st->vertices[v].texcoord.y);
			}

			//number of faces

			file->printf("num_faces %i\n", st->numIndices);
			st->numIndices *= 3;
			st->indices = new unsigned int[st->numIndices];

			//process faces
			for (int i = 0; i < st->numIndices / 3; i++) {
				file->printf("%i %i %i\n",
					st->indices[i * 3 + 0],
					st->indices[i * 3 + 1],
					st->indices[i * 3 + 2]);
			}
			file->printf("\n");
		}

		delete file;
	}

	/*
	*/
	void Mesh::drawSubset(int s) {
		Subset *st = subsets[s];

		st->vertBuff->set();
		st->vertBuff->setTexCoordSource(0, 2, sizeof(Vertex), sizeof(Vec3));
		st->vertBuff->setNormalSource(sizeof(Vertex), sizeof(Vec3)+sizeof(Vec2));
		st->vertBuff->setTexCoordSource(1, 3, sizeof(Vertex), 2 * sizeof(Vec3)+sizeof(Vec2));
		st->vertBuff->setTexCoordSource(2, 3, sizeof(Vertex), 3 * sizeof(Vec3)+sizeof(Vec2));
		st->vertBuff->setVertexSource(3, sizeof(Vertex), 0);

		GLSystem::Get()->drawIndexedGeometry(st->indices, st->numIndices);

		st->vertBuff->unset();
		st->vertBuff->unsetVertexSource();
		st->vertBuff->unsetTexCoordSource(0);
		st->vertBuff->unsetNormalSource();
		st->vertBuff->unsetTexCoordSource(1);
		st->vertBuff->unsetTexCoordSource(2);
	}

	/*
	*/
	int Mesh::getSubset(String name) {
		for (int s = 0; s < numSubsets; s++) {
			if (subsets[s]->name == name)
				return s;
		}
		return 0;
	}


	/*
	*/
	void Mesh::calculateTBN() {
		for (int s = 0; s < numSubsets; s++) {
			Subset *st = subsets[s];

			for (int iLoop = 0; iLoop < st->numIndices / 3; iLoop++) {
				int ind0 = st->indices[iLoop * 3 + 0];
				int ind1 = st->indices[iLoop * 3 + 1];
				int ind2 = st->indices[iLoop * 3 + 2];

				Vec3 n;
				TBNComputer::computeN(n, st->vertices[ind0].position,
					st->vertices[ind1].position,
					st->vertices[ind2].position);

				st->vertices[ind0].normal += n;
				st->vertices[ind1].normal += n;
				st->vertices[ind2].normal += n;
			}

			for (int vLoop = 0; vLoop < st->numVertices; vLoop++) {
				st->vertices[vLoop].normal = Vec3::normalize(st->vertices[vLoop].normal);
			}

			for (int iLoop = 0; iLoop < st->numIndices / 3; iLoop++) {
				int ind0 = st->indices[iLoop * 3 + 0];
				int ind1 = st->indices[iLoop * 3 + 1];
				int ind2 = st->indices[iLoop * 3 + 2];

				Vec3 t[3];
				Vec3 b[3];

				TBNComputer::computeTBN(t[0], b[0],
					st->vertices[ind0].position,
					st->vertices[ind1].position,
					st->vertices[ind2].position,
					st->vertices[ind0].texcoord,
					st->vertices[ind1].texcoord,
					st->vertices[ind2].texcoord,
					st->vertices[ind0].normal);
				TBNComputer::computeTBN(t[1], b[1],
					st->vertices[ind1].position,
					st->vertices[ind2].position,
					st->vertices[ind0].position,
					st->vertices[ind1].texcoord,
					st->vertices[ind2].texcoord,
					st->vertices[ind0].texcoord,
					st->vertices[ind1].normal);
				TBNComputer::computeTBN(t[2], b[2],
					st->vertices[ind2].position,
					st->vertices[ind0].position,
					st->vertices[ind1].position,
					st->vertices[ind2].texcoord,
					st->vertices[ind0].texcoord,
					st->vertices[ind1].texcoord,
					st->vertices[ind2].normal);

				st->vertices[ind0].tangent += t[0];
				st->vertices[ind1].tangent += t[1];
				st->vertices[ind2].tangent += t[2];

				st->vertices[ind0].binormal += b[0];
				st->vertices[ind1].binormal += b[1];
				st->vertices[ind2].binormal += b[2];
			}

			for (int vLoop = 0; vLoop < st->numVertices; vLoop++)	{
				st->vertices[vLoop].tangent = Vec3::normalize(st->vertices[vLoop].tangent);
				st->vertices[vLoop].binormal = Vec3::normalize(st->vertices[vLoop].binormal);
			}
		}
	}

	/*
	*/
	void Mesh::calculateBoundings() {
		for (int s = 0; s < numSubsets; s++) {
			Subset *st = subsets[s];

			for (int v = 0; v < st->numVertices; v++) {
				//need to do so because of MSVC 2005 bug
				if (v == 0) {
					st->min = st->vertices[0].position;
					st->max = st->vertices[0].position;
				}
				st->max = st->vertices[0].position;
				st->min.x = min(st->min.x, st->vertices[v].position.x);
				st->min.y = min(st->min.y, st->vertices[v].position.y);
				st->min.z = min(st->min.z, st->vertices[v].position.z);

				st->max.x = max(st->max.x, st->vertices[v].position.x);
				st->max.y = max(st->max.y, st->vertices[v].position.y);
				st->max.z = max(st->max.z, st->vertices[v].position.z);
			}

			st->radius = 0;
			st->center = (st->max + st->min) * 0.5;

			for (int v = 0; v < st->numVertices; v++) {
				st->radius = max(st->radius, (st->vertices[v].position - st->center).length());
			}
		}

		min = subsets[0]->min;
		max = subsets[0]->max;

		for (int s = 0; s < numSubsets; s++) {
			Subset *st = subsets[s];

			min.x = min(min.x, st->min.x);
			min.y = min(min.y, st->min.y);
			min.z = min(min.z, st->min.z);

			max.x = max(max.x, st->max.x);
			max.y = max(max.y, st->max.y);
			max.z = max(max.z, st->max.z);
		}

		center = (min + max) * 0.5;
		radius = 0;

		for (int s = 0; s < numSubsets; s++) {
			Subset *st = subsets[s];
			for (int v = 0; v < st->numVertices; v++) {
				radius = max(radius, (st->vertices[v].position - center).length());
			}
		}
	}

	/*
	*/
	void Mesh::createVBO() {
		for (int s = 0; s < numSubsets; s++) {
			Subset *st = subsets[s];
			st->vertBuff = GLVBO::CreateVBO(st->vertices, st->numVertices, sizeof(Vertex), GLVBO::FLOAT);
		}
	}

	/*
	*/
	void Mesh::loadXSMSH(const String &path) {
		//begin loading
		FStream *file = FStream::Create(path, FStream::READ_TEXT);

		//Check if exist
		if (!file) {
			Error("Mesh::loadXSMSH() error: mesh file '%s' not found", path);
			return;
		}

		String line;

		//number of subsets
		file->gets(); //#XSYSTEM_ENGINE_MESH
		file->gets(); //_

		line = file->gets(); //num_subsets
		numSubsets = line.getWord(2).toInt();
		subsets = new Mesh::Subset*[numSubsets];

		file->gets(); //_

		//process subsets
		for (int s = 0; s < numSubsets; s++) {
			subsets[s] = new Mesh::Subset();
			Mesh::Subset *st = subsets[s];

			//read the surface name
			line = file->gets();
			st->name = line.getQuotedWord(2);

			//number of vertices
			line = file->gets();
			st->numVertices = line.getWord(2).toInt();
			st->vertices = new Mesh::Vertex[st->numVertices];

			//process vertices
			for (int v = 0; v < st->numVertices; v++) {
				line = file->gets();
				st->vertices[v].position.x = line.getWord(1).toFloat();
				st->vertices[v].position.y = line.getWord(2).toFloat();
				st->vertices[v].position.z = line.getWord(3).toFloat();
				st->vertices[v].normal.x = line.getWord(4).toFloat();
				st->vertices[v].normal.y = line.getWord(5).toFloat();
				st->vertices[v].normal.z = line.getWord(6).toFloat();
				st->vertices[v].texcoord.x = line.getWord(7).toFloat();
				st->vertices[v].texcoord.y = line.getWord(8).toFloat();
			}

			//number of faces

			line = file->gets();
			st->numIndices = line.getWord(2).toInt() * 3;
			st->indices = new unsigned int[st->numIndices];

			//process faces
			for (int i = 0; i < st->numIndices / 3; i++) {
				line = file->gets();
				st->indices[i * 3 + 0] = line.getWord(1).toInt();
				st->indices[i * 3 + 1] = line.getWord(2).toInt();
				st->indices[i * 3 + 2] = line.getWord(3).toInt();
			}

			file->gets(); //_
		}

		file->Destroy();
	}

}





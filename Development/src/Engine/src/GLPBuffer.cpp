/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//***************************************************************************
#include "EngineAPI.h"
//***************************************************************************

namespace Vega {

	/*
	*/
	GLPBuffer *GLPBuffer::Create(int width, int height) {
		GLPBuffer *pbuffer = new GLPBuffer();

		pbuffer->width = width;
		pbuffer->height = height;

		pbuffer->hPBuffer = NULL;

		int pf_attr[] =
		{
			WGL_SUPPORT_OPENGL_ARB, TRUE,
			WGL_DRAW_TO_PBUFFER_ARB, TRUE,
			WGL_RED_BITS_ARB, 8,
			WGL_GREEN_BITS_ARB, 8,
			WGL_BLUE_BITS_ARB, 8,
			WGL_ALPHA_BITS_ARB, 8,
			WGL_DEPTH_BITS_ARB, 24,
			WGL_DOUBLE_BUFFER_ARB, FALSE,
			0
		};

		unsigned int count = 0;
		int pixelFormat;
		wglChoosePixelFormatARB(WindowSystem::Get()->hDC, pf_attr, NULL, 1, &pixelFormat, &count);

		if (!count) {
			Error("GLPBuffer::Create() error: could not find an acceptable pixel format");
		}

		pbuffer->hPBuffer = wglCreatePbufferARB(WindowSystem::Get()->hDC, pixelFormat, pbuffer->width, pbuffer->height, NULL);
		pbuffer->hDC = wglGetPbufferDCARB(pbuffer->hPBuffer);
		pbuffer->hRC = wglCreateContext(pbuffer->hDC);

		if (!pbuffer->hPBuffer)	{
			Error("GLPBuffer::Create() error: could not Create the p-buffer");
		}

		wglShareLists(WindowSystem::Get()->hRC, pbuffer->hRC);

		pbuffer->set();
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		glEnable(GL_CULL_FACE);

		glPixelStorei(GL_PACK_ALIGNMENT, 1);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
		pbuffer->unset();

		return pbuffer;
	}

	/*
	*/
	void GLPBuffer::Destroy() {
		if (hRC)	{
			wglMakeCurrent(hDC, hRC);
			wglDeleteContext(hRC);
			wglReleasePbufferDCARB(hPBuffer, hDC);
			wglDestroyPbufferARB(hPBuffer);
			hRC = NULL;
		}

		if (hDC)	{
			ReleaseDC((HWND)WindowSystem::Get()->hWnd, hDC);
			hDC = NULL;
		}
		delete this;
	}

	/*
	*/
	void GLPBuffer::set() {
		wglMakeCurrent(hDC, hRC);
		glViewport(0, 0, width, height);
	}

	/*
	*/
	void GLPBuffer::unset() {
		wglMakeCurrent(WindowSystem::Get()->hDC, WindowSystem::Get()->hRC);
		glViewport(0, 0, WindowSystem::Get()->getWidth(), WindowSystem::Get()->getHeight());
	}

	/*
	*/
	void GLPBuffer::clear() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

}






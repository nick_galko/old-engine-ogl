/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//***************************************************************************
#include "EngineAPI.h"
//***************************************************************************

namespace Vega {

	/*
	*/
	GLDisplayList *GLDisplayList::Create() {
		GLDisplayList *list = new GLDisplayList();

		list->glID = glGenLists(1);
		return list;
	}

	/*
	*/
	void GLDisplayList::Destroy() {
		glDeleteLists(glID, 1);
		delete this;
	}

	/*
	*/
	void GLDisplayList::beginBuild() {
		glNewList(glID, GL_COMPILE);
	}

	/*
	*/
	void GLDisplayList::endBuild() {
		glEndList();
	}

	/*
	*/
	void GLDisplayList::call() {
		glCallList(glID);
	}

}
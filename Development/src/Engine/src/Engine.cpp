/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//***************************************************
#include "EngineAPI.h"
//***************************************************

namespace Vega {

#define ENGINE_VERSION_NUMBER 0.2.9.2
#define ENGINE_VERSION_STRING "0.2.9.2"
#define ENGINE_CONFIG "config.ini"

	Engine *engine;

	Engine *GetEngine()	{
		return engine;
	}
	/*
	*/
	Engine *Engine::Create() {
		if (engine) return engine;

		engine = new Engine();

		engine->init();

		return engine;
	}



	/*
	*/
	void  Engine::init() {
		log = new Log();
		LogPrintf("Vega engine "ENGINE_VERSION_STRING"\n");

		rc = false;
		ec = false;

		config = new Config(ENGINE_CONFIG);

		int w = config->getInt(CONF_WIDTH);
		int h = config->getInt(CONF_HEIGHT);
		int b = config->getInt(CONF_BPP);
		int z = config->getInt(CONF_ZDEPTH);
		bool f = config->getBool(CONF_FULLSCREEN);

		windowSystem = new WindowSystem(w, h, b, z, f);
		glSystem = new GLSystem();
		alSystem = new ALSystem();
		ilSystem = new ILSystem();
		physSystem = new PhysSystem();
		cache = new Cache();
		script = new ScriptSystem();
		scene = new Scene();
		/*
		System Initialize
		*/
		windowSystem->Initialize();
		glSystem->Initialize();
		alSystem->Initialize();
		physSystem->Initialize();
		script->Initialize();
		scene->Initialize();
		/*
		System requirements
		*/
		if (!glSystem->vboSupported())
			Error("GLSystem: your videocard does not support vertex buffer object.");
		if (!glSystem->occlusionQuerySupported())
			Error("GLSystem: your videocard does not support occlusion query.");
		if (!glSystem->glslSupported())
			Error("GLSystem: your videocard does not support GLSL.");
		if (!glSystem->pBufferSupported())
			Error("GLSystem: your videocard does not support PBuffer.");

		running = true;
	}

	/*
	*/
	Engine* Engine::Get() {
		return engine;
	}

	/*
	*/
	void Engine::Destroy()  {
		if (GUI::Get()) GUI::Get()->Destroy();
		SAFE_DESTROY(scene);
		SAFE_DELETE(script);
		SAFE_DELETE(physSystem);
		SAFE_DELETE(ilSystem);
		alSystem->Destroy();
		SAFE_DELETE(glSystem);

		windowSystem->Destroy();
	}

	/*
	*/
	void Engine::mainLoop() {
		_update();
	}


	/*
	*/
	void Engine::_update() {
		windowSystem->update();

		while (running) {
			windowSystem->update();
			physSystem->update(windowSystem->getDTime());

			if (GUI::Get()) GUI::Get()->update();
			if (ec) events_callback();
			if (scene) scene->update();

			glSystem->clear(GLSystem::COLOR_BUFFER | GLSystem::DEPTH_BUFFER | GLSystem::STENCIL_BUFFER);

			if (scene) scene->draw();
			if (rc) render_callback();
			if (GUI::Get()) GUI::Get()->draw();

			glSystem->flush();

			windowSystem->swapBuffers();
		}
	}

	/*
	*/
	void Engine::renderCallback(EngineCallback callback) {
		render_callback = callback;
		rc = true;
	}

	/*
	*/
	void Engine::eventsCallback(EngineCallback callback) {
		events_callback = callback;
		ec = true;
	}

	/*
	*/
	void Engine::quit() {
		running = false;
	}
}
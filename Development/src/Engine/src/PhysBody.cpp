/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//***************************************************************************
#include "EngineAPI.h"
#include "../../../Externals/newton/coreLibrary_300/source/newton/newton.h"
//***************************************************************************

namespace Vega {

	/*
	*/
	PhysBody *PhysBody::CreateBox(const Vec3 &size, float mass) {
		PhysBody *body = new PhysBody();

		body->force = Vec3(0, 0, 0);
		body->torque = Vec3(0, 0, 0);
		body->impulse = Vec3(0, 0, 0);
		body->velocity = Vec3(0, 0, 0);

		body->mass = mass;

		NewtonCollision *collision = NewtonCreateBox(Engine::Get()->physSystem->nWorld, size.x, size.y, size.z, 0, 0);

		Vec3 inertia, origin;
		NewtonConvexCollisionCalculateInertialMatrix(collision, inertia, origin);
		float Ixx = mass * inertia.x;
		float Iyy = mass * inertia.y;
		float Izz = mass * inertia.z;

		body->nBody = NewtonCreateDynamicBody(Engine::Get()->physSystem->nWorld, collision, origin);
		NewtonDestroyCollision(collision);

		NewtonBodySetUserData(body->nBody, body);
		NewtonBodySetAutoSleep(body->nBody, 0);

		if (mass > 0) {
			NewtonBodySetMassMatrix(body->nBody, mass, Ixx, Iyy, Izz);
			NewtonBodySetCentreOfMass(body->nBody, origin);
			NewtonBodySetForceAndTorqueCallback(body->nBody, applyForce_Callback);
		}

		body->impactSrc = NULL;

		return body;
	}

	/*
	*/
	PhysBody *PhysBody::CreateSphere(float radius, float mass) {
		PhysBody *body = new PhysBody();

		body->force = Vec3(0, 0, 0);
		body->torque = Vec3(0, 0, 0);
		body->impulse = Vec3(0, 0, 0);
		body->velocity = Vec3(0, 0, 0);

		body->mass = mass;

		NewtonCollision *collision = NewtonCreateSphere(Engine::Get()->physSystem->nWorld, radius, 0, 0);

		Vec3 inertia, origin;
		NewtonConvexCollisionCalculateInertialMatrix(collision, inertia, origin);
		float Ixx = mass * inertia.x;
		float Iyy = mass * inertia.y;
		float Izz = mass * inertia.z;

		body->nBody = NewtonCreateDynamicBody(Engine::Get()->physSystem->nWorld, collision, origin);//Nick:����������,����� ��?
		NewtonDestroyCollision(collision);

		NewtonBodySetUserData(body->nBody, body);
		NewtonBodySetAutoSleep(body->nBody, 0);

		if (mass > 0) {
			NewtonBodySetMassMatrix(body->nBody, mass, Ixx, Iyy, Izz);
			NewtonBodySetCentreOfMass(body->nBody, origin);
			NewtonBodySetForceAndTorqueCallback(body->nBody, applyForce_Callback);
		}

		body->impactSrc = NULL;

		return body;
	}

	/*
	*/
	PhysBody *PhysBody::CreateCylinder(float radius, float height, float mass) {
		PhysBody *body = new PhysBody();

		body->force = Vec3(0, 0, 0);
		body->torque = Vec3(0, 0, 0);
		body->impulse = Vec3(0, 0, 0);
		body->velocity = Vec3(0, 0, 0);

		body->mass = mass;

		NewtonCollision *collision = NewtonCreateCylinder(Engine::Get()->physSystem->nWorld, radius, height, 0, 0);

		Vec3 inertia, origin;
		NewtonConvexCollisionCalculateInertialMatrix(collision, inertia, origin);
		float Ixx = mass * inertia.x;
		float Iyy = mass * inertia.y;
		float Izz = mass * inertia.z;

		body->nBody = NewtonCreateDynamicBody(Engine::Get()->physSystem->nWorld, collision, origin);//Nick:����������,����� ��?
		NewtonDestroyCollision(collision);

		NewtonBodySetUserData(body->nBody, body);
		NewtonBodySetAutoSleep(body->nBody, 0);

		if (mass > 0) {
			NewtonBodySetMassMatrix(body->nBody, mass, Ixx, Iyy, Izz);
			NewtonBodySetCentreOfMass(body->nBody, origin);
			NewtonBodySetForceAndTorqueCallback(body->nBody, applyForce_Callback);
		}

		body->impactSrc = NULL;

		return body;
	}

	/*
	*/
	PhysBody *PhysBody::CreateCone(float radius, float height, float mass) {
		PhysBody *body = new PhysBody();

		body->force = Vec3(0, 0, 0);
		body->torque = Vec3(0, 0, 0);
		body->impulse = Vec3(0, 0, 0);
		body->velocity = Vec3(0, 0, 0);

		body->mass = mass;

		NewtonCollision *collision = NewtonCreateCone(Engine::Get()->physSystem->nWorld, radius, height, 0, 0);

		Vec3 inertia, origin;
		NewtonConvexCollisionCalculateInertialMatrix(collision, inertia, origin);
		float Ixx = mass * inertia.x;
		float Iyy = mass * inertia.y;
		float Izz = mass * inertia.z;

		body->nBody = NewtonCreateDynamicBody(Engine::Get()->physSystem->nWorld, collision, origin);//Nick:����������,����� ��?
		NewtonDestroyCollision(collision);

		NewtonBodySetUserData(body->nBody, body);
		NewtonBodySetAutoSleep(body->nBody, 0);

		if (mass > 0) {
			NewtonBodySetMassMatrix(body->nBody, mass, Ixx, Iyy, Izz);
			NewtonBodySetCentreOfMass(body->nBody, origin);
			NewtonBodySetForceAndTorqueCallback(body->nBody, applyForce_Callback);
		}

		body->impactSrc = NULL;

		return body;
	}


	/*
	*/
	PhysBody *PhysBody::CreateCapsule(float radius, float height, float mass) {
		PhysBody *body = new PhysBody();

		body->force = Vec3(0, 0, 0);
		body->torque = Vec3(0, 0, 0);
		body->impulse = Vec3(0, 0, 0);
		body->velocity = Vec3(0, 0, 0);

		body->mass = mass;

		NewtonCollision *collision = NewtonCreateCapsule(Engine::Get()->physSystem->nWorld, radius, height, 0, 0);

		Vec3 inertia, origin;
		NewtonConvexCollisionCalculateInertialMatrix(collision, inertia, origin);
		float Ixx = mass * inertia.x;
		float Iyy = mass * inertia.y;
		float Izz = mass * inertia.z;

		body->nBody = NewtonCreateDynamicBody(Engine::Get()->physSystem->nWorld, collision, origin);//Nick:����������,����� ��?
		NewtonDestroyCollision(collision);

		NewtonBodySetUserData(body->nBody, body);
		NewtonBodySetAutoSleep(body->nBody, 0);

		if (mass > 0) {
			NewtonBodySetMassMatrix(body->nBody, mass, Ixx, Iyy, Izz);
			NewtonBodySetCentreOfMass(body->nBody, origin);
			NewtonBodySetForceAndTorqueCallback(body->nBody, applyForce_Callback);
		}

		body->impactSrc = NULL;

		return body;
	}


	/*
	*/
	PhysBody *PhysBody::CreateChampferCylinder(float radius, float height, float mass) {
		PhysBody *body = new PhysBody();

		body->force = Vec3(0, 0, 0);
		body->torque = Vec3(0, 0, 0);
		body->impulse = Vec3(0, 0, 0);
		body->velocity = Vec3(0, 0, 0);

		body->mass = mass;

		NewtonCollision *collision = NewtonCreateChamferCylinder(Engine::Get()->physSystem->nWorld, radius, height, 0, 0);

		Vec3 inertia, origin;
		NewtonConvexCollisionCalculateInertialMatrix(collision, inertia, origin);
		float Ixx = mass * inertia.x;
		float Iyy = mass * inertia.y;
		float Izz = mass * inertia.z;

		body->nBody = NewtonCreateDynamicBody(Engine::Get()->physSystem->nWorld, collision, origin);//Nick:����������,����� ��?
		NewtonDestroyCollision(collision);

		NewtonBodySetUserData(body->nBody, body);
		NewtonBodySetAutoSleep(body->nBody, 0);

		if (mass > 0) {
			NewtonBodySetMassMatrix(body->nBody, mass, Ixx, Iyy, Izz);
			NewtonBodySetCentreOfMass(body->nBody, origin);
			NewtonBodySetForceAndTorqueCallback(body->nBody, applyForce_Callback);
		}

		body->impactSrc = NULL;

		return body;
	}

	/*
	*/
	PhysBody *PhysBody::CreateConvexHull(Vec3 *pos, const int numPos, float mass) {
		PhysBody *body = new PhysBody();

		body->force = Vec3(0, 0, 0);
		body->torque = Vec3(0, 0, 0);
		body->impulse = Vec3(0, 0, 0);
		body->velocity = Vec3(0, 0, 0);

		body->mass = mass;

		NewtonCollision *collision = NewtonCreateConvexHull(Engine::Get()->physSystem->nWorld, numPos, &pos[0].x, 3 * sizeof(float), 0, NULL, 0);

		Vec3 inertia, origin;
		NewtonConvexCollisionCalculateInertialMatrix(collision, inertia, origin);
		float Ixx = mass * inertia.x;
		float Iyy = mass * inertia.y;
		float Izz = mass * inertia.z;

		body->nBody = NewtonCreateDynamicBody(Engine::Get()->physSystem->nWorld, collision, origin);//Nick:����������,����� ��?
		NewtonDestroyCollision(collision);

		NewtonBodySetUserData(body->nBody, body);
		NewtonBodySetAutoSleep(body->nBody, 0);

		if (mass > 0) {
			NewtonBodySetMassMatrix(body->nBody, mass, Ixx, Iyy, Izz);
			NewtonBodySetCentreOfMass(body->nBody, origin);
			NewtonBodySetForceAndTorqueCallback(body->nBody, applyForce_Callback);
		}

		body->impactSrc = NULL;

		return body;
	}

	/*
	*/
	PhysBody *PhysBody::CreateStaticMesh(Vec3 *pos, const int numPos, bool optimize) {
		TODO("To clear code")
		PhysBody *body = new PhysBody();

		body->force = Vec3(0, 0, 0);
		body->torque = Vec3(0, 0, 0);
		body->impulse = Vec3(0, 0, 0);
		body->velocity = Vec3(0, 0, 0);

		NewtonCollision const*collision = NewtonCreateTreeCollision(Engine::Get()->physSystem->nWorld, 0);
		NewtonTreeCollisionBeginBuild(collision);
		for (int i = 0; i < numPos / 3; i++) {
			Vec3 v[3];
			v[0] = pos[i * 3 + 0];
			v[1] = pos[i * 3 + 1];
			v[2] = pos[i * 3 + 2];

			NewtonTreeCollisionAddFace(collision, 3, &v[0].x, 3 * sizeof(float), i + 1);
		}
		NewtonTreeCollisionEndBuild(collision, 1);
		body->nBody = NewtonCreateDynamicBody(Engine::Get()->physSystem->nWorld, collision, Mat4().identity());//Nick:����������,����� ��?
		NewtonDestroyCollision(collision);
		TODO("����������� ��� ��� ������ NewtonBodySetMassMatrix")
#if 0
			NewtonBodySetMassMatrix(body->nBody, 1000.0f, 1, 1, 1);
#else
			NewtonBodySetMassMatrix(body->nBody, 1000.0f, pos->x, pos->y, pos->z);
#endif
		NewtonBodySetUserData(body->nBody, body);

		body->impactSrc = NULL;

		return body;
	}

	/*
	*/
	PhysBody::~PhysBody() {
		NewtonDestroyBody(Engine::Get()->physSystem->nWorld, nBody);
		impactSrc->Destroy();
	}


	/*
	*/
	void PhysBody::setTransform(const Mat4 &transform) {
		NewtonBodySetMatrix(nBody, transform);
	}

	/*
	*/
	Mat4 PhysBody::getTransform() {
		Mat4 out;
		NewtonBodyGetMatrix(nBody, out);
		return out;
	}


	/*
	*/
	void PhysBody::addTorque(const Vec3 &torque) {
		this->torque += torque;
		this->force += torque * getMass();
	}

	/*
	*/
	Vec3 PhysBody::getTorque() {
		return torque;
	}


	/*
	*/
	void PhysBody::addForce(const Vec3 &force) {
		this->torque += force / getMass();
		this->force += force;
	}

	/*
	*/
	Vec3 PhysBody::getForce() {
		return force;
	}


	/*
	*/
	void PhysBody::addVelocity(const Vec3 &velocity) {
		NewtonBodySetVelocity(nBody, getVelocity() + velocity);
	}

	/*
	*/
	void PhysBody::setVelocity(const Vec3 &velocity) {
		NewtonBodySetVelocity(nBody, velocity);
	}

	/*
	*/
	Vec3 PhysBody::getVelocity() {
		Vec3 velocity;
		NewtonBodyGetVelocity(nBody, velocity);
		return velocity;
	}


	/*
	*/
	float PhysBody::getMass() {
		float Ixx, Iyy, Izz;
		float mass;

		NewtonBodyGetMassMatrix(nBody, &mass, &Ixx, &Iyy, &Izz);

		return mass;
	}

	/*
	*/
	void PhysBody::setMass(float mass) {
		NewtonBodySetMassMatrix(nBody, mass, 1, 1, 1);
	}

	/*
	*/
	void PhysBody::applyForce_Callback(const NewtonBody* body, float timestep, int threadIndex) {
		PhysBody *pBody = (PhysBody*)NewtonBodyGetUserData(body);
		NewtonBodySetForce(body, pBody->force);

		pBody->force = Vec3(0, 0, 0);
		pBody->torque = Vec3(0, 0, 0);
	}
}

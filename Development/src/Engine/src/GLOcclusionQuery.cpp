/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//***************************************************************************
#include "EngineAPI.h"
//***************************************************************************

namespace Vega {
	TODO("Replace ON CHC");
	/*
	*/
	GLOcclusionQuery *GLOcclusionQuery::Create() {
		GLOcclusionQuery *ocq = new GLOcclusionQuery();

		glGenQueriesARB(1, &ocq->glID);
		return ocq;
	}

	/*
	*/
	void GLOcclusionQuery::Destroy() {
		glDeleteQueriesARB(1, &glID);
		delete this;
	}

	/*
	*/
	void GLOcclusionQuery::beginRendering() {
		glBeginQueryARB(GL_SAMPLES_PASSED_ARB, glID);
	}

	/*
	*/
	void GLOcclusionQuery::endRendering() {
		glEndQueryARB(GL_SAMPLES_PASSED_ARB);
	}

	/*
	*/
	unsigned int GLOcclusionQuery::getResult() {
		unsigned int fc;
		glGetQueryObjectuivARB(glID, GL_QUERY_RESULT_ARB, &fc);
		return fc;
	}

}
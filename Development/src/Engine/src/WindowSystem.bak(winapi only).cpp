/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//***************************************************
#include "../Inc/WindowSystem.h"
#include "../Inc/Config.h"
#include "../Inc/GLSystem.h"
#include "../../Common/inc/LogFuncts.h"
//***************************************************
#ifdef WIN32
#include <windows.h>
#endif
//***************************************************

namespace Vega {

	WindowSystem *windowSystem;
	LRESULT	CALLBACK wndProc(HWND, UINT, WPARAM, LPARAM);

	/*
	*/
	WindowSystem *WindowSystem::Create(int width, int height, int bpp, int zdepth, bool fullscreen) {
		if (windowSystem) { return windowSystem; }

		windowSystem = new WindowSystem();

		LogHeader("WindowSystem::Create()");
		
		//-----read-config-values-----------------------------------
		windowSystem->width = width;
		windowSystem->height = height;
		windowSystem->bpp = bpp;
		windowSystem->zdepth = zdepth;
		windowSystem->fullscreen = fullscreen;

		unsigned int pixelFormat;
		WNDCLASS wc;
		DWORD dwExStyle;
		DWORD dwStyle;
		RECT windowRect;

		windowRect.left = (long)0;
		windowRect.right = (long)width;
		windowRect.top = (long)0;
		windowRect.bottom = (long)height;

		windowSystem->hInstance = GetModuleHandle(NULL);
		wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
		wc.lpfnWndProc = (WNDPROC)wndProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = windowSystem->hInstance;
		wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = NULL;
		wc.lpszMenuName = NULL;
		wc.lpszClassName = "OpenGL";

		if (!RegisterClass(&wc)) {
			Error("WindowSystem::Create() error: failed to register the window class ENGINE_API");
			return NULL;
		}

		if (fullscreen) {
			DEVMODE dmScreenSettings;
			memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));

			dmScreenSettings.dmSize = sizeof(dmScreenSettings);
			dmScreenSettings.dmPelsWidth = width;
			dmScreenSettings.dmPelsHeight = height;
			dmScreenSettings.dmBitsPerPel = bpp;
			dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

			if (ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL) {
				fullscreen = false;
			}
		}

		if (fullscreen) {
			dwExStyle = WS_EX_APPWINDOW;
			dwStyle = WS_POPUP;
		}
		else {
			dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
			dwStyle = WS_OVERLAPPEDWINDOW;
		}

		AdjustWindowRectEx(&windowRect, dwStyle, FALSE, dwExStyle);

		if (!(windowSystem->hWnd = CreateWindowEx(dwExStyle,			// Extended Style For The Window
			"OpenGL",							// Class Name
			"Vega Engine",						// Window Title
			dwStyle |							// Defined Window Style
			WS_CLIPSIBLINGS |					// Required Window Style
			WS_CLIPCHILDREN,					// Required Window Style
			0, 0,								// Window Position
			windowRect.right - windowRect.left,	// Calculate Window Width
			windowRect.bottom - windowRect.top,	// Calculate Window Height
			NULL,								// No Parent Window
			NULL,								// No Menu
			windowSystem->hInstance,			// Instance
			NULL)))								// Dont Pass Anything To WM_CREATE
		{
			Error("WindowSystem::Create() error: window creation error");
			return false;
		}

		static PIXELFORMATDESCRIPTOR pfd =
		{
			sizeof(PIXELFORMATDESCRIPTOR),
			1,											// Version Number
			PFD_DRAW_TO_WINDOW |						// Format Must Support Window
			PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
			PFD_DOUBLEBUFFER,							// Must Support Double Buffering
			PFD_TYPE_RGBA,								// Request An RGBA Format
			bpp,										// Select Our Color Depth
			0, 0, 0, 0, 0, 0,							// Color Bits Ignored
			0,											// No Alpha Buffer
			0,											// Shift Bit Ignored
			0,											// No Accumulation Buffer
			0, 0, 0, 0,									// Accumulation Bits Ignored
			zdepth,										// Z-Buffer (Depth Buffer)  
			0,											// No Stencil Buffer
			0,											// No Auxiliary Buffer
			PFD_MAIN_PLANE,								// Main Drawing Layer
			0,											// Reserved
			0, 0, 0										// Layer Masks Ignored
		};

		if (!(windowSystem->hDC = GetDC(windowSystem->hWnd)))	{
			Error("WindowSystem::Create() error: can't Create a GL device context");
			return NULL;
		}

		if (!(pixelFormat = ChoosePixelFormat(windowSystem->hDC, &pfd))) {
			Error("WindowSystem::Create() error: can't find a suitable pixel format");
			return NULL;
		}

		if (!(SetPixelFormat(windowSystem->hDC, pixelFormat, &pfd)))	{
			Error("WindowSystem::Create() error: can't set the pixel format");
			return NULL;
		}

		if (!(windowSystem->hRC = wglCreateContext(windowSystem->hDC)))	{
			Error("WindowSystem::Create() error: can't Create a GL rendering context");
			return NULL;
		}

		if (!wglMakeCurrent(windowSystem->hDC, windowSystem->hRC)) {
			Error("WindowSystem::Create() error: can't activate the GL rendering context");
			return NULL;
		}

		ShowWindow(windowSystem->hWnd, SW_SHOW);
		SetForegroundWindow(windowSystem->hWnd);
		SetFocus(windowSystem->hWnd);

		for (int i = 0; i < 3; i++) 
			windowSystem->mouseButtons[i] = false;

		for (int i = 0; i < 315; i++)
			windowSystem->keys[i] = false;

		windowSystem->mouseGrabed = false;

		LARGE_INTEGER freq;
		QueryPerformanceFrequency(&freq);
		windowSystem->frequency = freq.QuadPart;

		return windowSystem;
	}

	/*
	*/
	WindowSystem *WindowSystem::Get() {
		return windowSystem;
	}

	/*
	*/
	void WindowSystem::Destroy() {
		if (fullscreen) {
			ShowCursor(TRUE);
		}

		if (hRC) {
			wglMakeCurrent(NULL, NULL);
			wglDeleteContext(hRC);
			hRC = NULL;
		}

		if (hDC) {
			ReleaseDC(hWnd, hDC);
			hDC = NULL;
		}

		if (hWnd) {
			DestroyWindow(hWnd);
			hWnd = NULL;
		}

		UnregisterClass("OpenGL", hInstance);
		hInstance = NULL;
		delete this;
	}

	/*
	*/
	void WindowSystem::setTitle(const String &title) {
		SetWindowText(hWnd, title.c_str());
	}


	LRESULT CALLBACK wndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
		switch (uMsg)
		{
		case WM_CLOSE:
		{
			//PostQuitMessage(0);	
			exit(0);
			return 0;
		}

		case WM_KEYDOWN:
		{
			windowSystem->keys[wParam] = true;
			return 0;
		}

		case WM_KEYUP:
		{
			windowSystem->keys[wParam] = false;
			return 0;
		}

		case WM_MOUSEMOVE:
		{
			windowSystem->mx = LOWORD(lParam);
			windowSystem->my = HIWORD(lParam);
			windowSystem->mousing = true;
			return 0;
		}

		case WM_LBUTTONDOWN:
		{
			windowSystem->mouseButtons[0] = true;
			return 0;
		}

		case WM_LBUTTONUP:
		{
			windowSystem->mouseButtons[0] = false;
			return 0;
		}

		case WM_RBUTTONDOWN:
		{
			windowSystem->mouseButtons[1] = true;
			return 0;
		}

		case WM_RBUTTONUP:
		{
			windowSystem->mouseButtons[1] = false;
			return 0;
		}

		case WM_SIZE:
		{
			int w = LOWORD(lParam);
			int h = HIWORD(lParam);

			GLSystem::Get()->reshape(w, h);

			windowSystem->width = w;
			windowSystem->height = h;
			return 0;
		}
		}
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}

	/*
	*/
	void WindowSystem::update() {
		updateTimer();

		memcpy(oldKeys, keys, sizeof(keys));
		memcpy(oldMouseButtons, mouseButtons, sizeof(mouseButtons));
		mousing = false;

		MSG	msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))	{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (mouseGrabed) {
			oldMouseX = mouseX;
			oldMouseY = mouseY;
			mouseX += mx - width / 2;
			mouseY += my - height / 2;
			setMousePos(width / 2, height / 2);
		}
		else {
			oldMouseX = mouseX;
			oldMouseY = mouseY;
			mouseX = mx;
			mouseY = my;
		}
	}

	/*
	*/
	bool WindowSystem::isKeyPressed(Key key) {
		return keys[key];
	}

	/*
	*/
	bool WindowSystem::isKeyDown(Key key) {
		return (keys[key] && !oldKeys[key]);
	}

	/*
	*/
	bool WindowSystem::isKeyUp(Key key) {
		return (!keys[key] && oldKeys[key]);
	}

	/*
	*/
	bool WindowSystem::isMouseButtonPressed(MouseButton mb) {
		return mouseButtons[mb];
	}

	/*
	*/
	bool WindowSystem::isMouseButtonDown(MouseButton mb) {
		return (mouseButtons[mb] && !oldMouseButtons[mb]);
	}

	/*
	*/
	bool WindowSystem::isMouseButtonUp(MouseButton mb) {
		return (!mouseButtons[mb] && oldMouseButtons[mb]);
	}

	/*
	*/
	void WindowSystem::showCursor(bool show) {
		ShowCursor(show);
		cursorVisible = show;
	}

	/*
	*/
	void WindowSystem::setMousePos(int x, int y) {
		POINT pt;
		pt.x = x;
		pt.y = y;
		ClientToScreen(hWnd, &pt);
		SetCursorPos(pt.x, pt.y);
	}

	/*
	*/
	void WindowSystem::grabMouse(bool grab) {
		mouseX = oldMouseX = width / 2;
		mouseY = oldMouseY = height / 2;

		if (grab) { setMousePos(width / 2, height / 2); }
		showCursor(!grab);
		mouseGrabed = grab;
	}

	/*
	*/
	void WindowSystem::swapBuffers() {
		SwapBuffers(hDC);
	}

	/*
	*/
	void WindowSystem::updateTimer() {
		if (!tPause) {
			LARGE_INTEGER counter;
			QueryPerformanceCounter(&counter);
			int ticks = counter.QuadPart * 1000 / frequency;

			dTime = ticks - eTime;
			eTime = ticks;
		}
	}

	/*
	*/
	int WindowSystem::getTime() {
		LARGE_INTEGER counter;
		QueryPerformanceCounter(&counter);
		int ticks = counter.QuadPart * 1000 / frequency;

		return ticks;
	}

}

/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//**************************************
#include "EngineAPI.h"
//**************************************

namespace Vega {

	/*
	*/
	SkinnedMesh *SkinnedMesh::Create(const String &path) {
		SkinnedMesh *mesh = new SkinnedMesh();

		mesh->loadXSSMSH(path);

		mesh->calculateTBN();
		mesh->calculateBoundings();
		return mesh;
	}

	/*
	*/
	void SkinnedMesh::Destroy() {
		for (int s = 0; s < numSubsets; s++) {
			Subset *st = subsets[s];

			delete[] st->vertices;
			delete[] st->indices;
			delete st;
		}
		delete this;
	}

	/*
	*/
	void SkinnedMesh::setFrame(float frame, int from, int to) {

		if (numFrames == 0) return;

		if (from < 0) from = 0;
		if (to < 0) to = numFrames;

		int frame0 = (int)frame;
		frame -= frame0;
		frame0 += from;
		if (frame0 >= to) frame0 = (frame0 - from) % (to - from) + from;
		int frame1 = frame0 + 1;
		if (frame1 >= to) frame1 = from;

		for (int i = 0; i < numBones; i++) {	// calculate matrixes
			Mat4 translate;
			translate = Mat4::translate(frames[frame0][i].position * (1.0f - frame) + frames[frame1][i].position * frame);

			Quat rot;
			rot = Quat::slerp(frames[frame0][i].rotation, frames[frame1][i].rotation, frame);

			bones[i].rotation = Mat4(rot.toMatrix());
			bones[i].transform = translate * bones[i].rotation;
		}

		for (int i = 0; i < (int)numSubsets; i++) {	// calculate vertexes
			Subset *st = subsets[i];

			st->min = st->vertices[0].position;
			st->max = st->vertices[0].position;

			for (int j = 0; j < st->numVertices; j++) {
				Vertex *v = &st->vertices[j];

				v->position = Vec3(0, 0, 0);
				v->normal = Vec3(0, 0, 0);
				v->tangent = Vec3(0, 0, 0);
				v->binormal = Vec3(0, 0, 0);

				for (int k = 0; k < v->numWeights; k++) {
					Weight *w = &v->weights[k];
					v->position += bones[w->bone].transform * w->position * w->weight;
					v->normal -= bones[w->bone].rotation * w->normal * w->weight;
					v->tangent += bones[w->bone].rotation * w->tangent * w->weight;
					v->binormal -= bones[w->bone].rotation * w->binormal * w->weight;
				}

				if (st->max.x < v->position.x) st->max.x = v->position.x;
				if (st->min.x > v->position.x) st->min.x = v->position.x;
				if (st->max.y < v->position.y) st->max.y = v->position.y;
				if (st->min.y > v->position.y) st->min.y = v->position.y;
				if (st->max.z < v->position.z) st->max.z = v->position.z;
				if (st->min.z > v->position.z) st->min.z = v->position.z;
			}

			st->center = (st->min + st->max) / 2.0f;
			st->radius = (st->max - st->center).length();
		}

		min = subsets[0]->min;
		min = subsets[0]->max;

		for (int i = 0; i < (int)numSubsets; i++) {
			Subset *st = subsets[i];
			if (max.x < st->max.x) max.x = st->max.x;
			if (min.x > st->min.x) min.x = st->min.x;
			if (max.y < st->max.y) max.y = st->max.y;
			if (min.y > st->min.y) min.y = st->min.y;
			if (max.z < st->max.z) max.z = st->max.z;
			if (min.z > st->min.z) min.z = st->min.z;
		}

		center = (min + max) / 2.0f;
		radius = (max - center).length();
	}

	/*
	*/
	void SkinnedMesh::save(const String &path) {
		FStream *file = FStream::Create(path, FStream::WRITE_TEXT);

		//number of subsets

		file->printf("#XSYSTEM_ENGINE_SKINNED_MESH\n\n"); //#AST_ENGINE_MESH

		file->printf("num_bones %i\n", numBones); //num_bones
		for (int i = 0; i < numBones; i++) {
			file->printf("%s %i\n", bones[i].name, bones[i].parent); //num_bones
		}
		file->printf("\n");

		file->printf("num_subsets %i\n\n", numSubsets); //num_subsets

		//process subsets
		for (int s = 0; s < numSubsets; s++) {
			Subset *st = subsets[s];
			file->printf("subset %s\n", st->name);

			file->printf("num_vertices %i\n", st->numVertices);

			//process vertices
			for (int v = 0; v < st->numVertices; v++) {
				file->printf("num_weights %i\n", st->vertices[v].numWeights);
				for (int w = 0; w < st->vertices[v].numWeights; w++) {
					file->printf("%i %f %f %f %f %f %f %f\n",
						st->vertices[v].weights[w].bone,
						st->vertices[v].weights[w].weight,
						st->vertices[v].weights[w].position.x,
						st->vertices[v].weights[w].position.y,
						st->vertices[v].weights[w].position.z,
						st->vertices[v].weights[w].normal.x,
						st->vertices[v].weights[w].normal.y,
						st->vertices[v].weights[w].normal.z);
				}
				file->printf("%f %f\n",
					st->vertices[v].texcoord.x,
					st->vertices[v].texcoord.y);
			}

			//number of faces

			file->printf("num_faces %i\n", st->numIndices / 3);

			//process faces
			for (int i = 0; i < st->numIndices / 3; i++) {
				file->printf("%i %i %i\n",
					st->indices[i * 3 + 0],
					st->indices[i * 3 + 1],
					st->indices[i * 3 + 2]);
			}
			file->printf("\n");
		}

		file->printf("num_frames %i\n", numFrames);
		for (int i = 0; i < numFrames; i++) {
			for (int k = 0; k < numBones; k++) {
				file->printf("%f %f %f %f %f %f %f\n", frames[i][k].position.x,
					frames[i][k].position.y,
					frames[i][k].position.z,
					frames[i][k].rotation.x,
					frames[i][k].rotation.y,
					frames[i][k].rotation.z,
					frames[i][k].rotation.w);
			}
		}

		delete file;
	}

	/*
	*/
	void SkinnedMesh::drawSubset(int s) {
		Subset *st = subsets[s];

		glClientActiveTextureARB(GL_TEXTURE0_ARB);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), st->vertices->texcoord);

		glEnableClientState(GL_NORMAL_ARRAY);
		glNormalPointer(GL_FLOAT, sizeof(Vertex), st->vertices->normal);

		glClientActiveTextureARB(GL_TEXTURE1_ARB);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(3, GL_FLOAT, sizeof(Vertex), st->vertices->tangent);

		glClientActiveTextureARB(GL_TEXTURE2_ARB);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(3, GL_FLOAT, sizeof(Vertex), st->vertices->binormal);

		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer(3, GL_FLOAT, sizeof(Vertex), st->vertices->position);


		glDrawElements(GL_TRIANGLES, st->numIndices, GL_UNSIGNED_INT, st->indices);


		glDisableClientState(GL_VERTEX_ARRAY);

		glClientActiveTextureARB(GL_TEXTURE0_ARB);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		glDisableClientState(GL_NORMAL_ARRAY);

		glClientActiveTextureARB(GL_TEXTURE1_ARB);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		glClientActiveTextureARB(GL_TEXTURE2_ARB);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}

	/*
	*/
	int SkinnedMesh::getSubset(String name) {
		for (int s = 0; s < numSubsets; s++) {
			if (subsets[s]->name == name)
				return s;
		}
		return 0;
	}


	/*
	*/
	void SkinnedMesh::calculateTBN() {
		setFrame(0.0);

		for (int s = 0; s < numSubsets; s++) {
			Subset *st = subsets[s];

			for (int iLoop = 0; iLoop < st->numIndices / 3; iLoop++) {
				int ind0 = st->indices[iLoop * 3 + 0];
				int ind1 = st->indices[iLoop * 3 + 1];
				int ind2 = st->indices[iLoop * 3 + 2];

				Vec3 t[3];
				Vec3 b[3];

				TBNComputer::computeTBN(t[0], b[0],
					st->vertices[ind0].position,
					st->vertices[ind1].position,
					st->vertices[ind2].position,
					st->vertices[ind0].texcoord,
					st->vertices[ind1].texcoord,
					st->vertices[ind2].texcoord,
					st->vertices[ind0].normal);
				TBNComputer::computeTBN(t[1], b[1],
					st->vertices[ind1].position,
					st->vertices[ind2].position,
					st->vertices[ind0].position,
					st->vertices[ind1].texcoord,
					st->vertices[ind2].texcoord,
					st->vertices[ind0].texcoord,
					st->vertices[ind1].normal);
				TBNComputer::computeTBN(t[2], b[2],
					st->vertices[ind2].position,
					st->vertices[ind0].position,
					st->vertices[ind1].position,
					st->vertices[ind2].texcoord,
					st->vertices[ind0].texcoord,
					st->vertices[ind1].texcoord,
					st->vertices[ind2].normal);

				for (int w = 0; w < st->vertices[ind0].numWeights; w++) {
					st->vertices[ind0].weights[w].tangent += t[0];
					st->vertices[ind0].weights[w].binormal += b[0];
				}
				for (int w = 0; w < st->vertices[ind1].numWeights; w++) {
					st->vertices[ind1].weights[w].tangent += t[1];
					st->vertices[ind1].weights[w].binormal += b[1];
				}
				for (int w = 0; w < st->vertices[ind2].numWeights; w++) {
					st->vertices[ind2].weights[w].tangent += t[2];
					st->vertices[ind2].weights[w].binormal += b[2];
				}
			}

			for (int vLoop = 0; vLoop < st->numVertices; vLoop++) {
				for (int w = 0; w < st->vertices[vLoop].numWeights; w++) {
					st->vertices[vLoop].weights[w].tangent = Vec3::normalize(st->vertices[vLoop].weights[w].tangent);
					st->vertices[vLoop].weights[w].binormal = Vec3::normalize(st->vertices[vLoop].weights[w].binormal);
				}
			}
		}

		setFrame(0.0);
	}

	/*
	*/
	void SkinnedMesh::calculateBoundings() {
		for (int s = 0; s < numSubsets; s++) {
			Subset *st = subsets[s];

			for (int v = 0; v < st->numVertices; v++) {
				//need to do so because of MSVC 2005 bug
				if (v == 0) {
					st->min = st->vertices[0].position;
					st->max = st->vertices[0].position;
				}
				st->max = st->vertices[0].position;
				st->min.x = min(st->min.x, st->vertices[v].position.x);
				st->min.y = min(st->min.y, st->vertices[v].position.y);
				st->min.z = min(st->min.z, st->vertices[v].position.z);

				st->max.x = max(st->max.x, st->vertices[v].position.x);
				st->max.y = max(st->max.y, st->vertices[v].position.y);
				st->max.z = max(st->max.z, st->vertices[v].position.z);
			}

			st->radius = 0;
			st->center = (st->max + st->min) * 0.5;

			for (int v = 0; v < st->numVertices; v++) {
				st->radius = max(st->radius, (st->vertices[v].position - st->center).length());
			}
		}

		min = subsets[0]->min;
		max = subsets[0]->max;

		for (int s = 0; s < numSubsets; s++) {
			Subset *st = subsets[s];

			min.x = min(min.x, st->min.x);
			min.y = min(min.y, st->min.y);
			min.z = min(min.z, st->min.z);

			max.x = max(max.x, st->max.x);
			max.y = max(max.y, st->max.y);
			max.z = max(max.z, st->max.z);
		}

		center = (min + max) * 0.5;
		radius = 0;

		for (int s = 0; s < numSubsets; s++) {
			Subset *st = subsets[s];
			for (int v = 0; v < st->numVertices; v++) {
				radius = max(radius, (st->vertices[v].position - center).length());
			}
		}
	}

	/*
	*/
	void SkinnedMesh::loadXSSMSH(const String &path) {
		//begin loading
		FStream *file = FStream::Create(path, FStream::READ_TEXT);

		//Check if exist
		if (!file) {
			Error("SkinnedMesh::SkinnedMesh() error: object file '%s' not found", path);
			return;
		}

		String line;

		file->gets(); //#XSYSTEM_ENGINE_SKINNED_MESH
		file->gets(); //_

		//num_bones
		line = file->gets(); //num_bones
		numBones = line.getWord(2).toInt();
		bones = new Bone[numBones];

		for (int i = 0; i < numBones; i++) {
			line = file->gets();

			bones[i].name = line.getQuotedWord(1);
			bones[i].parent = line.getWord(2).toInt();
		}

		file->gets(); //_

		//num_subsets
		line = file->gets();
		numSubsets = line.getWord(2).toInt();
		subsets = new Subset*[numSubsets];

		file->gets(); //_

		//process subsets
		for (int s = 0; s < numSubsets; s++) {
			subsets[s] = new Subset();
			Subset *st = subsets[s];

			//read the surface name
			line = file->gets();
			st->name = line.getQuotedWord(2);

			//number of vertices
			line = file->gets();
			st->numVertices = line.getWord(2).toInt();
			st->vertices = new Vertex[st->numVertices];

			//process vertices
			for (int v = 0; v < st->numVertices; v++) {
				line = file->gets();
				st->vertices[v].numWeights = line.getWord(2).toInt();
				st->vertices[v].weights = new Weight[st->vertices[v].numWeights];

				for (int w = 0; w < st->vertices[v].numWeights; w++) {
					line = file->gets();
					st->vertices[v].weights[w].bone = line.getWord(1).toInt();
					st->vertices[v].weights[w].weight = line.getWord(2).toFloat();
					st->vertices[v].weights[w].position.x = line.getWord(3).toFloat();
					st->vertices[v].weights[w].position.y = line.getWord(4).toFloat();
					st->vertices[v].weights[w].position.z = line.getWord(5).toFloat();
					st->vertices[v].weights[w].normal.x = line.getWord(6).toFloat();
					st->vertices[v].weights[w].normal.y = line.getWord(7).toFloat();
					st->vertices[v].weights[w].normal.z = line.getWord(8).toFloat();
				}

				line = file->gets();
				st->vertices[v].texcoord.x = line.getWord(1).toFloat();
				st->vertices[v].texcoord.y = line.getWord(2).toFloat();
			}

			//number of faces
			line = file->gets();
			st->numIndices = line.getWord(2).toInt() * 3;
			st->indices = new unsigned int[st->numIndices];

			//process faces
			for (int i = 0; i < st->numIndices / 3; i++) {
				line = file->gets();
				st->indices[i * 3 + 0] = line.getWord(1).toInt();
				st->indices[i * 3 + 1] = line.getWord(2).toInt();
				st->indices[i * 3 + 2] = line.getWord(3).toInt();
			}

			line = file->gets(); //_
		}

		line = file->gets();
		numFrames = line.getWord(2).toInt();
		frames = new Frame*[numFrames];

		for (int f = 0; f < numFrames; f++) {
			frames[f] = new Frame[numBones];

			for (int i = 0; i < numBones; i++) {
				line = file->gets();
				frames[f][i].position.x = line.getWord(1).toFloat();
				frames[f][i].position.y = line.getWord(2).toFloat();
				frames[f][i].position.z = line.getWord(3).toFloat();
				frames[f][i].rotation.x = line.getWord(4).toFloat();
				frames[f][i].rotation.y = line.getWord(5).toFloat();
				frames[f][i].rotation.z = line.getWord(6).toFloat();
				frames[f][i].rotation.w = line.getWord(7).toFloat();
			}
		}

		file->Destroy();
	}


}





/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//**************************************
#include "EngineAPI.h"
//**************************************

namespace Vega {


	/*
	*/
	Config::Config(const String &path) {
		FStream *file = FStream::Create(path, FStream::READ_TEXT);

		if (!file) {
			Error("Config::Create() error: config file '%s' not found", path);
			return;
		}

		while (!file->eof()) {
			String line = file->gets();
			if ((line.data()[0] == '/' && line.data()[1] == '/') || line.getWordCount() < 2) {
				continue;
			}

			ConfigVar cv;
			cv.name = line.getWord(1);
			cv.value = line.getWord(2);

			vars.push_back(cv);
		}

		delete file;
	}

	/*
	*/
	Config *Config::Get() {
		return GetEngine()->config;
	}

	/*
	*/
	void Config::save(const String &path) {
		FStream *file = FStream::Create(path, FStream::WRITE_TEXT);

		for (int i = 0; i < vars.size(); i++) {
			String line = vars[i].name + " " + vars[i].value + "\n";
			file->printf(line);
		}

		delete file;
	}

	/*
	*/
	float Config::getFloat(const String &name) {
		for (int i = 0; i < vars.size(); i++) {
			if (vars[i].name == name)
				return vars[i].value.toFloat();
		}
		return 0;
	}

	/*
	*/
	int Config::getInt(const String &name) {
		for (int i = 0; i < vars.size(); i++) {
			if (vars[i].name == name)
				return vars[i].value.toInt();
		}
		return 0;
	}

	/*
	*/
	bool Config::getBool(const String &name) {
		for (int i = 0; i < vars.size(); i++) {
			if (vars[i].name == name)
				return vars[i].value.toBool();
		}
		return false;
	}

	/*
	*/
	void Config::setFloat(const String &name, float value) {
		for (int i = 0; i < vars.size(); i++) {
			if (vars[i].name == name) {
				vars[i].value = String(value);
				return;
			}
		}
	}

	/*
	*/
	void Config::setInt(const String &name, int value) {
		for (int i = 0; i < vars.size(); i++) {
			if (vars[i].name == name) {
				vars[i].value = String(value);
				return;
			}
		}
	}

	/*
	*/
	void Config::setBool(const String &name, bool value) {
		for (int i = 0; i < vars.size(); i++) {
			if (vars[i].name == name) {
				vars[i].value = String(value);
				return;
			}
		}
	}

	/*
	*/
	void Config::setString(const String &name, const String &value) {
		for (int i = 0; i < vars.size(); i++) {
			if (vars[i].name == name) {
				vars[i].value = value;
				return;
			}
		}
	}

}
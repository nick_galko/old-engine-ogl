/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//***************************************************************************
#include "EngineAPI.h"
//***************************************************************************

namespace Vega {

#define BMP "bmp"
#define TGA "tga"
#define JPG "jpg"
#define PNG "png"
#define DDS "dds"
#define GIF "gif"

	int BPP2Format(int bpp) {
		if (bpp == 3) return ILImage::RGB;
		if (bpp == 4) return ILImage::RGBA;
		return 0;
	}

	int Format2Bpp(int format) {
		if (format == ILImage::RGB) return 3;
		if (format == ILImage::RGBA) return 4;
		return 0;
	}

	/*
	*/
	ILImage *ILImage::CreateEmpty2d(int width, int height, int format) {
		ILImage *image = new ILImage();

		image->width = width;
		image->height = height;
		image->depth = 1;
		image->bpp = Format2Bpp(format);
		image->format = format;
		image->data = new ILubyte[image->getSize()];

		return image;
	}

	/*
	*/
	ILImage *ILImage::CreateNoise2d(int width, int height, int format) {
		ILImage *image = new ILImage();

		image->width = width;
		image->height = height;
		image->depth = 1;
		image->bpp = Format2Bpp(format);
		image->format = format;
		image->data = new ILubyte[image->getSize()];

		for (int i = 0; i < image->getSize(); i++) {
			image->data[i] = (ILubyte)rand() % 255;
		}

		return image;
	}

	/*
	*/
	ILImage *ILImage::CreateEmpty3d(int width, int height, int depth, int format) {
		ILImage *image = new ILImage();

		image->width = width;
		image->height = height;
		image->depth = depth;
		image->bpp = Format2Bpp(format);
		image->format = format;
		image->data = new ILubyte[image->getSize()];

		return image;
	}

	/*
	*/
	ILImage *ILImage::CreateNoise3d(int width, int height, int depth, int format) {
		ILImage *image = new ILImage();

		image->width = width;
		image->height = height;
		image->depth = depth;
		image->bpp = Format2Bpp(format);
		image->format = format;
		image->data = new ILubyte[image->getSize()];

		for (int i = 0; i < image->getSize(); i++) {
			image->data[i] = (ILubyte)rand() % 255;
		}

		return image;
	}

	/*
	*/
	ILImage *ILImage::Create2d(const String &path) {
		ILImage *image = new ILImage();

		String ext = String(path).getFileExt();

		ilOriginFunc(IL_ORIGIN_UPPER_LEFT);

		if (ext == BMP)
			ilLoad(IL_BMP, (const ILstring)path.c_str());
		if (ext == TGA)
			ilLoad(IL_TGA, (const ILstring)path.c_str());
		if (ext == JPG)
			ilLoad(IL_JPG, (const ILstring)path.c_str());
		if (ext == PNG)
			ilLoad(IL_PNG, (const ILstring)path.c_str());
		if (ext == DDS)
			ilLoad(IL_DDS, (const ILstring)path.c_str());
		if (ext == GIF)
			ilLoad(IL_GIF, (const ILstring)path.c_str());

		int error = ilGetError();
		if (error != IL_NO_ERROR) {
			LogPrintf("ILImage::Create2d() error: %s", iluErrorString(error));
			return NULL;
		}

		image->width = ilGetInteger(IL_IMAGE_WIDTH);
		image->height = ilGetInteger(IL_IMAGE_HEIGHT);
		image->depth = 1;
		image->bpp = ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL);
		image->format = BPP2Format(image->bpp);

		//int format = ilGetInteger(IL_IMAGE_FORMAT);
		//if(format == IL_BGR || format == IL_BGRA) {
		//	iluSwapColours();
		//}

		ILubyte *tempData = ilGetData();
		image->data = new ILubyte[image->getSize()];

		memcpy(image->data, tempData, image->getSize());

		return image;
	}

	/*
	*/
	void ILImage::Destroy() {
		free(data);
		delete this;
	}

	/*
	*/
	void ILImage::toNormalMap(int k) {
		int w = width;
		int h = height;
		if (depth > 1)
			return;

		int byteCount = w*h;
		ILubyte* ndata = new ILubyte[byteCount];

		for (int i = 0; i < byteCount; i++) {
			ndata[i] = (data[i*bpp] + data[i*bpp + 1] + data[i*bpp + 2]) / 3.0;
		}

		delete[] data;
		data = new ILubyte[byteCount * 4];
		bpp = 4;
		format = RGBA;

		float oneOver255 = 1.0 / 255.0;
		int offs = 0;

		for (int i = 0; i < w; i++) {
			for (int j = 0; j < h; j++) {
				float c = ndata[i*h + j] * oneOver255;
				float cx = ndata[i*h + (j + 1) % w] * oneOver255;
				float cy = ndata[((i + 1) % h)*h + j] * oneOver255;

				float dx = (c - cx) * k;
				float dy = (c - cy) * k;

				float len = (float)sqrt(dx*dx + dy*dy + 1);

				float nx = dy / len;
				float ny = -dx / len;
				float nz = 1.0 / len;

				data[offs + 0] = (ILubyte)(128 + 127 * nx);
				data[offs + 1] = (ILubyte)(128 + 127 * ny);
				data[offs + 2] = (ILubyte)(128 + 127 * nz);
				data[offs + 3] = ndata[i*h + j];
				offs += 4;
			}
		}
		delete[] ndata;
	}

	/*
	*/
	void ILImage::toGreyScale() {
		int w = width;
		int h = height;
		int d = depth;

		for (int i = 0; i < w*h*d; i++)
		{
			ILubyte color = (data[i*bpp]
				+ data[i*bpp + 1]
				+ data[i*bpp + 2]) / 3.0;

			data[i*bpp] = color;
			data[i*bpp + 1] = color;
			data[i*bpp + 2] = color;
		}
	}

}



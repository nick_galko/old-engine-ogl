/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//**************************************
#include "EngineAPI.h"
//**************************************

namespace Vega {

	GUI *gui;

	//**************************************************************************
	//GUI class ENGINE_API
	//**************************************************************************
	/*
	*/
	GUI *GUI::Create(const String &folder) {
		if (gui) return gui;

		gui = new GUI();

		gui->alpha = 1.0;

		gui->windowTex = GLTexture::Create2d(folder + "/window.png");
		gui->windowTex->setWrap(GLTexture::CLAMP_TO_EDGE);

		gui->buttonTex = GLTexture::Create2d(folder + "/button.tga");
		gui->buttonTex->setWrap(GLTexture::CLAMP_TO_EDGE);

		gui->buttonFocusedTex = GLTexture::Create2d(folder + "/button_pressed.tga");
		gui->buttonFocusedTex->setWrap(GLTexture::CLAMP_TO_EDGE);

		gui->checkBoxTex = GLTexture::Create2d(folder + "/check_box.tga");
		gui->checkBoxTex->setWrap(GLTexture::CLAMP_TO_EDGE);

		gui->checkBoxCheckedTex = GLTexture::Create2d(folder + "/check_box_checked.tga");
		gui->checkBoxCheckedTex->setWrap(GLTexture::CLAMP_TO_EDGE);

		gui->hSliderTex = GLTexture::Create2d(folder + "/hslider.png");
		gui->hSliderTex->setWrap(GLTexture::CLAMP_TO_EDGE);

		gui->font = Font::Create(folder + "/font.png");

		return gui;
	}

	/*
	*/
	GUI *GUI::Get() {
		return gui;
	}

	/*
	*/
	void GUI::Destroy() {
		windowTex->Destroy();
		buttonTex->Destroy();
		buttonFocusedTex->Destroy();
		checkBoxTex->Destroy();
		checkBoxCheckedTex->Destroy();
		hSliderTex->Destroy();

		font->Destroy();
		delete this;
	}

	/*
	*/
	void GUI::addWidget(Widget *widget) {
		widget->setEnabled(true);
		widgets.push_back(widget);
	}

	/*
	*/
	void GUI::removeWidget(Widget *widget) {
		for (int i = 0; i < widgets.size(); i++) {
			if (widgets[i] == widget)
				widgets[i] = NULL;
		}
	}

	/*
	*/
	void GUI::draw() {
		for (int i = 0; i < widgets.size(); i++) {
			if (widgets[i]) {
				widgets[i]->draw();
			}
		}
	}

	/*
	*/
	void GUI::update() {
		for (int i = 0; i < widgets.size(); i++) {
			if (widgets[i]) {
				widgets[i]->update();
			}
		}
	}

	/*
	*/
	bool GUI::openFileDialog(const String &filter, const String &title, String &path) {
		char out[1024];
		out[0] = '\0';

		OPENFILENAME ofn;

		ZeroMemory(&ofn, sizeof(OPENFILENAME));
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = (HWND)WindowSystem::Get()->hWnd;
		ofn.lpstrFile = out;
		ofn.nMaxFile = _MAX_PATH;;
		ofn.lpstrFilter = filter.c_str();  //"Targa(*.TGA)\0*.TGA\0All(*.*)\0*.*\0";
		ofn.nFilterIndex = 1;
		ofn.lpstrFileTitle = NULL;
		ofn.nMaxFileTitle = 0;
		ofn.lpstrInitialDir = NULL;
		ofn.lpstrFileTitle = "TGAtoDOT3 Open File";
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

		bool s = GetOpenFileName(&ofn);
		path = out;
		return s;
	}

}



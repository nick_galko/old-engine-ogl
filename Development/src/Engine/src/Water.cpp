/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

//************************************
#include "EngineAPI.h"
//************************************

namespace Vega {

	//**************************************************************************
	//Water
	//**************************************************************************
	/*
	*/
	Water *Water::Create() {
		Water *water = new Water();

		water->material = Material::Create("../gamedata/materials/engine_materials/water.xsmtr");

		water->size = 1e4;
		water->depth = 10;

		Scene::Get()->setWater(water);
		return water;
	}

	/*
	*/
	void Water::Destroy() {
		delete material;
		delete this;
	}

	/*
	*/
	void Water::draw() {
		GLSystem::Get()->disableCulling();

		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex3f(-size, depth, -size);

		glTexCoord2f(0, size*0.05);
		glVertex3f(-size, depth, size);

		glTexCoord2f(size*0.05, size*0.05);
		glVertex3f(size, depth, size);

		glTexCoord2f(size*0.05, 0);
		glVertex3f(size, depth, -size);
		glEnd();

		GLSystem::Get()->enableCulling();
	}

	/*
	*/
	bool Water::isVisible() {
		GLSystem::Get()->colorMask(false, false, false, false);
		GLSystem::Get()->depthMask(false);

		Scene::Get()->query->beginRendering();

		draw();

		Scene::Get()->query->endRendering();

		GLSystem::Get()->depthMask(true);
		GLSystem::Get()->colorMask(true, true, true, true);

		return (Scene::Get()->query->getResult() > 2);
	}

}

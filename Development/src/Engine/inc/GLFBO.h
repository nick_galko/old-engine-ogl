/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once 

//***************************************************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/IncludesAndLibs.h"
//***************************************************************************
#include "../Inc/MathLib.h"
//***************************************************************************

namespace Vega {

	/**
	FBO class
	*/
	class ENGINE_API GLFBO {
	public:

		/**
		Creates GLFBO
		\param width  fbo width
		\param height  fbo height
		\param depthFormat  depth renderbuffer format
		\return pointer to new GLFBO
		*/
		static GLFBO *Create(int width, int height);

		/**
		Destroys GLFBO
		*/
		void Destroy();

		/**
		Adds color attachment to GLFBO
		*/
		void CreateColorAttachment();

		/**
		Adds depth attachment to GLFBO
		*/
		void CreateDepthAttachment();

		/**
		Adds stencil attachment to GLFBO
		*/
		void CreateStencilAttachment();

		/**
		Sets color render target
		\param texture render target
		\param cubemap face if texture is s cube texture
		*/
		void setColorTarget(GLTexture *texture = NULL, int face = -1);

		/**
		Sets depth render target
		\param texture render target
		*/
		void setDepthTarget(GLTexture *texture = NULL);

		/**
		Sets GLFBO
		*/
		void set();

		/**
		Unsets GLFBO
		*/
		void unset();

		/**
		Clears the frame buffer
		*/
		void clear();

		/**
		Flushes the frame buffer
		*/
		void flush();

	private:
		GLuint glID;

		GLuint glColorID;
		GLuint glStencilID;
		GLuint glDepthID;

		GLTexture *colorTarget;
		GLTexture *depthTarget;

		int width, height;
	};
}
/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once 

//***************************************************
//***************************************************
#define _CRT_SECURE_NO_DEPRICATE
#define ENGINE_API __declspec(dllexport)
#pragma warning(disable:4996)

#pragma warning(disable:4251)

#pragma warning(disable:4244)
#pragma warning(disable:4305)

#pragma warning(disable:4172)

#pragma warning(disable:4018)

#pragma warning(disable:4267)
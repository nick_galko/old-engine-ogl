/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/GLSystem.h"
#include "../Inc/Material.h"
//************************************

namespace Vega {

	/**
	Water class
	*/
	class ENGINE_API Water {
	public:

		/**
		water constructor
		*/
		static Water *Create();

		/**
		water deconstructor
		*/
		void Destroy();

		/**
		draw water
		*/
		void draw();

		void setSize(float size) { this->size = size; }
		float getSize() { return size; }

		void setDepth(float depth) { this->depth = depth; }
		float getDepth() { return depth; }

		void setMaterial(const String &path) { this->material = Material::Create(path); }
		Material *getMaterial() { return material; }


		/**
		is water visible
		*/
		bool isVisible();

	private:
		Material *material;

		float size;
		float depth;
	};

}
/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//**************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/GLSystem.h"
#include "../Inc/Font.h"
#include <vector>
//**************************************

namespace Vega {

	/**
	GUI Widget item
	*/
	class ENGINE_API Widget {
	public:
		/**
		Adds widget to the GUI
		\param widget pointer to the Widget to add
		*/
		virtual void addWidget(Widget *widget);

		/**
		Removes Widget from the GUI
		\param widget pointer to the Widget to remove
		*/
		virtual void removeWidget(Widget *widget);

		/**
		Draw Widget
		*/
		virtual	void draw();

		/**
		Update Widget
		*/
		virtual	void update();

		virtual void setPosition(int x, int y) { posX = x; posY = y; }
		virtual int getX() { if (getParent()) return posX + parent->getX(); else return posX; }
		virtual int getY() { if (getParent()) return posY + parent->getY(); else return posY; }

		virtual void setSize(int w, int h) { width = w; height = h; }
		virtual int getWidth() { return width; }
		virtual int getHeight() { return height; }

		virtual Widget *getParent() { return parent; }
		virtual void setParent(Widget *parent) { this->parent = parent; }

		virtual void setEnabled(bool enabled) { this->enabled = enabled; }
		virtual void toggleEnable() { enabled = !enabled; }
		virtual bool isEnabled() {
			if (parent) return enabled && parent->isEnabled();
			else return enabled;
		}

		typedef void(*WidgetCallback)(Widget*);

		bool isClicked() { return clicked; }
		bool isPressed() { return pressed; }
		bool isFocused() { return focused; }
		bool isFocusedOut() { return focusOut; }
		bool isFocusedIn() { return focusIn; }

	protected:
		int posX, posY;
		int width, height;

		bool enabled;

		Widget *parent;
		std::vector<Widget*> children;

		int pressedFlag;

		bool clicked;
		bool pressed;
		bool focusIn, focusOut, focused;

		friend class WidgetRadioGroup;
	};

	/**
	GUI button element
	*/
	class ENGINE_API WidgetButton : public Widget {
	public:

		/**
		Create WidgetButton
		\param text text string
		\return pointer to new WidgetButton
		*/
		static WidgetButton *Create(const String &text);

		/**
		Destroy WidgetButton
		*/
		void Destroy();

		/**
		Draw WidgetButton
		*/
		virtual	void draw();

		/**
		Update WidgetButton
		*/
		virtual	void update();

	private:
		String text;
		int size;
	};

	/**
	GUI check box
	*/
	class ENGINE_API WidgetCheckBox : public Widget {
	public:

		/**
		Create WidgetCheckBoxheckBox
		\param text label text string
		\return pointer to new WidgetC
		*/
		static WidgetCheckBox *Create(const String &text);

		/**
		Destroy GUI
		*/
		void Destroy();

		bool isChecked() { return checked; }
		void setChecked(bool checked) { this->checked = checked; }
		void toggleCheck() { checked = !checked; }

		/**
		Draw widgets
		*/
		virtual	void draw();

		/**
		Update widget
		*/
		virtual	void update();

	private:
		bool checked;

		String text;
	};

	/**
	GUI radio group
	*/
	class ENGINE_API WidgetRadioGroup : public Widget {
	public:
		/**
		WidgetRadioGroup constructor
		*/
		static WidgetRadioGroup *Create();

		/**
		GUI destructor
		*/
		void Destroy();

		void addCheckBox(WidgetCheckBox *cbox);
		void removeCheckBox(WidgetCheckBox *cbox);

		/**
		draw all GUI widgets
		*/
		virtual	void draw();

		virtual	void update();

	private:
		std::vector<WidgetCheckBox*> checkBoxes;
	};

	/**
	GUI label element
	*/
	class ENGINE_API WidgetLabel : public Widget {
	public:

		/**
		WidgetButton constructor
		*/
		static WidgetLabel *Create(const String &text);

		/**
		GUI destructor
		*/
		void Destroy();


		/**
		draw WidgetWindow
		*/
		virtual void draw();

		virtual void update();

		void setText(const String text) { this->text = text; width = height * text.size(); }

	private:
		String text;
	};

	/**
	GUI horizontal slider element
	*/
	class ENGINE_API WidgetHSlider : public Widget {
	public:

		/**
		WidgetWindow costructor
		\param w size
		\param   size in pixels
		\param x position
		\param
		*/
		static WidgetHSlider *Create();

		/**
		WidgetButton destructor
		*/
		void Destroy();

		/**
		draw all GUI widgets
		*/
		virtual void draw();

		virtual	void update();

		int getValue() { return value; }
		void setValue(int v) { value = v; pos = width*v*0.01; }

	private:
		int value;
		int pos;
	};

	/**
	Window GUI element
	*/
	class ENGINE_API WidgetWindow : public Widget {
	public:
		/**
		Create WidgetWindow
		*/
		static WidgetWindow *Create();

		/**
		WidgetWindow destructor
		*/
		void Destroy();


		/**
		draw WidgetWindow
		*/
		virtual void draw();

		virtual void update();

		void setMoveable(bool moveable);
		bool getMoveable();
	private:
		bool moveable;
	};
}
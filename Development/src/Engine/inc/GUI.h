/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//**************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/GLSystem.h"
#include "../Inc/Widget.h"
#include "../Inc/Font.h"
#include <vector>
//**************************************

namespace Vega {

	/**
	Gui class. Created one time.
	*/
	class ENGINE_API GUI {
	public:

		/**
		Creates new GUI
		\param folder folder where GUI textures are
		\return pointer to new GUI
		*/
		static GUI *Create(const String &folder);

		/**
		Returns existing GUI
		\return pointer to the existing GUI
		*/
		static GUI *Get();

		/**
		Destroys GUI
		*/
		void Destroy();

		/**
		Adds Widget to the GUI
		\param widget pointer to the Widget to add
		*/
		void addWidget(Widget *widget);

		/**
		Removes Widget from the GUI
		\param widget pointer to the Widget to remove
		*/
		void removeWidget(Widget *widget);

		/**
		Draw all GUI widgets
		*/
		void draw();

		/**
		Update all GUI widgets
		*/
		void update();

		/**
		Set alpha value
		\param alpha value to set
		*/
		void setAlpha(float alpha) { this->alpha = alpha; }

		/**
		Get alpha channel value
		\return alpha value
		*/
		float getAlpha() { return alpha; }
		bool openFileDialog(const String &filter, const String &title, String &path);

	private:
		GLTexture *windowTex;

		GLTexture *buttonTex;
		GLTexture *buttonFocusedTex;

		GLTexture *checkBoxTex;
		GLTexture *checkBoxCheckedTex;

		GLTexture *hSliderTex;

		Font *font;
		float alpha;

		std::vector<Widget*> widgets;

		friend class Widget;
		friend class WidgetButton;
		friend class WidgetCheckBox;
		friend class WidgetHSlider;
		friend class WidgetLabel;
		friend class WidgetWindow;
	};
}
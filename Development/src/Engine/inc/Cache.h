/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//**************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/String.h"
#include "../Inc/Mesh.h"
#include "../Inc/Material.h"
#include "../Inc/ALSound.h"
#include <vector>
#include <map>
//**************************************

namespace Vega {

	class ENGINE_API Cache {
	public:
		Cache();
		static Cache *Get();

		Material *loadMaterial(const String &path);
		Mesh *loadMesh(const String &path);
		ALSound *loadSound(const String &path);
		GLTexture *loadTexture2d(const String &path);
		GLTexture *loadTexture2d(ILImage *image, const String &path);
		GLTexture *loadTextureCube(const String &path);

		GLShader *loadShader(const String &path, const String &defines);
		void reloadShaders();

		void deleteMaterial(Material *material);
		void deleteMesh(Mesh *mesh);
		void deleteSound(ALSound *sound);
		void deleteTexture(GLTexture *texture);
		void deleteShader(GLShader *shader);

	private:
		std::map<String, std::pair<Material*, int>> materials;
		std::map<String, std::pair<Mesh*, int>> meshs;
		std::map<String, std::pair<ALSound*, int>> sounds;
		std::map<String, std::pair<GLTexture*, int>> textures;
		std::map<String, std::pair<GLShader*, int>> shaders;
	};
}
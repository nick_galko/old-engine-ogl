/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//**************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/String.h"
#include <vector>
//**************************************

namespace Vega {

	//graphics parameters
#define CONF_SPECULAR "g_specular"
#define CONF_PARALLAX "g_parallax"
#define CONF_SHADOW_TYPE "g_shadowtype"
#define CONF_SHADOW_SIZE "g_shadowsize"
#define CONF_REFLECTIONS "g_reflections"
#define CONF_HDR "g_hdr"

	//window parameters
#define CONF_WIDTH "w_width"
#define CONF_HEIGHT "w_height"
#define CONF_ZDEPTH "w_zdepth"
#define CONF_BPP "w_bpp"
#define CONF_FULLSCREEN "w_fullscreen"

	/**
	Config class
	*/
	class ENGINE_API Config {
	public:

		/**
		Creates new Config
		\return pointer to the new Config
		*/
		Config(const String &path);

		/**
		Return pointer to the existing Config
		\return pointer to the existing Config
		*/
		static Config *Get();
		void Destroy();

		/**
		Saves all parameters to file
		\param path file name
		*/
		void save(const String &path);

		/**
		Gets parameter by name
		\param name  parameter name
		\return parameter value
		*/
		float getFloat(const String &name);

		/**
		Gets param by name
		\param name parameter name
		\return parameter value
		*/
		int getInt(const String &name);

		/**
		Gets param by name
		\param name parameter name
		\return parameter value
		*/
		bool getBool(const String &name);
		const String &getString(const String &name);

		/**
		Sets paramemter by name
		\param name parameter name
		\param value new value
		\return parameter value
		*/
		void setFloat(const String &name, float value);

		/**
		Sets paramemter by name
		\param name parameter name
		\param value new value
		\return parameter value
		*/
		void setInt(const String &name, int value);

		/**
		Sets paramemter by name
		\param name parameter name
		\param value new value
		\return parameter value
		*/
		void setBool(const String &name, bool value);

		/**
		Sets paramemter by name
		\param name parameter name
		\param value new value
		\return parameter value
		*/
		void setString(const String &name, const String &value);

	private:
		struct ConfigVar {
			ConfigVar() {
				name = "";
				value = "";
			}

			String name;
			String value;
		};
		std::vector<ConfigVar> vars;
	};

}




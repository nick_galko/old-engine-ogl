/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/
#pragma once

//***************************************************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/IncludesAndLibs.h"
//***************************************************************************
#include "../Inc/String.h"
#include "../Inc/ILImage.h"
//***************************************************************************

namespace Vega {

	/**
	Engine`s main image loading system. Created one time
	*/
	class ENGINE_API ILSystem {
	public:
		ILSystem();
		~ILSystem(){}
	};
}
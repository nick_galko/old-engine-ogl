/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once
 
//***************************************************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/String.h"
#include "../Inc/FStream.h"
#include "../Inc/GLSystem.h"
#include <vector>
//***************************************************************************

namespace Vega {

#define U_TIME "u_time"
#define U_LIGHT_RADIUS "u_light_radius"
#define U_LIGHT_IRADIUS "u_light_iradius"

#define U_LIGHT_COLOR "u_light_color"
#define U_LIGHT_POSITION "u_light_position"
#define U_LIGHT_DIRECTION "u_light_direction"
#define U_VIEW_POSITION "u_view_position"
#define U_VIEW_DIRECTION "u_view_direction"

#define U_MVP_TRANSFORM "u_mvp_transform"
#define U_WORLD_TRANSFORM "u_world_transform"
#define U_VIEWPORT_TRANSFORM "u_viewport_transform" 
#define U_SPOT_TRANSFORM "u_spot_transform"

#define U_SHADOW_MAP "u_shadow_map"
#define U_VIEWPORT_MAP "u_viewport_map"
#define U_REFLECTION_MAP "u_reflection_map"
#define U_SPOT_MAP "u_spot_map"

	/**
	Material class
	*/
	class ENGINE_API Material {
	public:
		enum PassType {
			AMBIENT,
			POINT,
			SPOT,
			DIRECT,
			CUSTOM_0,
			CUSTOM_1
		};

		/**
		Creates new material from file
		*/
		static Material *Create(const String &path);

		/**
		destroys material
		*/
		void Destroy();

		/**
		sets material pass
		\param pass  pass type
		\return true - if pass found
		*/
		bool setPass(PassType pass);

		/**
		unsets material current pass
		*/
		void unsetPass();

		/**
		send parameter
		\param type  parameter type
		\param value  param value
		*/
		void setMat4(const String &name, const Mat4 &value);

		/**
		send parameter
		\param type  parameter type
		\param value  param value
		*/
		void setVec4(const String &name, const Vec4 &value);

		/**
		send parameter
		\param type  parameter type
		\param value  param value
		*/
		void setVec3(const String &name, const Vec3 &value);

		/**
		send parameter
		\param type  parameter type
		\param value  param value
		*/
		void setVec2(const String &name, const Vec2 &value);

		/**
		send parameter
		\param type  parameter type
		\param value  param value
		*/
		void setFloat(const String &name, float value);

		/**
		send parameter
		\param type  parameter type
		\param value  param value
		*/
		void setTexture(const String &name, GLTexture *texture, int unit);

		/**
		send parameter
		\param type  parameter type
		\param value  param value
		*/
		void unsetTexture(GLTexture *texture, int unit);

		/**
		do material has blending
		*/
		bool hasBlending();

		/**
		set material blending
		*/
		void setBlending();

		/**
		set material alpha test
		*/
		void setAlphaTest();

		/**
		unset material blending
		*/
		void unsetBlending();

		/**
		set material alpha test
		*/
		void unsetAlphaTest();

		/**
		gets material path
		*/
		const String &getPath();

	private:
		String path;

		bool blending;
		GLSystem::BlendParam src, dst;

		bool alphaTest;
		GLSystem::CompareType alphaFunc;
		float alphaRef;

		GLShader *ambientShader;
		GLShader *pointShader;
		GLShader *spotShader;
		GLShader *directShader;
		GLShader *custom0;
		GLShader *custom1;

		GLShader *currentShader;

		GLTexture *texture0;
		GLTexture *texture1;
		GLTexture *texture2;
		GLTexture *texture3;

		Vec4 param0;
		Vec4 param1;

		friend class Scene;
	};
}
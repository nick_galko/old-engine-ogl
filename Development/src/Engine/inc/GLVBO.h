/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once 

//***************************************************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/IncludesAndLibs.h"
//***************************************************************************
#include "../Inc/MathLib.h"
//***************************************************************************

namespace Vega {

	/**
	Vertex buffer object class
	*/
	class ENGINE_API GLVBO {
	public:
		/**
		VBO data type
		*/
		enum DataType {
			FLOAT,
			DOUBLE,
			UNSIGNED_INT,
			UNSIGNED_SHORT
		};

	public:

		/**
		Creates vertex buffer object
		\param data pointer to data array
		\param numElements  umber of elements
		\param elemSize sizeof element
		\param dataType VBO data type
		\return pointer to new GLVBO
		*/
		static GLVBO *CreateIBO(void *data, int numElements, int elemSize, DataType dataType);

		/**
		Creates vertex buffer object
		\param data pointer to data array
		\param numElements number of elements
		\param elemSize sizeof element
		\param dataType VBO data type
		\return pointer to new GLVBO
		*/
		static GLVBO *CreateVBO(void *data, int numElements, int elemSize, DataType dataType);

		/**
		Destroys GLVBO
		*/
		void Destroy();


		/**
		Sets VBO as vertex data source
		\param numComp number of vertex components
		\param stride data stride
		\param offset data offset from the beginning
		*/
		void setVertexSource(int numComp, int stride, int offset);

		/**
		Sets VBO as normal data source
		\param stride data stride
		\param offset data offset from the beginning
		*/
		void setNormalSource(int stride, int offset);

		/**
		Sets VBO as texture coords data source
		\param numComp number of vertex components
		\param stride data stride
		\param offset data offset from the beginning
		\param tex_unit number of texture unit
		*/
		void setTexCoordSource(int tex_unit, int numComp, int stride, int offset);

		/**
		Sets VBO as index data source
		\param stride data stride
		\param offset data offset from the beginning
		*/
		void setIndexSource(int stride, int offset);

		/**
		Unsets vertex data source
		*/
		void unsetVertexSource();

		/**
		Unsets normal data source
		*/
		void unsetNormalSource();

		/**
		Unsets tex coord data source
		\param tex_unit texture unit number
		*/
		void unsetTexCoordSource(int tex_unit);

		/**
		Unsets index data source
		\param vbo pointer to VBO with data
		*/
		void unsetIndexSource();

		/**
		Sets VBO
		*/
		void set();

		/**
		Unsets VBO
		*/
		void unset();

	private:
		GLuint glID;
		int numElements;
		int elementSize;

		GLenum dataType;
		GLuint type;
		GLuint drawType;
	};
}
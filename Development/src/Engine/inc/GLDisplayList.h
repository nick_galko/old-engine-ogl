/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once 

//***************************************************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/IncludesAndLibs.h"
//***************************************************************************
#include "../Inc/MathLib.h"
//***************************************************************************

namespace Vega {

	/**
	Display list class
	*/
	class ENGINE_API GLDisplayList {
	public:

		/**
		Creates GLDisplayList
		\return pointer to new display list
		*/
		static GLDisplayList *Create();

		/**
		Destroys GLDisplayList
		*/
		void Destroy();


		/**
		Begins the display list build
		*/
		void beginBuild();

		/**
		Ends the display list build
		*/
		void endBuild();

		/**
		Calls the display list
		*/
		void call();

	private:
		GLuint glID;
	};
}
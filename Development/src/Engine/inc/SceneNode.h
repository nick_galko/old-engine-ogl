/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/MathLib.h"
#include "../Inc/String.h"
#include "../Inc/PhysBody.h"
#include "../Inc/Material.h"
#include "../Inc/Serializer.h"
//************************************

namespace Vega {

	class ENGINE_API SceneNode {
	public:
		/*
		Type
		*/
		enum SceneNodeType {
			SCENE_NODE,

			SCENE_NODE_OBJECT,
			SCENE_NODE_OBJECT_MESH,
			SCENE_NODE_OBJECT_SKINNED_MESH,
			SCENE_NODE_OBJECT_PARTICLE_SYSTEM,

			SCENE_NODE_LIGHT,
			SCENE_NODE_LIGHT_POINT,
			SCENE_NODE_LIGHT_SPOT,
			SCENE_NODE_LIGHT_DIRECT,

			SCENE_NODE_CAMERA,
			SCENE_NODE_CAMERA_FREE,
			SCENE_NODE_CAMERA_FPS,
			SCENE_NODE_CAMERA_DUMMY
		};

		virtual SceneNodeType getType() {
			return SCENE_NODE;
		}

		/*
		Serialize
		*/
		virtual void serialize(FStream *file);
		virtual void deserialize(FStream *file);

		/*
		Name
		*/
		virtual void setName(const String &name);
		virtual const String &getName();

		/*
		Parent/children
		*/
		virtual void attach(SceneNode *node, const Mat4 &offset);
		virtual void deattach(SceneNode *node);

		virtual void attachTo(SceneNode *node, const Mat4 &offset);
		virtual void deattachFrom(SceneNode *node);

		/*
		Transforms
		*/
		virtual void setTransform(const Mat4 &transform);
		virtual const Mat4 &getTransform();

		virtual Vec3 getPosition();
		virtual Vec3 getOrientation();

	protected:
		void initNode() {
			serializer = Serializer::Create();
			parent = NULL;
		}

		void deinitNode() {
			serializer->Destroy();
		}

		Serializer *serializer;

		Mat4 transform;
		Mat4 offset;

		SceneNode *parent;
		std::vector<SceneNode*> children;

		String name;
	};
}
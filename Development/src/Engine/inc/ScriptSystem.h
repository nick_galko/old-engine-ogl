/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//***************************************************
#include "../Inc/CompileConfig.h"
//***************************************************
struct lua_State;

namespace Vega {

	/**
	Engine`s main window and input system. Created one time
	*/
	class ENGINE_API ScriptSystem {
	public:
		ScriptSystem();
		~ScriptSystem();
		void Initialize();
		__inline lua_State *GetLuaState(){ return mLuaState; }
	private:
		void CoreMathBinding();
	private:
		lua_State *mLuaState;
	};
}
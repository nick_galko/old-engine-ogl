/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//**************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/SceneNode.h"
#include "../Inc/MathLib.h"
#include "../Inc/PhysSystem.h"
#include "../Inc/String.h"
//**************************************

namespace Vega {

	/**
	Camera base class
	*/
	class ENGINE_API Camera : public SceneNode {
	public:
		/*
		Virtual
		*/
		virtual SceneNodeType getType() { return SCENE_NODE_CAMERA; }

		/**
		gets camera fov
		\return fov
		*/
		virtual float getFOV();

		/**
		sets camera fov
		*/
		virtual void setFOV(float fov);

		/**
		gets camera screen aspect
		\return aspect
		*/
		virtual float getAspect();

		/**
		sets camera screen aspect
		*/
		virtual void setAspect(float aspect);

		/**
		gets camera zNear clip plane distance
		\return distance
		*/
		virtual float getZNear();

		/**
		sets camera zNear clip plane distance
		*/
		virtual void setZNear(float znear);

		/**
		gets camera zFar clip plane distance
		\return distance
		*/
		virtual float getZFar();

		/**
		sets camera zFar clip plane distance
		*/
		virtual void setZFar(float zfar);

		/**
		sets camera view matrix
		*/
		virtual void setView(const Mat4 &view);

		/**
		gets camera view matrix
		\return view matrix
		*/
		virtual const Mat4 &getView();

		/**
		sets camera projection matrix
		*/
		virtual void setProjection(const Mat4 &projection);

		/**
		gets camera projection matrix
		\return projection matrix
		*/
		virtual const Mat4 &getProjection();

		/**
		gets camera phys body
		\return phys body
		*/
		virtual PhysBody *getPhysBody();

		/**
		updates the camera
		*/
		virtual void update();

	protected:
		void buildProjection();

		float angle[2];
		float fov;
		float aspect;
		float zNear, zFar;

		Mat4 projection;
		Mat4 view;

		friend class Scene;
	};

	/**
	Class of the FPS camera
	*/
	class ENGINE_API CameraFPS : public Camera {
	public:
		virtual SceneNodeType getType() { return SCENE_NODE_CAMERA_FPS; }

		/**
		Creates new FPS camera
		*/
		static CameraFPS *Create(bool _activate);

		/**
		Camera destructor
		*/
		void Destroy();

		/**
		get camera max velocity
		\return max velocity
		*/
		float getMaxVelocity();

		/**
		set camera max velocity
		*/
		void setMaxVelocity(float maxVelocity);

		/**
		updates the camera
		*/
		virtual void update();

		/**
		set physics to camera
		\param size  bounding sphere size
		\param mass  object mass
		*/
		void setPhysics(const Vec3 &size, float mass);

		/**
		gets camera phys body
		\return phys body
		*/
		virtual PhysBody *getPhysBody();

	private:
		ALSound *jumpSnd;
		ALSoundSource *jumpSrc;

		Vec3 position;
		Vec3 direction;

		float maxVelocity;

		PhysBody *pBody;
		PhysJointUpVector *pJoint;
		Vec3 size;

		friend class Scene;
	};


	/**
	class of the FPS camera
	*/
	class ENGINE_API CameraFree : public Camera {
	public:
		virtual SceneNodeType getType() { return SCENE_NODE_CAMERA_FREE; }

		/**
		Creates new FPS camera
		*/
		static CameraFree *Create(bool _activate);

		/**
		Camera destructor
		*/
		void Destroy();

		/**
		get camera max velocity
		\return max velocity
		*/
		float getMaxVelocity();

		/**
		set camera max velocity
		*/
		void setMaxVelocity(float maxVelocity);

		/**
		updates the camera
		*/
		virtual void update();

		/**
		set physics to camera
		\param size  bounding sphere size
		\param mass  object mass
		*/
		void setPhysics(const Vec3 &size, float mass);

		/**
		gets camera phys body
		\return phys body
		*/
		virtual PhysBody *getPhysBody();

	private:
		Vec3 position;
		Vec3 direction;

		float maxVelocity;

		PhysBody *pBody;

		friend class Scene;
	};

	/**
	class of the dummy camera
	*/
	class ENGINE_API CameraDummy : public Camera {
	public:
		virtual SceneNodeType getType() { return SCENE_NODE_CAMERA_DUMMY; }

		/**
		Creates new FPS camera
		*/
		static CameraDummy *Create(bool _activate);

		/**
		Camera destructor
		*/
		void Destroy();

	private:
		friend class Scene;
	};

}
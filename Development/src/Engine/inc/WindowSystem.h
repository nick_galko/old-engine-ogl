/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//***************************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/IncludesAndLibs.h"
#include "../Inc/Config.h"
#include "../Inc/GLSystem.h" 
//***************************************************************************
#include "../Inc/String.h"
//***************************************************
#include "../../Platform/inc/glfw3.h"
#ifdef WIN32
#include <windows.h>
#endif
//***************************************************

namespace Vega {

	/**
	Engine`s main window and input system. Created one time
	*/
	class ENGINE_API WindowSystem {
	public:
		/**
		Mouse buttons enum
		*/
		enum MouseButton {
			MOUSE_LEFT_BUTTON = GLFW_MOUSE_BUTTON_LEFT,
			MOUSE_RIGHT_BUTTON = GLFW_MOUSE_BUTTON_RIGHT
		};

		/**
		Keyboard keys enum
		*/
		enum Key {
			KEY_ESC = GLFW_KEY_ESCAPE,
			KEY_SPACE = GLFW_KEY_SPACE,
			KEY_RETURN = GLFW_KEY_BACKSPACE,

			KEY_UP = GLFW_KEY_UP,
			KEY_DOWN = GLFW_KEY_DOWN,
			KEY_LEFT = GLFW_KEY_LEFT,
			KEY_RIGHT = GLFW_KEY_RIGHT,

			KEY_0 = GLFW_KEY_KP_0,
			KEY_1,
			KEY_2, KEY_3,
			KEY_4, KEY_5,
			KEY_6, KEY_7,
			KEY_8, KEY_9,

			KEY_A = 65,
			KEY_B,
			KEY_C, KEY_D,
			KEY_E, KEY_F,
			KEY_G, KEY_H,
			KEY_I, KEY_J,
			KEY_K, KEY_L,
			KEY_M, KEY_N,
			KEY_O, KEY_P,
			KEY_Q, KEY_R,
			KEY_S, KEY_T,
			KEY_U, KEY_V,
			KEY_W, KEY_X,
			KEY_Y, KEY_Z,
		};

	public:
		WindowSystem(int width, int height, int bpp, int zdepth, bool fullscreen);
		/**
		Creates new WindowSystem
		\param width screen width
		\param height screen height
		\param bpp screen bpp
		\param zDepth ZBuffer depth
		\param fullscr fullscreen flag
		\param windowTitle window title string
		*/
		void Initialize();

		/**
		Returns existing WindowSystem
		\return pointer to existing WindowSystem
		*/
		static WindowSystem *Get();

		/**
		Destroys WindowSystem
		*/
		void Destroy();

		/**
		Sets window title
		\param title title text
		*/
		void setTitle(const String &title);

		/**
		Updates app`s timer
		*/
		void updateTimer();

		/**
		Swaps app`s back and front buffers
		*/
		void swapBuffers();

		/**
		Updates WindowSystem and processes events
		*/
		void update();

		/**
		Gets screen width
		\return screen width
		*/
		int getWidth() { return width; }

		/**
		Gets screen height
		\return screen height
		*/
		int getHeight() { return height; }

		/**
		Gets frames delta time
		\return dTime
		*/
		int getDTime() { return dTime; }

		/**
		Gets elapsed time
		\return eTime
		*/
		int getETime() { return eTime; }

		/**
		Pauses timer
		\param pause pause flag
		*/
		void timerPause(bool pause) { tPause = pause; }

		/**
		Get time in mseconds
		\return time in mseconds
		*/
		int getTime();

		/**
		Checks if mouse was moved
		\return true if moved
		*/
		bool isMouseMoved() { return mousing; }

		/**
		Gets mouse X coordinate
		\return X coordinate
		*/
		int getMouseX() { return mouseX; }

		/**
		Gets mouse Y coordinate
		\return Y coordinate
		*/
		int getMouseY() { return mouseY; }

		/**
		Gets mouse delta X coordinate
		\return delta X coordinate
		*/
		int getMouseDX() { return mouseX - oldMouseX; }

		/**
		Gets mouse delta Y coordinate
		\return delta Y coordinate
		*/
		int getMouseDY() { return mouseY - oldMouseY; }

		/**
		Sets mouse position
		\param x mouse x
		\param y mouse y
		*/
		void setMousePos(int x, int y);

		/**
		Show/hide cursor
		\param show show cursor if true
		*/
		void showCursor(bool show);

		/**
		Toggle show/hide cursor
		*/
		void toggleShowCursor() { showCursor(!cursorVisible); }

		/**
		Checks if cursor is visible
		\return true if visible
		*/
		bool isCursorVisible() { return cursorVisible; }

		/**
		Grab/release cursor
		\param grab grab cursor if true
		*/
		void grabMouse(bool grab);

		/**
		Toggle grab/release cursor
		*/
		void toggleGrabMouse() { grabMouse(!mouseGrabed); }

		/**
		Checks if cursor is grabed
		\return true if grabed
		*/
		bool isMouseGrabed() { return mouseGrabed; }

		/**
		Checks if the mouse button is pressed
		\param mb mouse button id
		\return true if pressed
		*/
		bool isMouseButtonPressed(MouseButton mb);

		/**
		Checks if the mouse button was pressed in previous frame and now it is released
		\param mb mouse button id
		\return true if was pressed
		*/
		bool isMouseButtonDown(MouseButton mb);

		/**
		Checks if the mouse button was released in previous frame and now it is pressed
		\param mb mouse button id
		\return true if was pressed
		*/
		bool isMouseButtonUp(MouseButton mb);

		/**
		Checks if the key is pressed
		\param key key id
		\return true if pressed
		*/
		bool isKeyPressed(Key key);

		/**
		Checks if the key was pressed in previous frame and now it is released
		\param key key id
		\return true if was pressed
		*/
		bool isKeyDown(Key key);

		/**
		Checks if the key was released in previous frame and now it is pressed
		\param key key id
		\return true if was released
		*/
		bool isKeyUp(Key key);

	public:
		HDC hDC;
		HGLRC hRC;
		int hWnd;
		HINSTANCE hInstance;

		friend class GLPBuffer;

	public:
		int width, height, bpp, zdepth;
		bool fullscreen;

		int eTime, dTime;
		__int64 frequency;
		bool tPause;

		int mouseX, mouseY;
		int oldMouseX, oldMouseY;

		bool cursorVisible;
		bool mouseGrabed;

		int mx, my;
		bool mousing;

		bool mouseButtons[3];
		bool oldMouseButtons[3];

		bool keys[315];
		bool oldKeys[315];
	private:
		GLFWwindow* window;
	};
}
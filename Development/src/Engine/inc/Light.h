/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//**************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/SceneNode.h"
#include "../Inc/GLSystem.h"
#include "../Inc/MathLib.h"
#include "../Inc/Mesh.h"
//**************************************

namespace Vega {

	/**
	Base light class
	*/
	class ENGINE_API Light : public SceneNode {
	public:
		virtual SceneNodeType getType() { return SCENE_NODE_LIGHT; }


		/**
		enables light
		\param enabled  enable flag
		*/
		virtual void setEnabled(bool enabled);

		/**
		is light enabled
		\return true if enabled
		*/
		virtual bool getEnabled();

	protected:
		bool enabled;
		friend class Scene;
	};

	/**
	Point light class
	*/
	class ENGINE_API LightPoint : public Light {
	public:
		virtual SceneNodeType getType() { return SCENE_NODE_LIGHT_POINT; }

		/**
		Creates new point light
		*/
		static LightPoint *Create();

		/**
		destroys point light
		*/
		void Destroy();

		/**
		get light color
		\return light color
		*/
		const Vec3 &getColor();

		/**
		set light color
		*/
		void setColor(const Vec3 &color);

		/**
		get light radius
		\return light radius
		*/
		float getRadius();

		/**
		get light inversed radius
		\return light iradius
		*/
		float getIRadius();

		/**
		set light radius
		*/
		void setRadius(float radius);

		/**
		does the light cast shadows
		\return true if casts
		*/
		bool getShadows();

		/**
		set light shadow cast flag
		\param shadows  shadow cast flag
		*/
		void setShadows(bool shadows);

		/**
		is light visible
		\return true if visible
		*/
		bool isVisible();

		/**
		return light shadow map
		\return shadow map
		*/
		GLTexture *getShadowMap();

	private:
		Vec3 color;

		float radius;
		bool castShadows;

		GLTexture *shadowMap;

		friend class Scene;
	};

	/**
	Spot light class
	*/
	class ENGINE_API LightSpot : public Light {
	public:
		virtual SceneNodeType getType() { return SCENE_NODE_LIGHT_SPOT; }

		/**
		Creates new point light
		*/
		static LightSpot *Create();

		/**
		destroys point light
		*/
		void Destroy();

		/**
		get light color
		\return light color
		*/
		const Vec3 &getColor();

		/**
		set light color
		*/
		void setColor(const Vec3 &color);

		/**
		get light FOV
		\return fov
		*/
		float getFOV();

		/**
		set light fov
		\param fov  fov to set
		*/
		void setFOV(float fov);

		/**
		get light radius
		\return light radius
		*/
		float getRadius();

		/**
		get light inversed radius
		\return light iradius
		*/
		float getIRadius();

		/**
		set light radius
		*/
		void setRadius(float radius);

		/**
		does the light cast shadows
		\return true if casts
		*/
		bool getShadows();

		/**
		set light shadow cast flag
		\param shadows  shadow cast flag
		*/
		void setShadows(bool shadows);

		/**
		is light visible
		\return true if visible
		*/
		bool isVisible();

		/**
		return light shadow map
		\return shadow map
		*/
		GLTexture *getShadowMap();

		/**
		return light shadow map
		\return shadow map
		*/
		GLTexture *getSpotMap();

	private:
		Vec3 color;

		float radius, fov;
		bool castShadows;

		GLTexture *shadowMap;
		GLTexture *projMap;

		friend class Scene;
	};

	/**
	Direct light class
	*/
	class ENGINE_API LightDirect : public Light {
	public:
		virtual SceneNodeType getType() { return SCENE_NODE_LIGHT_DIRECT; }

		/**
		Creates new point light
		*/
		static LightDirect *Create();

		/**
		destroys point light
		*/
		void Destroy();

		/**
		get light color
		\return light color
		*/
		const Vec3 &getColor();

		/**
		set light color
		*/
		void setColor(const Vec3 &color);

		/**
		is light visible
		\return true if visible
		*/
		bool isVisible();

	private:
		Vec3 color;

		friend class Scene;
	};

}
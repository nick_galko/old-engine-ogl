/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/
#pragma once
 
#ifdef WIN32
#include <windows.h>
#endif

//***************************************************************************
#include "../Inc/XSIncludes/AL/al.h"
#include "../Inc/XSIncludes/AL/alc.h"
//***************************************************************************
#include "../../../Externals/libvorbis/include/vorbis/codec.h"
#include "../../../Externals/libvorbis/include/vorbis/vorbisfile.h"
//***************************************************************************
#include "../Inc/XSIncludes/IL/il.h"
#include "../Inc/XSIncludes/IL/ilu.h"
//***************************************************************************
#include "../Inc/XSIncludes/GL/gl.h"
#include "../Inc/XSIncludes/GL/glext.h"
#include "../Inc/XSIncludes/GL/wglext.h"
//***************************************************************************
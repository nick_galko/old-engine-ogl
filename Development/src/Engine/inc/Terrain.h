/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/GLSystem.h"
#include "../Inc/ILSystem.h"
#include "../Inc/PhysSystem.h"
#include "../Inc/Material.h"
//************************************

namespace Vega {

	class ENGINE_API Terrain {
	public:
		static Terrain *Create(const String &path, float step, float height, int nodeSize);
		void Destroy();

		void drawNode(int n, const Vec3 &cameraPos);
		int getNumNodes() { return numNodes; }

		void setLods(const Vec4 &lods) { this->lods = lods; }

		const Vec3 &getMin(int n);
		const Vec3 &getMax(int n);
		const Vec3 &getCenter(int n);
		float getRadius(int n);

		void setPhysics();

		void computeTBN();

		void setMaterial(const String &path) { this->material = Material::Create(path); }
		Material *getMaterial() { return material; }

	private:
		Vec4 lods;

		struct Vertex {
			Vec3 position;
			Vec2 texcoord;
			Vec3 normal;
		};

		struct Node {
			Vec3 min, max, center;
			float radius;

			unsigned int *indices[4];
			unsigned int numIndices[4];

			bool visible;
		};

		int numNodes;
		Node *nodes;

		Material *material;

		GLVBO *vertBuff;
		Vertex *vertices;
		unsigned int numVertices;

		PhysBody *pBody;

		friend class Scene;
	};

}
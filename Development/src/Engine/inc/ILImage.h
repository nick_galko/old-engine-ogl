/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/
#pragma once

//***************************************************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/IncludesAndLibs.h"
//***************************************************************************
#include "../Inc/String.h"
//***************************************************************************

namespace Vega {

	/**
	Image class
	*/
	class ENGINE_API ILImage {
	public:
		/**
		Image format
		*/
		enum Format {
			RGB = IL_RGB,
			RGBA = IL_RGBA
		};

	public:
		/**
		Loads ILImage from file
		\param path image file path
		\return pointer to new ILImage
		*/
		static ILImage *Create2d(const String &path);

		/**
		Creates empty ILImage
		\param width image size
		\param height image size
		\param depth image size
		\param format image format(RGB/RGBA)
		\return pointer to new ILImage
		*/
		static ILImage *CreateEmpty2d(int width, int height, int format);

		/**
		Creates noise ILImage
		\param width image size
		\param height image size
		\param depth image size
		\param format image format(RGB/RGBA)
		\return pointer to new ILImage
		*/
		static ILImage *CreateNoise2d(int width, int height, int format);

		/**
		Creates empty ILImage
		\param width image size
		\param height image size
		\param depth image size
		\param format image format(RGB/RGBA)
		\return pointer to new ILImage
		*/
		static ILImage *CreateEmpty3d(int width, int height, int depth, int format);

		/**
		Creates noise ILImage
		\param width image size
		\param height image size
		\param depth image size
		\param format image format(RGB/RGBA)
		\return pointer to new ILImage
		*/
		static ILImage *CreateNoise3d(int width, int height, int depth, int format);

		/**
		Destroys ILImage
		*/
		void Destroy();

		/**
		Converts ILImage to normal map
		\param k height rescale coef
		*/
		void toNormalMap(int k);

		/**
		Converts ILImage to grey scale
		*/
		void toGreyScale();

		/**
		Gets image data
		\return image data
		*/
		unsigned char *getData() { return data; }

		/**
		Gets image width
		\return image width
		*/
		int getWidth() { return width; }

		/**
		Gets image height
		\return image height
		*/
		int getHeight() { return height; }

		/**
		Gets image depth
		\return image depth
		*/
		int getDepth() { return depth; }

		/**
		Gets image bytes per pixel
		\return image bytes per pixel
		*/
		int getBPP() { return bpp; }

		/**
		Gets image format
		\return image format
		*/
		int getFormat() { return format; }

		/**
		Checks if image is a 3D image
		\return true if 3D
		*/
		bool is3D() { return depth > 1; }

	private:
		int width, height, depth;
		int bpp, format;

		ILubyte *data;

		int getSize() { return width * height * depth * bpp; }
	};
}
/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/
#pragma once

//**************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/String.h"
//**************************************

namespace Vega {

	/**
	File functionality
	*/
	class ENGINE_API FStream {
	public:
		/**
		Open mode
		*/
		enum Mode {
			READ_TEXT,
			WRITE_TEXT,
			APPEND_TEXT
		};

		/**
		Creates new FStream
		\param path file path
		\param mode open mode
		\return pointer to the new FStream
		*/
		static FStream *Create(const String &path, Mode mode);

		/**
		Destroys FStream
		*/
		void Destroy();

		/**
		Close file
		*/
		void close();

		/**
		feof analog
		*/
		bool eof();

		/**
		getc analog
		*/
		char getc();

		/**
		gets analog
		\return String from file
		*/
		String gets();

		/**
		printf analog
		\param format format string
		*/
		void printf(const char *format, ...);

	private:
		FILE *file;
	};
}
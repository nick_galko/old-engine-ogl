/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once 

//***************************************************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/IncludesAndLibs.h"
//***************************************************************************
#include "../Inc/MathLib.h"
//***************************************************************************

namespace Vega {

	/**
	Occlusion query class
	*/
	class ENGINE_API GLOcclusionQuery {
	public:

		/**
		Creates GLOcclusionQuery
		\return pointer to new GLOcclusionQuery
		*/
		static GLOcclusionQuery *Create();

		/**
		Destroys GLOcclusionQuery
		*/
		void Destroy();

		/**
		Begins rendering to query
		*/
		void beginRendering();

		/**
		End rendering to query
		*/
		void endRendering();

		/**
		Get number of passed samples
		\return number of passed samples
		*/
		unsigned int getResult();

	private:
		GLuint glID;
	};
}

/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once 

//***************************************************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/IncludesAndLibs.h"
//***************************************************************************
#include "../Inc/MathLib.h"
//***************************************************************************

namespace Vega {

	/**
	PBuffer class
	*/
	class ENGINE_API GLPBuffer {
	public:
		/**
		Creates GLPBuffer
		\param width  fbo width
		\param height  fbo height
		\return pointer to new GLPBuffer
		*/
		static GLPBuffer *Create(int width, int height);

		/**
		Destroy GLPBuffer
		*/
		void Destroy();

		/**
		Sets GLPBuffer
		*/
		void set();

		/**
		Unsets GLPBuffer
		*/
		void unset();

		/**
		Clears the GLPBuffer
		*/
		void clear();

	private:
		HPBUFFERARB hPBuffer;
		HDC hDC;
		HGLRC hRC;

		int width, height;
	};
}
/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//**************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/GLSystem.h"
#include "../Inc/MathLib.h"
#include <vector>
//**************************************

namespace Vega {

	/**
	Class of the particle system
	*/
	class ENGINE_API ParticleSystem {
	public:

		/**
		Creates new ParticleSystem
		*/
		static ParticleSystem *Create(const String &path, int numParticles);

		/**
		ParticleSystem destructor
		*/
		void Destroy();


		/**
		draw ParticleSystem
		*/
		void draw();

		/**
		update ParticleSystem
		*/
		void update();

		const Vec3 &getColor() { return color; }
		void setColor(const Vec3 &color) { this->color = color; }

		void setParticleLifeTime(int time) { lifeTime = time; }
		int getParticleLifeTime() { return lifeTime; }

		void setVelocity(const Vec3 &velocity) { this->velocity = velocity; }
		const Vec3 &getVelocity() { return velocity; }

		void setForce(const Vec3 &force) { this->force = force; }
		const Vec3 &getForce() { return force; }

		void setDispersion(float dispersion) { this->dispersion = dispersion; }
		float getDispersion() { return dispersion; }

		void setSize(float size) { this->size = size; }
		float getSize() { return size; }

	private:
		Vec3 min, max;
		Vec3 center;
		float radius;

		int lifeTime;
		float dispersion;

		GLTexture *texture;

		Vec3 color;
		Vec3 force, velocity;
		float size;

		int numParticles;
		struct Particle {
			Vec3 position;
			Vec3 velocity;
			int lifeTime;
		};
		Particle *particles;


		struct Vertex {
			Vec3 position;
			Vec2 texcoord;
		};
		Vertex *vertices;

		friend class Scene;
		friend class ObjectParticleSystem;
	};
}

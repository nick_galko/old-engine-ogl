/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//**************************************
#include "../Include/Light.h"
#include "../Include/ParticleSystem.h"
#include "../Include/Object.h"
#include "../Include/Terrain.h"
#include "../Include/Camera.h"
#include "../Include/GLSystem.h"
#include "../Include/Water.h"
#include "../Include/Skydome.h"
#include <vector>
//**************************************

namespace XSystemEngine {

/**
Class of the scene render. 
*/
class XSYSTEMENGINEAPI SceneRender {
public:

	friend class Light;
	friend class LightPoint;
	friend class LightSpot;
	friend class LightDirect;

	friend class Object;
	friend class ObjectMesh;
	friend class ObjectSkinnedMesh;
	friend class ObjectParticleSystem;

	friend class Water;
	friend class Terrain;
	friend class Skydome;

	friend class Material;
};

};



/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//**************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/String.h"
#include "../Inc/MathLib.h"
#include "../Inc/GLSystem.h"
#include "../Inc/MathLib.h"
#include "../Inc/PhysSystem.h"
#include "../Inc/Frustum.h"
#include "../Inc/Material.h"
#include "../../Common/inc/LogFuncts.h"
//**************************************

namespace Vega {

	/**
	Skinned mesh class
	*/
	class ENGINE_API SkinnedMesh {
	public:

		/**
		Creates new SkinnedSkinnedMesh from file
		\param path  object file path
		*/
		static SkinnedMesh *Create(const String &path);

		/**
		destroys object
		*/
		void Destroy();


		/**
		saves mesh to file
		\param path  file path
		*/
		void save(const String &path);

		/**
		draws object subset
		\param s  surface number
		*/
		void drawSubset(int s);
		int getNumSubsets() { return (int)numSubsets; }

		/**
		get subset number by number
		\param s  subset name
		\return subset number
		*/
		int getSubset(String name);

		Vec3 &getMax() { return max; }
		Vec3 &getMin() { return min; }
		Vec3 &getCenter() { return center; }
		float getRadius() { return radius; }

		Vec3 &getMax(int s) { return subsets[s]->max; }
		Vec3 &getMin(int s) { return subsets[s]->min; }
		Vec3 &getCenter(int s) { return subsets[s]->center; }
		float getRadius(int s) { return subsets[s]->radius; }

		void setFrame(float frame, int from = -1, int to = -1);

	private:

		void loadXSSMSH(const String &path);

		struct Weight {
			int bone;
			float weight;
			Vec3 position;
			Vec3 normal;
			Vec3 tangent;
			Vec3 binormal;
		};

		struct Vertex {
			Vec3 position;
			Vec2 texcoord;
			Vec3 normal;
			Vec3 tangent;
			Vec3 binormal;
			int numWeights;
			Weight *weights;
		};

		struct Bone {
			String name;
			Mat4 transform;
			Mat4 rotation;
			int parent;
		};

		struct Frame {
			Vec3 position;
			Quat rotation;
		};

		int numBones;
		Bone *bones;

		int numFrames;
		Frame **frames;

		struct Subset {
			String name;

			Subset() {
				numVertices = 0;
				numIndices = 0;
			}

			unsigned int numVertices;
			unsigned int numIndices;

			Vertex *vertices;
			unsigned int *indices;

			Vec3 min, max;
			Vec3 center;
			float radius;

			bool visible;
		};

		int numSubsets;
		Subset **subsets;

		Vec3 min, max;
		Vec3 center;
		float radius;


		void calculateBoundings();
		void calculateTBN();

		bool visible;

		friend class Scene;
		friend class ObjectSkinnedMesh;
	};
}
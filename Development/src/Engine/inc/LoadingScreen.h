/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//***************************************************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/String.h"
#include "../Inc/GLSystem.h"
//***************************************************************************

namespace Vega {

	/**
	Loading screen class ENGINE_API
	*/
	class ENGINE_API LoadingScreen {
	public:

		/**
		Creates the loading screen
		\param path image to show
		*/
		static LoadingScreen *Create(const String &path);

		/**
		Destroys loading screen
		*/
		void Destroy();

		/**
		Shows the loading screen
		*/
		void show();

	private:
		GLTexture *background;
	};
}
/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//**************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/SceneNode.h"
#include "../Inc/String.h"
#include "../Inc/MathLib.h"
#include "../Inc/GLSystem.h"
#include "../Inc/PhysSystem.h"
#include "../Inc/Frustum.h"
#include "../Inc/Material.h"
#include "../../Common/inc/LogFuncts.h"
#include "../Inc/Mesh.h"
#include "../Inc/SkinnedMesh.h"
#include "../Inc/ParticleSystem.h"
//**************************************

namespace Vega {

	/**
	Base object class
	*/
	class ENGINE_API Object : public SceneNode {
	public:
		/*
		Virtual
		*/
		virtual SceneNodeType getType() { return SCENE_NODE_OBJECT; }

		/*
		Pure virtual
		*/

		/**
		draws object subset
		\param s  surface number
		*/
		virtual void drawSubset(int s) = 0;

		/**
		get number of object subsets
		\return subsets number
		*/
		virtual int getNumSubsets() = 0;

		/**
		get max of AABB of the whole object
		\return max
		*/
		virtual const Vec3 &getMax() = 0;

		/**
		get min of AABB of the whole object
		\return min
		*/
		virtual const Vec3 &getMin() = 0;

		/**
		get center of boundong sphere of the whole object
		\return center
		*/
		virtual const Vec3 &getCenter() = 0;

		/**
		get radius of boundong sphere of the whole object
		\return radius
		*/
		virtual float getRadius() = 0;

		/**
		get max of AABB of the object subset
		\param s  subset number
		\return max
		*/
		virtual const Vec3 &getMax(int s) = 0;

		/**
		get min of AABB of the object subset
		\param s  subset number
		\return min
		*/
		virtual const Vec3 &getMin(int s) = 0;

		/**
		get center of the object subset
		\param s  subset number
		\return center
		*/
		virtual const Vec3 &getCenter(int s) = 0;

		/**
		get radius of the object subset
		\param s  subset number
		\return radius
		*/
		virtual float getRadius(int s) = 0;

		/**
		get material
		\param s  subset number
		*/
		virtual Material *getMaterial(int s);

		/**
		set material to the subset
		\param name  subset name
		\param path  material file path
		*/
		virtual void setMaterial(const String &name, const String &path) {}

		/**
		set materials to the subsets
		\param path  material list file path
		*/
		virtual void setMaterialList(const String &path) {}

		/**
		update the transformation matrix
		*/
		virtual void update() {}

		/**
		get the object phys body
		\return phys body
		*/
		virtual PhysBody *getPhysBody();
	};

	/**
	ActorMesh class
	*/
	class ENGINE_API ActorMesh : public Object {
	public:
		/**
		Creates new ActorMesh from file
		\param path  object file path
		*/
		ActorMesh(const String &path);
		virtual SceneNodeType getType() { return SCENE_NODE_OBJECT_MESH; }
		/**
		Creates new ActorMesh from file
		\param path  object file path
		*/
		void create(const String &path);

		/**
		destroys object
		*/
		void Destroy();

		/**
		draws object subset
		\param s  surface number
		*/
		virtual void drawSubset(int s);

		/**
		get number of object subsets
		\return subsets number
		*/
		virtual int getNumSubsets();

		/**
		get max of AABB of the whole object
		\return max
		*/
		virtual const Vec3 &getMax();

		/**
		get min of AABB of the whole object
		\return min
		*/
		virtual const Vec3 &getMin();

		/**
		get center of boundong sphere of the whole object
		\return center
		*/
		virtual const Vec3 &getCenter();

		/**
		get radius of boundong sphere of the whole object
		\return radius
		*/
		virtual float getRadius();

		/**
		get max of AABB of the object subset
		\param s  subset number
		\return max
		*/
		virtual const Vec3 &getMax(int s);

		/**
		get min of AABB of the object subset
		\param s  subset number
		\return min
		*/
		virtual const Vec3 &getMin(int s);

		/**
		get center of the object subset
		\param s  subset number
		\return center
		*/
		virtual const Vec3 &getCenter(int s);

		/**
		get radius of the object subset
		\param s  subset number
		\return radius
		*/
		virtual float getRadius(int s);

		/**
		get subset material by number
		\param s subset number
		\return subset`s material
		*/
		virtual Material *getMaterial(int s);

		/**
		set material to the subset
		\param name  subset name
		\param path  material file path
		*/
		virtual void setMaterial(const String &name, const String &path);

		/**
		set materials to the subsets
		\param path  material list file path
		*/
		virtual void setMaterialList(const String &path);

		/**
		sets object transform
		\param trans  transform matrix
		*/
		virtual void setTransform(const Mat4 &transform);

		/**
		update the transformation matrix
		*/
		virtual void update();


		/**
		get the object phys body
		\return phys body
		*/
		virtual PhysBody *getPhysBody();

		/*
		Not virtual
		*/
		void setPhysicsBox(const Vec3 &size, float mass = 0);
		void setPhysicsSphere(const Vec3 &size, float mass = 0);

		void setPhysicsCylinder(float radius, float height, float mass = 0);
		void setPhysicsCone(float radius, float height, float mass = 0);
		void setPhysicsCapsule(float radius, float height, float mass = 0);
		void setPhysicsChampferCylinder(float radius, float height, float mass = 0);

		void setPhysicsConvexHull(float mass = 0);

		/**
		sets static object collider
		*/
		void setPhysicsStaticMesh();

		void setImpactSound(const String &path);

	private:
		Mesh *mesh;

		PhysBody *pBody;
		ALSound *impactSound;
		Material **materials;

		friend class Scene;
	};

	/**
	class of the scene object
	*/
	class ENGINE_API ObjectSkinnedMesh : public Object {
	public:
		virtual SceneNodeType getType() { return SCENE_NODE_OBJECT_SKINNED_MESH; }

		/**
		Creates new ObjectSkinnedMesh from file
		\param path  object file path
		*/
		static ObjectSkinnedMesh *Create(const String &path);

		/**
		destroys object
		*/
		void Destroy();

		/**
		draws object subset
		\param s  surface number
		*/
		virtual void drawSubset(int s);

		/**
		get number of object subsets
		\return subsets number
		*/
		virtual int getNumSubsets();

		/**
		get max of AABB of the whole object
		\return max
		*/
		virtual const Vec3 &getMax();

		/**
		get min of AABB of the whole object
		\return min
		*/
		virtual const Vec3 &getMin();

		/**
		get center of boundong sphere of the whole object
		\return center
		*/
		virtual const Vec3 &getCenter();

		/**
		get radius of boundong sphere of the whole object
		\return radius
		*/
		virtual float getRadius();

		/**
		get max of AABB of the object subset
		\param s  subset number
		\return max
		*/
		virtual const Vec3 &getMax(int s);

		/**
		get min of AABB of the object subset
		\param s  subset number
		\return min
		*/
		virtual const Vec3 &getMin(int s);

		/**
		get center of the object subset
		\param s  subset number
		\return center
		*/
		virtual const Vec3 &getCenter(int s);

		/**
		get radius of the object subset
		\param s  subset number
		\return radius
		*/
		virtual float getRadius(int s);

		/**
		get subset material by number
		\param s  subset number
		\return subset`s material
		*/
		virtual Material *getMaterial(int s);

		/**
		set material to the subset
		\param name  subset name
		\param path  material file path
		*/
		virtual void setMaterial(const String &name, const String &path);

		/**
		set materials to the subsets
		\param path  material list file path
		*/
		virtual void setMaterialList(const String &path);

		/**
		set the animation frame
		\param frame  frame number
		*/
		void setAnimationFrame(int frame, int from = -1, int to = -1);

	private:
		SkinnedMesh *mesh;
		Material **materials;

		friend class Scene;
	};

	/**
	Particle system class
	*/
	class ENGINE_API ObjectParticleSystem : public Object {
	public:
		virtual SceneNodeType getType() { return SCENE_NODE_OBJECT_PARTICLE_SYSTEM; }


		/**
		ObjectParticleSystem constructor
		*/
		static ObjectParticleSystem *Create(const String &path, int numParticles);

		/**
		destroys object
		*/
		void Destroy();


		/**
		draws object subset
		\param s surface number
		*/
		virtual void drawSubset(int s);

		/**
		get number of object subsets
		\return subsets number
		*/
		virtual int getNumSubsets();

		/**
		get max of AABB of the whole object
		\return max
		*/
		virtual const Vec3 &getMax();

		/**
		get min of AABB of the whole object
		\return min
		*/
		virtual const Vec3 &getMin();

		/**
		get center of boundong sphere of the whole object
		\return center
		*/
		virtual const Vec3 &getCenter();

		/**
		get radius of boundong sphere of the whole object
		\return radius
		*/
		virtual float getRadius();

		/**
		get max of AABB of the object subset
		\param s  subset number
		\return max
		*/
		virtual const Vec3 &getMax(int s);

		/**
		get min of AABB of the object subset
		\param s subset number
		\return min
		*/
		virtual const Vec3 &getMin(int s);

		/**
		get center of the object subset
		\param s subset number
		\return center
		*/
		virtual const Vec3 &getCenter(int s);

		/**
		get radius of the object subset
		\param s  subset number
		\return radius
		*/
		virtual float getRadius(int s);

		/**
		get subset material by number
		\param s  subset number
		\return subset`s material
		*/
		virtual Material *getMaterial(int s);

		/**
		updates the particle system
		*/
		virtual void update();

		/*
		Not virtual
		*/
		const Vec3 &getColor();
		void setColor(const Vec3 &color);

		void setParticleLifeTime(int time);
		int getParticleLifeTime();

		void setVelocity(const Vec3 &velocity);
		const Vec3 &getVelocity();

		void setForce(const Vec3 &force);
		const Vec3 &getForce();

		void setDispersion(float dispersion);
		float getDispersion();

		void setSize(float size);
		float getSize();

	private:
		ParticleSystem *system;

		friend class Scene;
	};

}
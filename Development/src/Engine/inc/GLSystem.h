/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once 

//***************************************************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/IncludesAndLibs.h"
//***************************************************************************
#include "../Inc/MathLib.h"
#include "../Inc/GLExtensions.h"
#include "../Inc/GLTexture.h"
#include "../Inc/GLVBO.h"
#include "../Inc/GLShader.h"
#include "../Inc/GLDisplayList.h"
#include "../Inc/GLFBO.h"
#include "../Inc/GLPBuffer.h"
#include "../Inc/GLOcclusionQuery.h"
#include <vector>
//***************************************************************************

namespace Vega {

	/**
	Engine`s main video system. Created one time
	*/
	class ENGINE_API GLSystem {
	public:
		/**
		Blending type enum
		*/
		enum BlendParam {
			ONE = GL_ONE,
			ZERO = GL_ZERO,
			SRC_COLOR = GL_SRC_COLOR,
			DST_COLOR = GL_DST_COLOR,
			SRC_ALPHA = GL_SRC_ALPHA,
			DST_ALPHA = GL_DST_ALPHA,
			ONE_MINUS_SRC_COLOR = GL_ONE_MINUS_SRC_COLOR,
			ONE_MINUS_DST_COLOR = GL_ONE_MINUS_DST_COLOR,
			ONE_MINUS_SRC_ALPHA = GL_ONE_MINUS_SRC_ALPHA,
			ONE_MINUS_DST_ALPHA = GL_ONE_MINUS_DST_ALPHA
		};

		/**
		Depth function enum
		*/
		enum CompareType {
			NEVER = GL_NEVER,
			LESS = GL_LESS,
			EQUAL = GL_EQUAL,
			LEQUAL = GL_LEQUAL,
			GREATER = GL_GREATER,
			NOTEQUAL = GL_NOTEQUAL,
			GEQUAL = GL_GEQUAL,
			ALWAYS = GL_ALWAYS
		};

		/**
		Cull type enum
		*/
		enum CullType {
			CCW = GL_CCW,
			CW = GL_CW
		};

		/**
		Cull face
		*/
		enum CullFace {
			FRONT_FACE = GL_FRONT,
			BACK_FACE = GL_BACK
		};

		/**
		Buffers enum
		*/
		enum Buffer {
			COLOR_BUFFER = GL_COLOR_BUFFER_BIT,
			DEPTH_BUFFER = GL_DEPTH_BUFFER_BIT,
			STENCIL_BUFFER = GL_STENCIL_BUFFER_BIT
		};

	public:

		/**
		Creates new GLSystem
		\return pointer to new GLSystem
		*/
		GLSystem();

		/**
		Creates new GLSystem
		\return pointer to new GLSystem
		*/
		void Initialize();
		/**
		Returns existing GLSystem
		\return pointer to existing GLSystem
		*/
		static GLSystem *Get();

		/**
		Destroy GLSystem
		*/
		~GLSystem();

		/**
		Gets video card vendor
		\return vendor name
		*/
		String getVendor();

		/**
		Gets video card renderer
		\return renderer name
		*/
		String getRenderer();

		/**
		Gets OpenGL version
		\return version number
		*/
		String getVersion();

		/**
		Gets OpenGL extensions
		\return extensions string
		*/
		String getExtensions();


		/**
		Gets number of video card texture units
		\return tex units number
		*/
		int getNumTexUnits();

		/**
		Gets video card max level of anisotropy
		\return max aniso
		*/
		int getMaxAniso();

		/**
		Checks if occlusion query is supported
		\return true if supported
		*/
		bool occlusionQuerySupported();

		/**
		Checks if GLSL is supported
		\return true if supported
		*/
		bool glslSupported();

		/**
		Checks if VBO is supported
		\return true if supported
		*/
		bool vboSupported();

		/**
		Checks if FBO is supported
		\return true if supported
		*/
		bool fboSupported();

		/**
		Checks if PBuffer is supported
		\return true if supported
		*/
		bool pBufferSupported();

		GLTexture::Filter defFilter;
		GLTexture::Aniso defAniso;

		/**
		Reshapes the screen
		\param width new screen width
		\param height new screen height
		*/
		void reshape(int width, int height);

		/**
		Gets the viewport coords
		\param viewport pointer to array of at least 4 ints
		*/
		void getViewport(int *viewport);

		/**
		Clears color of the screen
		\param color new screen color
		*/
		void clearColor(const Vec3 &color);

		/**
		Sets color buffer mask
		\param r red color masks
		\param g green color masks
		\param b blue color masks
		\param a alpha color masks
		*/
		void colorMask(bool r, bool g, bool b, bool a);

		/**
		Clears listed buffers
		\param buffers list of buffers to clear
		*/
		void clear(GLbitfield buffers);

		/**
		Flushes the OpenGL pipeline
		*/
		void flush();

		/**
		Sets the viewport size
		\param x new viewport width
		\param y new viewport height
		*/
		void viewport(int x, int y);

		/**
		Sets the current state color
		\param color new color
		*/
		void setColor(const Vec3 &color);

		/**
		Sets the current state color
		\param color new color
		*/
		void setColor(const Vec4 &color);

		/**
		Enables 2D mode
		\param normalized if true the ortho matrix will be normalized
		*/
		void enable2d(bool normalized);

		/**
		Enables 3D mode
		*/
		void enable3d();

		/**
		Draws 2D rectangle
		\param x0 min x
		\param y0 min y
		\param x3 max x
		\param y3 max y
		\param tx0 min tex coord x
		\param ty0 min tex coord y
		\param tx3 max tex coord x
		\param ty3 max tex coord y
		*/
		void drawRect(float x0, float y0, float x3, float y3, float tx0, float ty0, float tx3, float ty3);

		/**
		Sets the blending fuction
		\param src src color facrot
		\param dst dst color factor
		*/
		void blendFunc(BlendParam src, BlendParam dst);

		/**
		Enables blending
		*/
		void enableBlending();

		/**
		Enables blending
		\param src src color facrot
		\param dst dst color factor
		*/
		void enableBlending(BlendParam src, BlendParam dst);

		/**
		Disables blending
		*/
		void disableBlending();

		/**
		Sets the current state alpha test function
		\param type alpha test function
		*/
		void alphaTestFunc(CompareType type, float alphaRef);

		/**
		Enables the alpha test
		*/
		void enableAlphaTest();

		/**
		Enables the alpha test
		\param type alpha test function
		*/
		void enableAlphaTest(CompareType type, float alphaRef);

		/**
		Disables the alpha test
		*/
		void disableAlphaTest();

		/**
		Sets the current state depth test function
		\param type depth test function
		*/
		void depthFunc(CompareType type);

		/**
		Enables the depth test
		\param type depth test function
		*/
		void enableDepth(CompareType type);

		/**
		Enables the depth test
		*/
		void enableDepth();

		/**
		Disables the depth test
		*/
		void disableDepth();

		/**
		Sets the depth buffer mask
		\param mask if false the depth buffer will be locked
		*/
		void depthMask(bool mask);

		/**
		Sets the scissor rectangle
		\param x x coord
		\param y y coord
		\param z z coord
		\param w w coord
		*/
		void scissorRect(int x, int y, int z, int w);

		/**
		Enables scissors
		\param x x coord
		\param y y coord
		\param z z coord
		\param w w coord
		*/
		void enableScissor(int x, int y, int z, int w);

		/**
		Enables scissors
		*/
		void enableScissor();

		/**
		Disables scissors
		*/
		void disableScissor();

		/**
		Sets the polygon offset function
		\param a offset factor
		\param b offset units
		*/
		void polygonOffsetFill(float a, float b);

		/**
		Enables the polygon offset fill
		\param a offset factor
		\param b offset units
		*/
		void enablePolygonOffsetFill(float a, float b);

		/**
		Enables the polygon offset fill
		*/
		void enablePolygonOffsetFill();

		/**
		Disables the polygon offset fill
		\param a offset factor
		\param b offset units
		*/
		void disablePolygonOffsetFill();

		/**
		Sets the current state cull function
		\param type cull type
		*/
		void cullFunc(CullType type);

		/**
		Sets the current state cull face
		\param face face to leave
		*/
		void cullFace(CullFace face);

		/**
		Enables back face culling
		\param type cull type
		*/
		void enableCulling(CullType type);

		/**
		Enables back face culling
		\param face face to leave
		*/
		void enableCulling(CullFace face);

		/**
		Enables back face culling
		*/
		void enableCulling();

		/**
		Disables back face culling
		*/
		void disableCulling();

		/**
		Sets clip plain
		\param plain plain equation
		\param plainNum plain number (max - 6)
		*/
		void clipPlane(const Vec4 &plain, int plainNum);

		/**
		Enables clip plain
		\param plainNum plain number (max - 6)
		*/
		void enableClipPlane(int plainNum);

		/**
		Enables and sets clip plain
		\param plain plain equation
		\param plainNum plain number (max - 6)
		*/
		void enableClipPlane(const Vec4 &plain, int plainNum);

		/**
		Disables clip plain
		\param plainNum plain number (max - 6)
		*/
		void disableClipPlane(int plainNum);

		/**
		Sets the current matrix mode to projection
		*/
		void setMatrixMode_Projection();

		/**
		Sets the current matrix mode to modelview
		*/
		void setMatrixMode_Modelview();


		/**
		Pops the state matrix stack
		*/
		void pop();

		/**
		Pushes the state matrix stack
		*/
		void push();

		/**
		Replases the current state matrix with identity matrix
		*/
		void identity();

		/**
		Gets the current state MVP matrix
		*/
		Mat4 getMatrix_MVP();

		/**
		Gets the current state projection matrix
		*/
		Mat4 getMatrix_Projection();

		/**
		Gets the current state modelview matrix
		*/
		Mat4 getMatrix_Modelview();


		/**
		Mults the current state matrix
		\param matrix matrix to mult with
		*/
		void multMatrix(const Mat4 &matrix);

		/**
		Loads the current state matrix
		\param matrix matrix to load
		*/
		void loadMatrix(const Mat4 &matrix);


		/**
		Mults the current state matrix with translation matrix
		\param pos position to translate
		*/
		void translate(const Vec3 &pos);

		/**
		Mults the current state matrix with rotation matrix
		\param angle rotation angle
		\param axis rotation angle
		*/
		void rotate(float angle, const Vec3 &axis);

		/**
		Mults the current state matrix with scale matrix
		\param coef scale coefs
		*/
		void scale(const Vec3 &coef);

		/**
		Draws indexed geometry
		\param indices indices data
		\param indexCount number of indices to draw
		*/
		void drawIndexedGeometry(void *indices, int indexCount);

		/**
		Draws unindexed geometry
		\param vertexCount number of indices to draw
		*/
		void drawGeometry(int vertexCount);

	private:
		friend class GLTexture;
		friend class GLVBO;
		friend class GLShader;
		friend class GLDisplayList;
	};
}
/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//**************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/GLSystem.h"
//**************************************

namespace Vega {

	class ENGINE_API Font {
	public:
		static Font* Create(const String &path);
		void Destroy();

		void print(int x, int y, int size, const String &text, const Vec3 &color, float alpha);

	private:
		GLTexture *fontTex;
		GLDisplayList *list[256];
	};
}
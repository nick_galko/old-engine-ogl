/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//***************************************************************************
#include "../Inc/IncludesAndLibs.h"
#include "../Inc/CompileConfig.h"
//***************************************************************************
#include "../Inc/String.h"
#include "../Inc/ALSound.h"
#include "../Inc/ALSoundSource.h"
//***************************************************************************

namespace Vega {

	/**
	Engine`s main sound system. Created one time
	*/
	class ALSystem {
	public:
		ALSystem();
		/**
		Creates new ALSystem
		\return pointer to new ALSystem
		*/
		void Initialize();

		/**
		Returns existing ALSystem
		\return pointer to existing ALSystem
		*/
		static ALSystem *Get();

		/**
		Destroys ALSystem
		*/
		void Destroy();

		/**
		Gets sound card vendor
		\return vendor name
		*/
		String getVendor();

		/**
		Gets sound card renderer
		\return renerer name
		*/
		String getRenderer();

		/**
		Gets OpenAL version
		\return version number
		*/
		String getVersion();

		/**
		Gets sound card extensions
		\return extensions string
		*/
		String getExtensions();

		/**
		Sets the listener orientation
		\param position position of the listener
		\param dir direction of the listener
		*/
		void setListener(const Vec3 &pos, const Vec3 &dir);

	private:
		ALCcontext *alContext;
		ALCdevice  *alDevice;
	};
}
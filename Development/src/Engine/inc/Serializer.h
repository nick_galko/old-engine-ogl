/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//**************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/String.h"
#include "../Inc/FStream.h"
#include "../Inc/MathLib.h"
#include <vector>
#include <map>
//**************************************

namespace Vega {

	/**
	Serializer class
	*/
	class ENGINE_API Serializer {
	public:
		static Serializer *Create();
		void Destroy();

		void save(FStream *file);

		int getInt(const String &name);
		float getFloat(const String &name);
		Vec2 getVec2(const String &name);
		Vec3 getVec3(const String &name);
		Vec4 getVec4(const String &name);
		Mat4 getMat4(const String &name);
		String getString(const String &name);

		void setInt(const String &name, int value);
		void setFloat(const String &name, float value);
		void setVec2(const String &name, const Vec2 &value);
		void setVec3(const String &name, const Vec3 &value);
		void setVec4(const String &name, const Vec4 &value);
		void setMat4(const String &name, const Mat4 &value);
		void setString(const String &name, const String &value);

	private:
		std::map<String, int> s_int;
		std::map<String, float> s_float;
		std::map<String, Vec2> s_vec2;
		std::map<String, Vec3> s_vec3;
		std::map<String, Vec4> s_vec4;
		std::map<String, Mat4> s_mat4;
		std::map<String, String> s_string;
	};

}
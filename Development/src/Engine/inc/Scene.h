/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//**************************************
#include "../Inc/Light.h"
#include "../Inc/ParticleSystem.h"
#include "../Inc/Object.h"
#include "../Inc/Terrain.h"
#include "../Inc/Camera.h"
#include "../Inc/GLSystem.h"
#include "../Inc/Water.h"
#include "../Inc/Skydome.h"
#include <vector>
//**************************************

namespace Vega {

	/**
	Class of the scene. Created one time
	*/
	class ENGINE_API Scene {
	public:

		Scene();

		/**
		Initialize new scene with ambient light
		*/
		void Initialize();

		/**
		returns pointer to the existing scene
		*/
		static Scene *Get();


		/**
		destroys scene
		*/
		void Destroy();


		/**
		destroys scene
		*/
		void clear();

		/**
		main draw function
		*/
		void draw();

		/**
		main update function
		*/
		void update();


		/**
		sets the scene gravity value
		\param gravity  gravity value
		*/
		void setGravity(const Vec3 &gravity);


		/**
		sets the ambient color of the scene
		\param ambient  pointer to LightAmbient
		*/
		void setAmbient(const Vec3 &color);

		/**
		sets the camera of the scene
		\param camera  pointer to camera
		*/
		void setCamera(Camera *camera);

		/**
		sets water depth and size
		*/
		void setWater(Water *water);

		/**
		Creates terrain
		*/
		void setTerrain(Terrain *terrain);

		/**
		Creates skydome
		*/
		void setSkydome(Skydome *skydome);


		/**
		adds new object to scene
		\param object  pointer to ActorMeshs to add
		*/
		void addObject(Object *object);

		/**
		adds new light to scene
		\param light  pointer to Light to add
		*/
		void addLight(Light *light);


		/**
		removes ActorMesh from scene
		\param object  pointer to ActorMesh to remove
		*/
		void deleteObject(Object *object);

		/**
		removes Light from scene
		\param light  pointer to Light to remove
		*/
		void deleteLight(Light *light);


		/**
		reload all shaders
		*/
		void reloadShaders();


		/**
		finds the intersected object
		\param src  ray src
		\param dst  ray dst
		\param normal  intersection normal
		\param point  intersection point
		\return pointer to intersected object or NULL
		*/
		Object *intersectObjects(const Vec3 &src, const Vec3 &dst, Vec3 &normal, Vec3 &point);

		/**
		checks if the terrain is intersected
		\param src  ray src
		\param dst  ray dst
		\param normal  intersection normal
		\param point  intersection point
		\return true if intersected
		*/
		bool intersectTerrain(const Vec3 &src, const Vec3 &dst, Vec3 &normal, Vec3 &point);

	private:

		/**
		draws the ambient pass
		*/
		void drawAmbient(bool blended);


		/**
		draws all lights
		*/
		void drawLights(bool blended);

		/**
		checks visibility of all the lights
		*/
		void checkLightsVisibility();

		/**
		gets shadow maps of all the lights
		*/
		void getLightsShadowMaps();


		/**
		draws point lights
		\param light  point light to draw
		*/
		void drawPoint(LightPoint *light, bool blended);

		/**
		draws spot light
		\param light  spot light to draw
		*/
		void drawSpot(LightSpot *light, bool blended);

		/**
		draws direct light
		\param light  direct light to draw
		*/
		void drawDirect(LightDirect *light, bool blended);


		/**
		gets point light shadow map
		\param light  point light to get shadow map
		*/
		void getPointShadowMap(LightPoint *light);

		/**
		draws spot light shadow map
		\param light  spot light to get shadow map
		*/
		void getSpotShadowMap(LightSpot *light);


		/**
		checks point light visibility
		\param light  point light to check
		*/
		void checkPointVisibility(LightPoint *light);

		/**
		checks spot light visibility
		\param light  spot light to check
		*/
		void checkSpotVisibility(LightSpot *light);


		/**
		draws water
		*/
		void drawWater();

		/**
		draws skydome
		*/
		void drawSkydome();

		Vec3 ambient;
		Vec3 gravity;
		Camera *camera;

		Water *water;
		Terrain *terrain;
		Skydome *skydome;

		std::vector<Object*> objects;
		std::vector<Light*> lights;
		std::vector<Light*> visibleLights;

		GLPBuffer *viewportPBO;
		GLPBuffer *shadowPBO;

		GLTexture *reflectionMap;
		GLTexture *viewportCopy;
		GLTexture *viewportCopy_brightPass;
		GLTexture *viewportCopy_brightPass_blured;

		Material *depthPass;
		Material *hdr;

		GLOcclusionQuery *query;
		Frustum *frustum;
		Mesh *sphereMesh;

		friend class Light;
		friend class LightPoint;
		friend class LightSpot;
		friend class LightDirect;

		friend class Object;
		friend class ActorMesh;
		friend class ObjectSkinnedMesh;
		friend class ObjectParticleSystem;

		friend class Water;
		friend class Terrain;
		friend class Skydome;

		friend class Material;
	};

}
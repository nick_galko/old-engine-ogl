/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once 

//***************************************************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/IncludesAndLibs.h"
//***************************************************************************
#include "../Inc/ILImage.h"
//***************************************************************************

namespace Vega {

	/**
	Texture class
	*/
	class ENGINE_API GLTexture {
	public:
		/**
		Texture target enum
		*/
		enum Target {
			TEXTURE_2D = GL_TEXTURE_2D,
			TEXTURE_3D = GL_TEXTURE_3D_EXT,
			TEXTURE_CUBE = GL_TEXTURE_CUBE_MAP_ARB
		};

		/**
		Texture src format enum
		*/
		enum Format {
			RGB,
			RGBA,

			RGB_16,
			RGBA_16,

			RGB_FP16,
			RGBA_FP16,

			RGB_FP32,
			RGBA_FP32
		};

		/**
		Texture filter enum
		*/
		enum Filter {
			NEAREST,
			LINEAR,
			NEAREST_MIPMAP_NEAREST,
			LINEAR_MIPMAP_NEAREST,
			LINEAR_MIPMAP_LINEAR
		};

		/**
		Texture edge wrap enum
		*/
		enum Wrap {
			REPEAT = GL_REPEAT,
			CLAMP = GL_CLAMP,
			CLAMP_TO_EDGE = GL_CLAMP_TO_EDGE
		};

		/**
		Texture anisotropy enum
		*/
		enum Aniso {
			ANISO_X0 = 0,
			ANISO_X1 = 1,
			ANISO_X2 = 2,
			ANISO_X4 = 4,
			ANISO_X8 = 8,
			ANISO_X16 = 16
		};

	public:
		/**
		Creates 2d GLTexture from file
		\param path image file path
		\return pointer to new Texture
		*/
		static GLTexture *Create2d(const String &path);

		/**
		Adds cube GLTexture from 6 files
		\param path pointer to file path
		\return pointer to new GLTexture
		*/
		static GLTexture *CreateCube(const String &path);


		/**
		Creates 2d GLTexture from image
		\param image pointer to ILImage to use
		\return pointer to new GLTexture
		*/
		static GLTexture *Create2d(ILImage *image);

		/**
		Creates 3d GLTexture from image
		\param image pointer to ILImage to use
		\return pointer to new GLTexture
		*/
		static GLTexture *Create3d(ILImage *image);

		/**
		Creates GLTexture from 6 images
		\param image pointer to arrray of 6 ILImages to use
		\return pointer to new GLTexture
		*/
		static GLTexture *CreateCube(ILImage **image);

		/**
		Creates empty 2d GLTexture
		\param width tex size
		\param height texture edges wrap
		\param format tex format
		\return pointer to new GLTexture
		*/
		static GLTexture *Create2d(int width, int height, Format format);

		/**
		Creates empty 3d GLTexture
		\param width tex size
		\param height texture edges wrap
		\param format tex format
		\return pointer to new GLTexture
		*/
		static GLTexture *Create3d(int width, int height, int depth, Format format);

		/**
		Creates empty CUBE GLTexture
		\param width tex size
		\param height texture edges wrap
		\param format tex format
		\return pointer to new GLTexture
		*/
		static GLTexture *CreateCube(int width, int height, Format format);

		/**
		Destroys GLTexture
		*/
		void Destroy();


		/**
		Sets GLTexture wrap
		\param wrap wrap value
		*/
		void setWrap(Wrap wrap);

		/**
		Sets GLTexture filter
		\param filter filter value
		*/
		void setFilter(Filter filter);

		/**
		Sets GLTexture aniso
		\param aniso aniso value
		*/
		void setAniso(Aniso aniso);

		/**
		Sets texture
		\param tex_unit texture unit number
		*/
		void set(int tex_unit);

		/**
		Sets texture
		\param tex_unit texture unit number
		*/
		void unset(int tex_unit);


		/**
		Begins render to texture
		*/
		void beginRenderTo();

		/**
		Copy screen to texture
		\param face face number (for cube textures)
		*/
		void copy(int face = -1);

		/**
		Ends render to texture
		*/
		void endRenderTo();

	private:

		/**
		Creates new texture
		\param width tex size
		\param height texture edges wrap
		\param depth texture edges wrap
		\param target tex type
		\param format tex src format
		\param wrap tex edges wrap
		\param filter tex filter
		\param aniso tex aniso level
		\param data image data
		\return pointer to new texture
		*/
		static GLTexture *Create(int width, int height, int depth, Target target, Format format, void **data);

		GLuint glID;
		int width, height, depth;

		GLuint target;

		GLuint minFilter;
		GLuint magFilter;

		GLuint aniso;
		GLuint wrap;

		GLuint internalFormat;
		GLuint srcFormat;
		GLenum dataType;

		friend class GLFBO;
	};
}
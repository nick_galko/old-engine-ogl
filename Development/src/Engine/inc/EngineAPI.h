/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once 

//***************************************************
#include "../../Common/inc/CommonPrivate.h"
#include "../Inc/Engine.h"

#include "../Inc/WindowSystem.h"
#include "../Inc/GLSystem.h"
#include "../Inc/ILSystem.h"
#include "../Inc/ALSystem.h"
#include "../Inc/PhysSystem.h"
#include "../Inc/ParticleSystem.h"
#include "../Inc/ScriptSystem.h"
#include "../Inc/Cache.h"
#include "../../Common/inc/LogFuncts.h"
#include "../Inc/Config.h"

#include "../Inc/LoadingScreen.h"

#include "../Inc/Scene.h"
#include "../Inc/Object.h"
#include "../Inc/Terrain.h"
#include "../Inc/Light.h"
#include "../Inc/Camera.h"

#include "../Inc/Font.h"
#include "../Inc/GUI.h"
#include "../Inc/Widget.h"
//***************************************************


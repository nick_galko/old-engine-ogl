/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once 

//***************************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/WindowSystem.h"
#include "../Inc/GLSystem.h"
#include "../Inc/ILSystem.h"
#include "../Inc/ALSystem.h"
#include "../Inc/PhysSystem.h"
#include "../Inc/Cache.h"
#include "../Inc/Config.h"
#include "../Inc/Scene.h"
#include "../Inc/GUI.h"
//***************************************************

namespace Vega {

	/**
	Engine`s main class. Created one time
	*/
	class ENGINE_API Engine {
	public:

		/**
		Creates new Vega
		\return pointer to new Vega
		*/
		static Engine *Create();


		/**
		Creates new Vega
		\return pointer to new Vega
		*/
		void init();

		/**
		returns existing Vega
		\return pointer to existing Vega
		*/
		static Engine *Get();

		void Destroy();

		typedef void(*EngineCallback)();


		/**
		sets render callback
		\param callback  pointer to render function
		*/
		void renderCallback(EngineCallback callback);

		/**
		sets events callback
		\param callback  pointer to events function
		*/
		void eventsCallback(EngineCallback callback);


		/**
		engines main loop
		*/
		void mainLoop();


		/**
		exits the main loop
		*/
		void quit();


		/**
		engines main loop
		*/
		void _update();


	private:
		EngineCallback render_callback;
		EngineCallback events_callback;
		bool rc, ec;
	public:
		//Engine
		class WindowSystem *windowSystem;
		
		class GLSystem *glSystem;
		
		class ALSystem *alSystem;
		
		class ILSystem *ilSystem;
		
		class PhysSystem *physSystem;
		
		class Scene* scene;

		class ScriptSystem*script;
		//Core
		class Cache *cache;
		
		class Config *config;

		class Log* log;

		bool running;
	};
	ENGINE_API Engine*GetEngine();
}
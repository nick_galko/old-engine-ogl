/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//***************************************************************************
#include "../Inc/IncludesAndLibs.h"
#include "../Inc/CompileConfig.h"
//***************************************************************************
#include "../Inc/String.h"
//***************************************************************************

namespace Vega {

	/**
	Sound class
	*/
	class ENGINE_API ALSound {
	public:

		/**
		Creates ALSound from file
		\param path sound file path
		\return pointer to new ALSound
		*/
		static ALSound *Create(const String &path);

		/**
		Destroy ALSound
		*/
		void Destroy();

	private:
		ALenum format;
		short *samples;

		int rate;
		int size;

		ALuint buffID;

		friend class ALSoundSource;
	};

}
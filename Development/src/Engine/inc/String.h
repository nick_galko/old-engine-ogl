/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//**************************************
#include "../Inc/CompileConfig.h"
#include <string>
//**************************************

namespace Vega {

	/**
	std::string
	*/
	class ENGINE_API String : public std::string {
	public:
		/**
		String constructor
		*/
		String();

		/**
		String constructor
		*/
		String(const char *str);

		/**
		String constructor
		*/
		String(std::string &str);

		/**
		String constructor
		*/
		String(int i);

		/**
		String constructor
		*/
		String(float i);

		/**
		String constructor
		*/
		String(double i);

		/**
		String constructor
		*/
		String(bool i);

		/**
		Gets word from String. Words are separated by space
		\param n word number
		\return word from the String
		*/
		String getWord(int n);

		/**
		Gets quoted word from String. Words are separated by space and quoted word can contain spaces
		\param n word number
		\return word from the String
		*/
		String getQuotedWord(int n);

		/**
		Gets words count. Words are separated by space
		\param input String containing words
		\return number of words in String
		*/
		int getWordCount();

		/**
		Cuts file ext from file name
		\return modified name
		*/
		String cutFileExt();

		/**
		Gets file ext from file name
		\return file name
		*/
		String getFileExt();

		/**
		Converts String to int
		\return converted to int
		*/
		int toInt();

		/**
		Converts String to float
		\return converted to float
		*/
		float toFloat();

		/**
		Converts String to double
		\return converted to double
		*/
		double toDouble();

		/**
		Converts String to bool
		\return converted to bool
		*/
		bool toBool();

		/**
		sprintf analog
		\param format format String
		\return sprintf to String
		*/
		void printf(const String &format, ...);

		/**
		Converts String to const char *
		\return converted to const char *
		*/
		operator const char*() const;
	};
}

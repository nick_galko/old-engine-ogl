/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//***************************************************************************
#include "CompileConfig.h"
#include "IncludesAndLibs.h"
//***************************************************************************
#include "PhysBody.h"
#include "MathLib.h"
//**************************************
struct NewtonJoint;

namespace Vega {

	/**
	Class of the phys joint
	*/
	class ENGINE_API PhysJoint {};


	/**
	Class of the up vector joint
	*/
	class ENGINE_API PhysJointUpVector : public PhysJoint {
	public:
		PhysJointUpVector(const Vec3 &direction, PhysBody *body);
		~PhysJointUpVector();

	private:
		NewtonJoint *nJoint;
	};
}
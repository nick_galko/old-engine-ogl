/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/GLSystem.h"
#include "../Inc/Material.h"
#include "../Inc/Mesh.h"
//************************************

namespace Vega {

	/**
	Skydome class
	*/
	class ENGINE_API Skydome {
	public:

		/**
		skydome constructor
		*/
		static Skydome *Create();

		/**
		skydome deconstructor
		*/
		void Destroy();

		/**
		draw skydome
		*/
		void draw();

		void setMaterial(const String &path) {
			this->material = Material::Create(path);
		}

		Material *getMaterial() {
			return material;
		}

		void serialize(FStream *file);

	private:
		Material *material;
		Mesh *sphereMesh;
	};
}
/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//**************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/String.h"
#include "../Inc/MathLib.h"
#include "../Inc/GLSystem.h"
#include "../Inc/MathLib.h"
#include "../Inc/PhysSystem.h"
#include "../Inc/Frustum.h"
#include "../Inc/Material.h"
//**************************************

namespace Vega {

	/**
	Mesh class
	*/
	class ENGINE_API Mesh {
	public:

		/**
		Creates new Mesh from file
		\param path  object file path
		*/
		static Mesh *Create(const String &path);

		/**
		destroys object
		*/
		void Destroy();

		/**
		saves mesh to file
		\param path  file path
		*/
		void save(const String &path);

		/**
		draws object subset
		\param s  surface number
		*/
		void drawSubset(int s);
		int getNumSubsets() { return numSubsets; }

		/**
		get subset number by number
		\param s  subset name
		\return subset number
		*/
		int getSubset(String name);

		Vec3 &getMax() { return max; }
		Vec3 &getMin() { return min; }
		Vec3 &getCenter() { return center; }
		float getRadius() { return radius; }

		Vec3 &getMax(int s) { return subsets[s]->max; }
		Vec3 &getMin(int s) { return subsets[s]->min; }
		Vec3 &getCenter(int s) { return subsets[s]->center; }
		float getRadius(int s) { return subsets[s]->radius; }

	private:
		void loadXSMSH(const String &path);

		struct Vertex {
			Vec3 position;
			Vec2 texcoord;
			Vec3 normal;
			Vec3 tangent;
			Vec3 binormal;
		};

		struct Subset {
			String name;

			unsigned int numVertices;
			unsigned int numIndices;

			Vertex *vertices;
			unsigned int *indices;

			GLVBO *vertBuff;

			Vec3 min, max;
			Vec3 center;
			float radius;
		};

		int numSubsets;
		Subset **subsets;

		Vec3 min, max;
		Vec3 center;
		float radius;

		void calculateBoundings();

		void calculateTBN();
		void createVBO();

		bool visible;

		friend class MeshLoader;
		friend class Scene;
		friend class ActorMesh;
	};

}


/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/
#pragma once

//***************************************************************************
#include "CompileConfig.h"
#include "IncludesAndLibs.h"
//***************************************************************************
#include "MathLib.h"
#include "ALSound.h"
#include "ALSoundSource.h"
//***************************************************************************
struct NewtonBody;
namespace Vega {


	/**
	Rigid body struct
	*/
	class ENGINE_API PhysBody {
	public:
		/**
		Creates phys body with box coliider
		\param size box size
		\param mass box mass
		\return pointer to PhysBody
		*/
		static PhysBody *CreateBox(const Vec3 &size, float mass = 0);

		/**
		Creates phys body with sphere coliider
		\param size sphere size
		\param mass sphere mass
		\return pointer to PhysBody
		*/
		static PhysBody *CreateSphere(float radius, float mass = 0);

		/**
		Creates phys body with cylinder coliider
		\param radius cylinder radius
		\param height cylinder height
		\param mass cylinder mass
		\return pointer to PhysBody
		*/
		static PhysBody *CreateCylinder(float radius, float height, float mass = 0);

		/**
		Creates phys body with cone coliider
		\param radius cone radius
		\param height cone height
		\param mass cone mass
		\return pointer to PhysBody
		*/
		static PhysBody *CreateCone(float radius, float height, float mass = 0);

		/**
		Creates phys body with capsule coliider
		\param radius capsule radius
		\param height capsule height
		\param mass capsule mass
		\return pointer to PhysBody
		*/
		static PhysBody *CreateCapsule(float radius, float height, float mass = 0);

		/**
		Creates phys body with champfer cylinder coliider
		\param radius champfer cylinder radius
		\param height champfer cylinder height
		\param mass champfer cylinder mass
		\return pointer to PhysBody
		*/
		static PhysBody *CreateChampferCylinder(float radius, float height, float mass = 0);

		/**
		Creates phys body with convex hull coliider
		\param pos array of positions
		\param numPos number of positions
		\param mass convex hull mass
		\return pointer to PhysBody
		*/
		static PhysBody *CreateConvexHull(Vec3 *pos, const int numPos, float mass = 0);

		/**
		Creates phys body with static mesh coliider
		\param pos array of positions
		\param numPos number of positions
		\param mass static mesh mass
		\return pointer to PhysBody
		*/
		static PhysBody *CreateStaticMesh(Vec3 *pos, const int numPos, bool optimize);

		/**
		Destroys PhysBody
		*/
		~PhysBody();

		/**
		Set transformation matrix
		\param trans transformation matrix to set
		*/
		void setTransform(const Mat4 &trans);

		/**
		Get transformation matrix
		\return transformation matrix
		*/
		Mat4 getTransform();

		/**
		Get mass
		\return mass
		*/
		float getMass();

		/**
		Set mass
		\param mass mass
		*/
		void setMass(float mass);

		/**
		Add force
		\param force force vector to add
		*/
		void addForce(const Vec3 &force);

		/**
		Get force
		\return force vector
		*/
		Vec3 getForce();

		/**
		Add torque
		\param torque torque vector to add
		*/
		void addTorque(const Vec3 &torque);

		/**
		Get torque
		\return torque vector
		*/
		Vec3 getTorque();

		/**
		Add velocity
		\param velocity velocity vector to add
		*/
		void addVelocity(const Vec3 &velocity);

		/**
		Set velocity
		\param velocity velocity vector to set
		*/
		void setVelocity(const Vec3 &velocity);

		/**
		Get velocity
		\return velocity vector
		*/
		Vec3 getVelocity();

		typedef void(*ContactCallback)();

		/**
		Set impact sound
		\param snd pointer to sound to set
		*/
		void setImpactSound(ALSound *snd) {
			impactSrc = ALSoundSource::Create(snd);
		}

	private:
		NewtonBody *nBody;

		ALSoundSource *impactSrc;

		Vec3 force;
		Vec3 torque;
		Vec3 impulse;
		Vec3 velocity;

		float mass;

		static void applyForce_Callback(const NewtonBody* body, float timestep, int threadIndex);

		ContactCallback contact;
		bool cc;

		friend class PhysSystem;
		friend class PhysJoint;
		friend class PhysJointUpVector;
	};
}

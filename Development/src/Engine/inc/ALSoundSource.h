/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once

//***************************************************************************
#include "../Inc/IncludesAndLibs.h"
#include "../Inc/CompileConfig.h"
//***************************************************************************
#include "../Inc/ALSound.h"
#include "../Inc/MathLib.h"
//***************************************************************************

namespace Vega {

	/**
	Sound source class
	*/
	class ENGINE_API ALSoundSource {
	public:

		/**
		Creates new ALSoundSource
		\param sound pointer to the ALSound used by the ALSoundSource
		\return pointer to new ALSoundSource
		*/
		static ALSoundSource *Create(ALSound *sound);

		/**
		Destroys ALSoundSource
		*/
		void Destroy();

		/**
		Plays the ALSoundSource
		*/
		void play();

		/**
		Stops the ALSoundSource playing
		*/
		void stop();

		/**
		Pauses the ALSoundSource playing
		*/
		void pause();

		/**
		Is the ALSoundSource playing
		\return true if ALSoundSource is playing
		*/
		bool isPlaying();

		/**
		Sets ALSoundSource looping on/off
		\param loop loop flag
		*/
		void setLooping(bool loop);

		/**
		Enables ALSoundSource relative position on/off
		\param relative relPos flag
		*/
		void setRelative(bool relative);

		/**
		Sets ALSoundSource gain
		\param gain gain value
		*/
		void setGain(float gain);

		/**
		Sets ALSoundSource position
		\param pos new ALSoundSource position
		*/
		void setPosition(const Vec3 &position);

		/**
		Sets ALSoundSource rolloff factor
		\param rolloffFactor rolloff factor value
		*/
		void setRolloffFactor(float rolloffFactor);

		/**
		Sets ALSoundSource reference distance
		\param referenceDisatance reference distance value
		*/
		void setReferenceDistance(float referenceDistance);

		/**
		Sets ALSoundSource max distance
		\param maxDisatance max distance value
		*/
		void setMaxDistance(float maxDistance);

	private:
		ALuint alID;
	};

}
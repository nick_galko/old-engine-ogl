/***************************************************************************
 *   Copyright (C) 2006 by AST   *
 *   tsyplyaev@gmail.com   *
 *   ICQ: 279-533-134                          *
 *   This is a part of work done by AST.       *
 *   If you want to use it, please contact me. *
 ***************************************************************************/

#pragma once 

//***************************************************************************
#include "../Inc/CompileConfig.h"
#include "../Inc/IncludesAndLibs.h"
//***************************************************************************
#include "../Inc/MathLib.h"
#include "../Inc/String.h"
//***************************************************************************

namespace Vega {

	/**
	Shader class
	*/
	class ENGINE_API GLShader {
	public:
		/**
		Creates new GLShader
		\param path shader file path
		\defines shader code defines
		\return pointer to the new GLShader
		*/
		static GLShader *Create(const String &path, const String &defines = "");

		/**
		Destroys  GLShader
		*/
		void Destroy();

		/**
		Sets shader
		*/
		void set();

		/**
		Unsets shader
		*/
		void unset();


		/**
		Sends uniform param to shader
		\param name parameter name
		\param value param value
		*/
		void sendMat4(const String &name, const Mat4 &value);

		/**
		Sends uniform param to shader
		\param name parameter name
		\param value param value
		*/
		void sendVec4(const String &name, const Vec4 &value);

		/**
		Sends uniform param to shader
		\param name parameter name
		\param value param value
		*/
		void sendVec3(const String &name, const Vec3 &value);

		/**
		Sends uniform param to shader
		\param name parameter name
		\param value param value
		*/
		void sendVec2(const String &name, const Vec2 &value);

		/**
		Sends uniform param to shader
		\param name parameter name
		\param value param value
		*/
		void sendFloat(const String &name, float value);

		/**
		Sends uniform param to shader
		\param name parameter name
		\param value param value
		*/
		void sendInt(const String &name, int value);

		const String &getSource() { return source; }
	private:
		GLhandleARB vs;
		GLhandleARB fs;
		GLhandleARB program;

		String source;
	};

}

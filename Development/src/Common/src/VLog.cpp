#include "CommonPrivate.h"
#include "VLog.h"

namespace Vega{
	/**
	*/
	Log* _gLogSingleton;
	/**
	*/
	Log::Log(){
		TODO("RENAME ON JUST LOG")
		if(!init("..\\vegaengine.html"))
			return;
		_writeHTMLHeader();
		_gLogSingleton = this;
	}
	/**
	*/
	Log::~Log(){
		_write("</body>");
	}
	/**
	*/
	bool Log::init(const char *_fileName)
	{
		filename = _fileName;
		FILE *fLog;
		fLog = fopen(_fileName, "wb");
		fclose(fLog);
		return true;
	}
	/**
	*/
	void Log::logPrintf(std::string _message){
		std::string message = "<div id=\"msg\">" + _message;
		message += "</div>";
		_write(message);
	}
	/**
	*/
	void Log::logWarn(std::string _message){
		std::string message = "<div id=\"warn\">" + _message;
		message += "</div>";
		_write(message);
	}
	/**
	*/
	void Log::logErr(std::string _message){
		std::string message = "<div id=\"err\">" + _message;
		message += "</div>";
		_write(message);
	}
	/**
	*/
	void Log::_write(std::string _message){
		_message += '\n';
		FILE *fLog;
		fLog = fopen(filename.c_str(), "a+t");
#ifdef WIN32
		fprintf(fLog, "%s", _message.c_str());
#else
		printf("%s", _message.c_str());
#endif
		fclose(fLog);
	}
	/**
	*/
	void Log::logHeader(std::string _message){
		std::string message = "<h2>--" + _message;
		message += "--</h2>";
		_write(message);
	}
	/**
	*/
	void Log::_writeHTMLHeader()
	{
		std::string header="<html><head><meta http-equiv=\"Content-Type\"content=\
			   \"text/html;charset=windows-1251\"/><title>VEGA Engine log file\
			   </title><style type=\"text/css\">body{background-color:\
			   #061920;padding:0px;}h1{font-size:18pt;font-family:Arial;\
			   color:#C9D6D6;margin:20px;}h2{font-size:10pt;font-family:\
			   Arial;color:#C9D6D6;margin:0px;padding-top:10px;}#msg{\
			   background-color:#39464C;font-size:10pt;font-family:\
			   Arial;color:white;padding-left:5px;margin-bottom:1px;\
			   }#warn{background-color:#A68600;font-size:11pt;font-\
			   weight:bold;font-family:Arial;color:white;padding-left\
			   : 15px;margin-bottom:1px;}#err{background-color:maroon; \
			   font-size:11pt;font-weight:bold;font-family:Arial;color\
			   :white;padding-left:15px;margin-bottom:1px;}</style>\
			   </head><body><h1>VEGA Engine log file</h1>";
		_write(header);
	}
	/**
	*/
	Log*Log::GetPtr(){
		return _gLogSingleton;
	}
}
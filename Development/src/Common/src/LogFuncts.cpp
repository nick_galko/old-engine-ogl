#include "CommonPrivate.h"
#include "VLog.h"

void ErrorMessageBox(const char *fmt){
	MessageBox(0, fmt, "Error", 0);
}

void Warning(const char *fmt, ...)
{
	va_list mark;
	char buf[8000];
	va_start(mark, fmt);
	int sz = vsprintf(buf, fmt, mark); buf[sizeof(buf)-1] = 0;
	va_end(mark);
	if (sz)
	{
		std::string sMsg = "[WARNING]: ";
		sMsg += buf; 
		using namespace Vega;
		if (Log::GetPtr())
			Log::GetPtr()->logWarn(sMsg);

	}
}

void LogPrintf(const char *fmt, ...)
{
	va_list mark;
	char buf[8000];
	va_start(mark, fmt);
	int sz = vsprintf(buf, fmt, mark); buf[sizeof(buf)-1] = 0;
	va_end(mark);
	if (sz)
	{
		using namespace Vega;
		if (Log::GetPtr())
			Log::GetPtr()->logPrintf(buf);
		else
			printf(buf);
	}
}

void Error(const char *fmt, ...)
{
	va_list argptr;
	char            msg[8000];
	va_start(argptr, fmt);
	_vstprintf(msg, fmt, argptr);
	va_end(argptr);

	if (std::string(msg).empty())
		return;

	using namespace Vega;
	if (Log::GetPtr())
		Log::GetPtr()->logErr(msg);
	ErrorMessageBox(msg);
}

void Debug(const char *fmt, ...)
{
#ifdef _DEVELOP
	va_list mark;
	char buf[8000];
	va_start(mark, fmt);
	int sz = vsprintf(buf, fmt, mark); buf[sizeof(buf)-1] = 0;
	va_end(mark);
	if (sz)
	{
		std::string sMsg = "[DEBUG]: ";
		sMsg += buf;
		LogPrintf(sMsg.c_str());
	}
#endif
}

void LogHeader(const char *fmt, ...)
{
	char msg[8000];

	va_list  argptr;
	va_start(argptr, fmt);
	vsprintf(msg, fmt, argptr);
	va_end(argptr);

	if (std::string(msg).empty())
		return;

	using namespace Vega;
	if (Log::GetPtr())
		Log::GetPtr()->logHeader(msg);
}
#include "CommonPrivate.h"

#if defined(WIN32) || defined(WIN64)
#include <intrin.h>
#include <d3d9.h>
#include <dxgi.h>
#include <d3d11.h>
#include <map>
#include "AutoDetectSpec.h"

namespace Win32SysInspect
{
#if defined(WIN32) && defined(WIN64)
	extern "C" void cpuid64(int*);
#endif
	class ApicExtractor
	{
	public:
		ApicExtractor(unsigned int logProcsPerPkg = 1, unsigned int coresPerPkg = 1)		{
			SetPackageTopology(logProcsPerPkg, coresPerPkg);
		}

		unsigned char SmtId(unsigned char apicId) const		{
			return apicId & m_smtIdMask.mask;
		}

		unsigned char CoreId(unsigned char apicId) const		{
			return (apicId & m_coreIdMask.mask) >> m_smtIdMask.width;
		}

		unsigned char PackageId(unsigned char apicId) const		{
			return (apicId & m_pkgIdMask.mask) >> (m_smtIdMask.width + m_coreIdMask.width);
		}

		unsigned char PackageCoreId(unsigned char apicId) const		{
			return (apicId & (m_pkgIdMask.mask | m_coreIdMask.mask)) >> m_smtIdMask.width;
		}

		unsigned int GetLogProcsPerPkg() const		{
			return m_logProcsPerPkg;
		}

		unsigned int GetCoresPerPkg() const		{
			return m_coresPerPkg;
		}

		void SetPackageTopology(unsigned int logProcsPerPkg, unsigned int coresPerPkg)		{
			m_logProcsPerPkg = (unsigned char)logProcsPerPkg;
			m_coresPerPkg = (unsigned char)coresPerPkg;

			m_smtIdMask.width = GetMaskWidth(m_logProcsPerPkg / m_coresPerPkg);
			m_coreIdMask.width = GetMaskWidth(m_coresPerPkg);
			m_pkgIdMask.width = 8 - (m_smtIdMask.width + m_coreIdMask.width);

			m_pkgIdMask.mask = (unsigned char)(0xFF << (m_smtIdMask.width + m_coreIdMask.width));
			m_coreIdMask.mask = (unsigned char)((0xFF << m_smtIdMask.width) ^ m_pkgIdMask.mask);
			m_smtIdMask.mask = (unsigned char)~(0xFF << m_smtIdMask.width);
		}

	private:
		unsigned char GetMaskWidth(unsigned char maxIds) const		{
			--maxIds;
			unsigned char msbIdx(8);
			unsigned char msbMask(0x80);
			while (msbMask && !(msbMask & maxIds))
			{
				--msbIdx;
				msbMask >>= 1;
			}
			return msbIdx;
		}

		struct IdMask		{
			unsigned char width;
			unsigned char mask;
		};

		unsigned char m_logProcsPerPkg;
		unsigned char m_coresPerPkg;
		IdMask m_smtIdMask;
		IdMask m_coreIdMask;
		IdMask m_pkgIdMask;
	};

	static bool IsAMD()
	{
#if defined(WIN32) || defined(WIN64)
		int CPUInfo[4];
		char refID[] = "AuthenticAMD";
		__cpuid(CPUInfo, 0x00000000);
		return ((int*)refID)[0] == CPUInfo[1] && ((int*)refID)[1] == CPUInfo[3] && ((int*)refID)[2] == CPUInfo[2];
#else
		return false;
#endif
	}

	static bool IsIntel()
	{
#if defined(WIN32) || defined(WIN64)
		int CPUInfo[4];
		char refID[] = "GenuineIntel";
		__cpuid(CPUInfo, 0x00000000);
		return ((int*)refID)[0] == CPUInfo[1] && ((int*)refID)[1] == CPUInfo[3] && ((int*)refID)[2] == CPUInfo[2];
#else
		return false;
#endif
	}

	void GetCPUName(char* pName, size_t bufferSize)
	{
		if (!pName || !bufferSize)
			return;

		char name[12 * 4 + 1];

		int CPUInfo[4];
		__cpuid(CPUInfo, 0x80000000);
		if (CPUInfo[0] >= 0x80000004)
		{
			__cpuid(CPUInfo, 0x80000002);
			((int*)name)[0] = CPUInfo[0];
			((int*)name)[1] = CPUInfo[1];
			((int*)name)[2] = CPUInfo[2];
			((int*)name)[3] = CPUInfo[3];

			__cpuid(CPUInfo, 0x80000003);
			((int*)name)[4] = CPUInfo[0];
			((int*)name)[5] = CPUInfo[1];
			((int*)name)[6] = CPUInfo[2];
			((int*)name)[7] = CPUInfo[3];

			__cpuid(CPUInfo, 0x80000004);
			((int*)name)[8] = CPUInfo[0];
			((int*)name)[9] = CPUInfo[1];
			((int*)name)[10] = CPUInfo[2];
			((int*)name)[11] = CPUInfo[3];

			name[48] = '\0';
		}
		else
			name[0] = '\0';

		int ret(_snprintf(pName, bufferSize, name));
		if (ret == bufferSize || ret < 0)
			pName[bufferSize - 1] = '\0';
	}
	
	// Preferred solution to determine the number of available CPU cores, works reliably only on WinVista/Win7 32/64 and above
	// See http://msdn2.microsoft.com/en-us/library/ms686694.aspx for reasons
	static void GetNumCPUCoresGlpi(unsigned int& totAvailToSystem, unsigned int& totAvailToProcess)
	{
		typedef BOOL(WINAPI *FP_GetLogicalProcessorInformation)(PSYSTEM_LOGICAL_PROCESSOR_INFORMATION, PDWORD);
		FP_GetLogicalProcessorInformation pglpi((FP_GetLogicalProcessorInformation)GetProcAddress(GetModuleHandle("kernel32"), "GetLogicalProcessorInformation"));
		if (pglpi)
		{
			unsigned long bufferSize(0);
			pglpi(0, &bufferSize);

			void* pBuffer(malloc(bufferSize));

			SYSTEM_LOGICAL_PROCESSOR_INFORMATION* pLogProcInfo((SYSTEM_LOGICAL_PROCESSOR_INFORMATION*)pBuffer);
			if (pLogProcInfo && pglpi(pLogProcInfo, &bufferSize))
			{
				DWORD_PTR processAffinity, systemAffinity;
				GetProcessAffinityMask(GetCurrentProcess(), &processAffinity, &systemAffinity);

				unsigned long numEntries(bufferSize / sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION));
				for (unsigned long i(0); i < numEntries; ++i)
				{
					switch (pLogProcInfo[i].Relationship)
					{
					case RelationProcessorCore:
					{
												  ++totAvailToSystem;
												  if (pLogProcInfo[i].ProcessorMask & processAffinity)
													  ++totAvailToProcess;
					}
						break;

					default:
						break;
					}
				}
			}

			free(pBuffer);
		}
	}
	
	static void GetNumCPUCoresApic(unsigned int& totAvailToSystem, unsigned int& totAvailToProcess)
	{
		unsigned int numLogicalPerPhysical(1);
		unsigned int numCoresPerPhysical(1);

		int CPUInfo[4];
		__cpuid(CPUInfo, 0x00000001);
		if ((CPUInfo[3] & 0x10000000) != 0) // Hyperthreading / Multicore bit set
		{
			numLogicalPerPhysical = (CPUInfo[1] & 0x00FF0000) >> 16;

			if (IsIntel())
			{
				__cpuid(CPUInfo, 0x00000000);
				if (CPUInfo[0] >= 0x00000004)
				{
#if defined(WIN32) && !defined(WIN64)
					__asm
					{
						mov eax, 4
							xor ecx, ecx
							cpuid
							mov CPUInfo, eax
					}
#else
					CPUInfo[0] = 4;
					CPUInfo[2] = 0;
					cpuid64(CPUInfo);
#endif
					numCoresPerPhysical = ((CPUInfo[0] & 0xFC000000) >> 26) + 1;
				}
			}
			else if (IsAMD())
			{
				__cpuid(CPUInfo, 0x80000000);
				if (CPUInfo[0] >= 0x80000008)
				{
					__cpuid(CPUInfo, 0x80000008);
					if (CPUInfo[2] & 0x0000F000)
						numCoresPerPhysical = 1 << ((CPUInfo[2] & 0x0000F000) >> 12);
					else
						numCoresPerPhysical = (CPUInfo[2] & 0xFF) + 1;
				}
			}
		}

		HANDLE hCurProcess(GetCurrentProcess());
		HANDLE hCurThread(GetCurrentThread());

		const int c_maxLogicalProcessors(sizeof(DWORD_PTR)* 8);
		unsigned char apicIds[c_maxLogicalProcessors] = { 0 };
		unsigned char items(0);

		DWORD_PTR processAffinity, systemAffinity;
		GetProcessAffinityMask(hCurProcess, &processAffinity, &systemAffinity);

		if (systemAffinity == 1)
		{
			assert(numLogicalPerPhysical == 1);
			apicIds[items++] = 0;
		}
		else
		{
			if (processAffinity != systemAffinity)
				SetProcessAffinityMask(hCurProcess, systemAffinity);

			DWORD_PTR prevThreadAffinity(0);
			for (DWORD_PTR threadAffinity = 1; threadAffinity && threadAffinity <= systemAffinity; threadAffinity <<= 1)
			{
				if (systemAffinity & threadAffinity)
				{
					if (!prevThreadAffinity)
					{
						assert(!items);
						prevThreadAffinity = SetThreadAffinityMask(hCurThread, threadAffinity);
					}
					else
					{
						assert(items > 0);
						SetThreadAffinityMask(hCurThread, threadAffinity);
					}

					Sleep(0);

					int CPUInfo[4];
					__cpuid(CPUInfo, 0x00000001);
					apicIds[items++] = (unsigned char)((CPUInfo[1] & 0xFF000000) >> 24);
				}
			}

			SetProcessAffinityMask(hCurProcess, processAffinity);
			SetThreadAffinityMask(hCurThread, prevThreadAffinity);
			Sleep(0);
		}

		ApicExtractor apicExtractor(numLogicalPerPhysical, numCoresPerPhysical);

		totAvailToSystem = 0;
		{
			unsigned char pkgCoreIds[c_maxLogicalProcessors] = { 0 };
			for (unsigned int i(0); i < items; ++i)
			{
				unsigned int j(0);
				for (; j < totAvailToSystem; ++j)
				{
					if (pkgCoreIds[j] == apicExtractor.PackageCoreId(apicIds[i]))
						break;
				}
				if (j == totAvailToSystem)
				{
					pkgCoreIds[j] = apicExtractor.PackageCoreId(apicIds[i]);
					++totAvailToSystem;
				}
			}
		}

		totAvailToProcess = 0;
		{
			unsigned char pkgCoreIds[c_maxLogicalProcessors] = { 0 };
			for (unsigned int i(0); i < items; ++i)
			{
				if (processAffinity & ((DWORD_PTR)1 << i))
				{
					unsigned int j(0);
					for (; j < totAvailToProcess; ++j)
					{
						if (pkgCoreIds[j] == apicExtractor.PackageCoreId(apicIds[i]))
							break;
					}
					if (j == totAvailToProcess)
					{
						pkgCoreIds[j] = apicExtractor.PackageCoreId(apicIds[i]);
						++totAvailToProcess;
					}
				}
			}
		}
	}


	void GetNumCPUCores(unsigned int& totAvailToSystem, unsigned int& totAvailToProcess)
	{
		totAvailToSystem = 0;
		totAvailToProcess = 0;

		GetNumCPUCoresGlpi(totAvailToSystem, totAvailToProcess);

		if (!totAvailToSystem)
			GetNumCPUCoresApic(totAvailToSystem, totAvailToProcess);
	}


	bool IsDX11Supported()
	{
		typedef HRESULT(WINAPI *FP_CreateDXGIFactory)(REFIID, void**);
		FP_CreateDXGIFactory pCDXGIF((FP_CreateDXGIFactory)GetProcAddress(LoadLibrary("dxgi.dll"), "CreateDXGIFactory"));

		bool dx11AdapterFound(false);
		IDXGIFactory* pFactory(0);
		if (pCDXGIF && SUCCEEDED(pCDXGIF(__uuidof(IDXGIFactory), (void**)&pFactory)))
		{
			typedef HRESULT(WINAPI *FP_D3D11CreateDevice)(IDXGIAdapter*, D3D10_DRIVER_TYPE, HMODULE, UINT, CONST D3D_FEATURE_LEVEL*, UINT, UINT, ID3D11Device**, D3D_FEATURE_LEVEL*, ID3D11DeviceContext **);
			FP_D3D11CreateDevice pD3D11CC((FP_D3D11CreateDevice)GetProcAddress(LoadLibrary("d3d11.dll"), "D3D11CreateDevice"));

			if (pD3D11CC)
			{
				unsigned int nAdapter(0);
				IDXGIAdapter* pAdapter(0);
				while (pFactory->EnumAdapters(nAdapter, &pAdapter) != DXGI_ERROR_NOT_FOUND)
				{
					if (pAdapter)
					{
						ID3D11Device* pDevice(0);
						D3D_FEATURE_LEVEL levels[] = { D3D_FEATURE_LEVEL_10_0, D3D_FEATURE_LEVEL_10_1, D3D_FEATURE_LEVEL_11_0 };
						HRESULT hr = pD3D11CC(pAdapter, D3D10_DRIVER_TYPE_HARDWARE, NULL, 0, levels, 3, D3D11_SDK_VERSION, &pDevice, NULL, NULL);
						if (SUCCEEDED(hr))
							dx11AdapterFound = true;

						SAFE_RELEASE(pDevice);
						SAFE_RELEASE(pAdapter);
					}
					if (dx11AdapterFound)
						break;
					++nAdapter;
				}
			}
		}
		SAFE_RELEASE(pFactory);
		return dx11AdapterFound;
	}


	void GetGPUInfo(char* pName, size_t bufferSize, unsigned int& vendorID, unsigned int& deviceID)
	{
		if (pName && !bufferSize)
			return;

		{
			if (pName)
				pName[0] = '\0';
			vendorID = 0;
			deviceID = 0;

			typedef IDirect3D9* (WINAPI *FP_Direct3DCreate9)(UINT);
			FP_Direct3DCreate9 pd3dc9((FP_Direct3DCreate9)GetProcAddress(LoadLibrary("d3d9.dll"), "Direct3DCreate9"));
			IDirect3D9* pD3D(pd3dc9 ? pd3dc9(D3D_SDK_VERSION) : 0);
			if (pD3D)
			{
				D3DADAPTER_IDENTIFIER9 adapterIdent;
				if (SUCCEEDED(pD3D->GetAdapterIdentifier(D3DADAPTER_DEFAULT, 0, &adapterIdent)))
				{
					if (pName)
					{
						int ret(_snprintf(pName, bufferSize, "%s", adapterIdent.Description));
						if (ret == bufferSize || ret < 0)
							pName[bufferSize - 1] = '\0';
					}
					vendorID = adapterIdent.VendorId;
					deviceID = adapterIdent.DeviceId;
				}

				D3DCAPS9 devCaps;
				if (SUCCEEDED(pD3D->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &devCaps)))
				{
					unsigned int vsMajor(D3DSHADER_VERSION_MAJOR(devCaps.VertexShaderVersion));
					unsigned int psMajor(D3DSHADER_VERSION_MAJOR(devCaps.PixelShaderVersion));
				}

				SAFE_RELEASE(pD3D);
			}
		}
	}
}
#else

void CSystem::AutoDetectSpec()
{
}
#endif
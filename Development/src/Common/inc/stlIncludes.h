#pragma once

#include <windows.h>
#include <stdio.h>
#include <assert.h>
#include <direct.h>
#include <sys/stat.h>
#include <mmsystem.h>
#include <memory.h>
#include <float.h>
#include <math.h>
#include <tchar.h>

// stl
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <algorithm>
#include <set>
#pragma once

#include "vlist.h"

namespace Vega {
	// FILE_MANAGER
	//   Simple manager for file operations.
	class IFileManager
	{
	public:
		IFileManager(){}
		~IFileManager(){}
		virtual void Release() = 0;

		// adds new search directory for data
		virtual void AddDirectory(const char *directory)=0;

		// removes all search directories
		virtual void RemoveDirectories() = 0;

		// checks, if file path exists
		virtual bool FilePathExists(const char *filePath) const = 0;

		// sets working directory
		virtual bool SetWorkDirectory(const char *workDirectory) const = 0;

		// gets directory, in which executable is located
		virtual bool GetExeDirectory(char *exeDirectory) const = 0;

		// gets file path for specified file name
		virtual bool GetFilePath(const char *fileName, char *filePath) const = 0;

		// gets file name for specified file path
		virtual bool GetFileName(const char *filePath, char *fileName) const = 0;
	protected:
		VList<char[255]> directories;
	};
}
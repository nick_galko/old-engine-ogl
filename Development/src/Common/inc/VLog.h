#pragma once

namespace Vega{

	class Log
	{
	public:
		/**
		*/
		Log();
		/**
		*/
		~Log();
		/**
		*/
		static Log*GetPtr();
		/**
		*/
		bool init(const char *pfileName);
		/**
		*/
		void logPrintf(std::string);
		/**
		*/
		void logHeader(std::string _message);
		/**
		*/
		void logWarn(std::string _message);
		/**
		*/
		void logErr(std::string _message);
	private:
		/**
		*/
		void _write(std::string);
		void _writeHTMLHeader();
	private:
		std::string filename;
	};
}
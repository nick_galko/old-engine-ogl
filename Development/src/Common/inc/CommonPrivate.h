#pragma once

#include "stlIncludes.h"
//Logging
#include "VLog.h"
#include "LogFuncts.h"
//Platform
#include "PlatformManagement.h"

#include "defines.h"
#include "VVersions.h"
//Core Interfaces
#include "IFileManager.h"
//PlatformDepend
#include "AutoDetectSpec.h"
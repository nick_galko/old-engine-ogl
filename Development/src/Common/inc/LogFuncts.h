#pragma once

/// Report warning without terminating program (stops program until user responds).
void Warning(const char *fmt, ...);

/// Report error and terminate program. Returns S_OK to shut up functions. Will never really return.
void Error(const char *fmt, ...);
/// Report error
void ErrorMessageBox(const char *fmt);


/// Report a message to the user for debug-only builds
void Debug(const char *fmt, ...);
/// Writing in Log
void LogPrintf(const char *fmt, ...);

/// Writes for subsystems Header
void LogHeader(const char *fmt, ...);
#ifndef PLATFORM_MANAGEMENT_H
#define PLATFORM_MANAGEMENT_H

#define PLATFORM_WINDOWS 1
#define PLATFORM_ANDROID 2

#ifdef _MSC_VER
#define CURRENT_PLATFORM PLATFORM_WINDOWS
#elif __ANDROID__
#define CURRENT_PLATFORM PLATFORM_ANDROID
#endif

#if CURRENT_PLATFORM == PLATFORM_WINDOWS 
//#include "../../PlatformWin/inc/PWPrivate.h"
#elif CURRENT_PLATFORM == PLATFORM_ANDROID 
#include "../../PatformAndroid/inc/PWPrivate.h"
#endif

#endif
#ifndef _AUTODETECTSPEC_
#define _AUTODETECTSPEC_

#pragma once


#if defined(WIN32) || defined(WIN64)

// exposed AutoDetectSpec() helper functions for reuse in CrySystem
namespace Win32SysInspect
{
	void GetNumCPUCores(unsigned int& totAvailToSystem, unsigned int& totAvailToProcess);
	bool IsDX11Supported();
	void GetGPUInfo(char* pName, size_t bufferSize, unsigned int& vendorID, unsigned int& deviceID);
	void GetCPUName(char* pName, size_t bufferSize);
}

#endif // #if defined(WIN32) || defined(WIN64)


#endif // #ifndef _AUTODETECTSPEC_
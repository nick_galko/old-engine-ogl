#include "../../../Engine/Inc/EngineAPI.h"

using namespace Vega;
//***************************************************

GLShader *mandelbotShader;
GLTexture *usor;
WidgetLabel *scaleLabel, *positionLabel;
WidgetButton *exitButton;

Vec2 position, scale;

void init() {
	GUI::Create("../gamedata/textures/gui");

	mandelbotShader = GLShader::Create("../gamedata/shaders/mandelbrot/mandelbrot.glsl");
	usor = GLTexture::Create2d("../gamedata/textures/usor.jpg");
	usor->setFilter(GLTexture::LINEAR);

	GUI::Get()->addWidget(scaleLabel = WidgetLabel::Create("Scale (mouse buttons)"));
	scaleLabel->setPosition(0, 0);

	GUI::Get()->addWidget(positionLabel = WidgetLabel::Create("Position (arrow keys)"));
	positionLabel->setPosition(0, 15);

	GUI::Get()->addWidget(exitButton = WidgetButton::Create("Exit"));
	exitButton->setPosition(20, 40);

	position = Vec2(-0.5, 0.0);
	scale = Vec2(1.0, 1.0);
}

void render() {
	GLSystem::Get()->enable2d(true);

	mandelbotShader->set();
	mandelbotShader->sendVec2("position", position);
	mandelbotShader->sendVec2("scale", scale);
	mandelbotShader->sendInt("colorMap", 0);

	usor->set(0);
	GLSystem::Get()->drawRect(0, 0, 1, 1, 0, 1, 1, 0);
	usor->unset(0);

	mandelbotShader->unset();
}

void events() {
	if (WindowSystem::Get()->isMouseButtonPressed(WindowSystem::MOUSE_LEFT_BUTTON)) {
		float s = (float)WindowSystem::Get()->getDTime() / 1000.0;
		scale = scale * 1.0 / (1.0 + s * 0.5);
	}

	if (WindowSystem::Get()->isMouseButtonPressed(WindowSystem::MOUSE_RIGHT_BUTTON)) {
		float s = (float)WindowSystem::Get()->getDTime() / 1000.0;
		scale = scale * (1.0 + s * 0.5);
	}

	if (WindowSystem::Get()->isKeyPressed(WindowSystem::KEY_UP)) {
		float p = (float)WindowSystem::Get()->getDTime() / 1000.0 * 0.5;
		position.y += p * scale.y;
	}

	if (WindowSystem::Get()->isKeyPressed(WindowSystem::KEY_DOWN)) {
		float p = (float)WindowSystem::Get()->getDTime() / 1000.0 * 0.5;
		position.y -= p * scale.y;
	}

	if (WindowSystem::Get()->isKeyPressed(WindowSystem::KEY_LEFT)) {
		float p = (float)WindowSystem::Get()->getDTime() / 1000.0 * 0.5;
		position.x -= p * scale.x;
	}

	if (WindowSystem::Get()->isKeyPressed(WindowSystem::KEY_RIGHT)) {
		float p = (float)WindowSystem::Get()->getDTime() / 1000.0 * 0.5;
		position.x += p * scale.x;
	}

	if (exitButton->isPressed() || WindowSystem::Get()->isKeyDown(WindowSystem::KEY_ESC)) {
		Engine::Get()->quit();
	}
}

//-------------------------------------------------------------
int main(int argc, char **argv) {
	Engine *engine = Engine::Create();

	init();
	engine->renderCallback(render);
	engine->eventsCallback(events);

	engine->mainLoop();

	engine->Destroy();

	return 0;
}

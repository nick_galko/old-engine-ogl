#include "../../../Engine/Inc/EngineAPI.h"

using namespace Vega;
//***************************************************

ActorMesh *box[5];
ActorMesh *sphere[5];
ActorMesh *cylinder[5];

ObjectSkinnedMesh *chammy;

CameraFree *camera;

LightDirect *dirl;

Water *water;
Skydome *skydome;
Terrain *terrain;

//GUI widgets
WidgetWindow *window;
WidgetLabel *label;
WidgetCheckBox *cb[3], *cbSP, *cbPrlx, *cbRefl, *cbHDR;
WidgetRadioGroup *group;
WidgetButton *exitButton;
WidgetLabel *fpsLabel;

//------------------------------------------------------------
void init() {
	WindowSystem::Get()->setTitle("Engine");
	//initializing loading screen
	LoadingScreen *lscreen = LoadingScreen::Create("../gamedata/textures/logos/background.jpg");
	lscreen->show();

	//initialize GUI
	GUI::Create("../gamedata/textures/gui");
	GUI::Get()->setAlpha(0.8);
	GUI::Get()->addWidget(fpsLabel = WidgetLabel::Create("FPS: "));
	fpsLabel->setPosition(20, 40);

	GUI::Get()->addWidget(window = WidgetWindow::Create());
	window->setPosition(500, 300);
	window->setEnabled(false);

	window->addWidget(label = WidgetLabel::Create("Settings"));
	label->setPosition(10, 10);
	window->addWidget(group = WidgetRadioGroup::Create());
	group->setPosition(10, 10);

	window->addWidget(cbSP = WidgetCheckBox::Create("specular"));
	cbSP->setPosition(10, 150);
	cbSP->setChecked(Config::Get()->getInt(CONF_SPECULAR));

	window->addWidget(cbPrlx = WidgetCheckBox::Create("paralax"));
	cbPrlx->setPosition(10, 180);
	cbPrlx->setChecked(Config::Get()->getInt(CONF_PARALLAX));

	window->addWidget(cbRefl = WidgetCheckBox::Create("reflections"));
	cbRefl->setPosition(10, 210);
	cbRefl->setChecked(Config::Get()->getInt(CONF_REFLECTIONS));

	window->addWidget(cbHDR = WidgetCheckBox::Create("HDR bloom"));
	cbHDR->setChecked(Config::Get()->getBool(CONF_HDR));
	cbHDR->setPosition(10, 240);

	group->addCheckBox(cb[0] = WidgetCheckBox::Create("no shadows"));
	cb[0]->setPosition(10, 30);
	group->addCheckBox(cb[1] = WidgetCheckBox::Create("sm shadows"));
	cb[1]->setPosition(10, 60);
	group->addCheckBox(cb[2] = WidgetCheckBox::Create("vsm shadows"));
	cb[2]->setPosition(10, 90);
	cb[Config::Get()->getInt(CONF_SHADOW_TYPE)]->setChecked(true);

	window->addWidget(exitButton = WidgetButton::Create("Exit"));
	exitButton->setPosition(300, 250);

	//initializing scene

	for (int i = 0; i < 5; i++) {
		box[i] = new ActorMesh("../gamedata/meshes/cube.xsmsh");
		box[i]->setMaterial("*", "../gamedata/materials/usor.xsmtr");
		box[i]->setPhysicsBox(Vec3(10, 10, 10), 10);
		box[i]->setTransform(Mat4::translate(Vec3(-10 - i * 2, i * 20 + 10, i - 10)));
		box[i]->setImpactSound("../gamedata/sounds/impact.ogg");
	}

	for (int i = 0; i < 5; i++) {
		sphere[i] = new ActorMesh("../gamedata/meshes/sphere.xsmsh");
		sphere[i]->setMaterial("*", "../gamedata/materials/usor.xsmtr");
		sphere[i]->setPhysicsSphere(Vec3(5, 5, 5), 10);
		sphere[i]->setTransform(Mat4::translate(Vec3(10 + i * 2, i * 20 + 10, i - 10)));
		sphere[i]->setImpactSound("../gamedata/sounds/impact.ogg");
	}

	for (int i = 0; i < 5; i++) {
		cylinder[i] = new ActorMesh("../gamedata/meshes/torus.xsmsh");
		cylinder[i]->setMaterial("*", "../gamedata/materials/usor.xsmtr");
		cylinder[i]->setPhysicsConvexHull(10);
		cylinder[i]->setTransform(Mat4::translate(Vec3(20 + i * 2, i * 20 + 20, i - 10)));
		cylinder[i]->setImpactSound("../gamedata/sounds/impact.ogg");
	}

	camera = CameraFree::Create(true);
	camera->setTransform(Mat4::translate(Vec3(20, 140, 0)));
	camera->setMaxVelocity(2000);
	camera->setPhysics(Vec3(5, 5, 5), 1.0);
	camera->setFOV(60);

	dirl = LightDirect::Create();
	dirl->setColor(Vec3(0.6, 0.6, 0.6));
	dirl->setTransform(Mat4::rotate(45, Vec3(1, 1, 1)));

	water = Water::Create();
	water->setSize(1e3);
	water->setDepth(27);

	skydome = Skydome::Create();
	skydome->setMaterial("../gamedata/materials/skybox_sunny.xsmtr");

	terrain = Terrain::Create("../gamedata/textures/terrain.png", 1.0, 50, 16);
	terrain->setLods(Vec4(400, 700, 1000, 1300));
	terrain->setPhysics();
	terrain->setMaterial("../gamedata/materials/terrain.xsmtr");

	Scene::Get()->setGravity(Vec3(0, -9.8, 0));
	Scene::Get()->setAmbient(Vec3(0.4, 0.4, 0.4));

	WindowSystem::Get()->grabMouse(true);
}

//------------------------------------------------------------
void render() {
}

//------------------------------------------------------------
void events() {
	if (WindowSystem::Get()->isKeyDown(WindowSystem::KEY_ESC)) {
		window->toggleEnable();
		WindowSystem::Get()->toggleGrabMouse();
	}

	for (int i = 0; i < 3; i++) {
		if (cb[i]->isClicked()) {
			Config::Get()->setInt(CONF_SHADOW_TYPE, i);
			Scene::Get()->reloadShaders();
		}
	}

	if (cbSP->isClicked()) {
		Config::Get()->setBool(CONF_SPECULAR, cbSP->isChecked());
		Scene::Get()->reloadShaders();
	}

	if (cbPrlx->isClicked()) {
		Config::Get()->setBool(CONF_PARALLAX, cbPrlx->isChecked());
		Scene::Get()->reloadShaders();
	}

	if (cbRefl->isClicked()) {
		Config::Get()->setBool(CONF_REFLECTIONS, cbRefl->isChecked());
		Scene::Get()->reloadShaders();
	}

	if (cbHDR->isClicked()) {
		Config::Get()->setBool(CONF_HDR, cbHDR->isChecked());
	}

	if (WindowSystem::Get()->getDTime() > 0)
		fpsLabel->setText("FPS: " + String(1000.0f / (float)WindowSystem::Get()->getDTime()));

	if (exitButton->isClicked()) {
		Engine::Get()->quit();
	}
}

//-------------------------------------------------------------
int main(int argc, char **argv) {
	Engine *engine = Engine::Create();

	init();
	engine->renderCallback(render);
	engine->eventsCallback(events);

	engine->mainLoop();
	engine->Destroy();

	return 0;
}
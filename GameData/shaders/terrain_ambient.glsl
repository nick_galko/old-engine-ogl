[GLSL_VERTEX_SHADER]

uniform mat4 u_mvp_transform;
varying vec2 v_tex_coord;

void main() {
	gl_Position = u_mvp_transform * gl_Vertex; 
	v_tex_coord = gl_MultiTexCoord0.xy;
}


[GLSL_FRAGMENT_SHADER]

varying vec2 v_tex_coord;

uniform vec3 u_light_color;
uniform sampler2D u_texture_0;
uniform sampler2D u_texture_1;
uniform vec4 u_material_param_0;

void main() {
	vec4 baseColor = texture2D(u_texture_0, v_tex_coord);
	vec4 detColor = texture2D(u_texture_1, v_tex_coord * u_material_param_0.x);
	
	gl_FragColor = baseColor * detColor * vec4(u_light_color, 1.0);
}
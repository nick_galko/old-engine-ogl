[GLSL_VERTEX_SHADER]

uniform mat4 u_mvp_transform;
uniform vec3 u_light_direction;

varying vec2 v_tex_coord;
varying vec3 v_light_vec;
varying vec3 v_normal;

void main() {
	gl_Position = u_mvp_transform * gl_Vertex; 
	v_tex_coord = gl_MultiTexCoord0.xy;

	v_normal = gl_Normal.xyz; 

	v_light_vec = -u_light_direction;
}



[GLSL_FRAGMENT_SHADER]

varying vec2 v_tex_coord;
varying vec3 v_light_vec;
varying vec3 v_normal;

uniform vec3 u_light_color;
uniform sampler2D u_texture_0;
uniform sampler2D u_texture_1;
uniform vec4 u_material_param_0;

void main() {

	vec3 lVec = normalize(v_light_vec);

	vec4 baseColor = texture2D(u_texture_0, v_tex_coord);
	vec4 detColor = texture2D(u_texture_1, v_tex_coord * u_material_param_0.x);
		
    float diffuse = clamp(dot(v_normal, lVec), 0.0, 1.0);

	gl_FragColor = baseColor * detColor * diffuse * vec4(u_light_color, 1.0);
}

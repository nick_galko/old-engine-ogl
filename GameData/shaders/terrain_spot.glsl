[GLSL_VERTEX_SHADER]

uniform mat4 u_mvp_transform;
uniform vec3 u_light_position;
uniform float u_light_iradius;
uniform mat4 u_spot_transform;

varying vec2 v_tex_coord;
varying vec3 v_light_vec;
varying vec3 v_s_vec;
varying vec3 v_normal;
varying vec4 v_spot_proj_coord;

void main() {
	gl_Position = u_mvp_transform * gl_Vertex; 
	v_tex_coord = gl_MultiTexCoord0.xy;

	v_normal = gl_Normal.xyz; 

	v_light_vec = (u_light_position - gl_Vertex.xyz) * u_light_iradius;
	v_s_vec = -v_light_vec;
	
	v_spot_proj_coord = u_spot_transform * gl_Vertex;
}



[GLSL_FRAGMENT_SHADER]

varying vec2 v_tex_coord;
varying vec3 v_light_vec;
varying vec3 v_s_vec;
varying vec3 v_normal;
varying vec4 v_spot_proj_coord;

uniform vec3 u_light_direction;
uniform vec3 u_light_color;
uniform sampler2D u_texture_0;
uniform sampler2D u_texture_1;
uniform sampler2D u_shadow_map;
uniform sampler2D u_spot_map;
uniform vec4 u_material_param_0;

void main() {
	float atten = 1.0;
	atten = clamp(1.0 - dot(v_light_vec, v_light_vec), 0.0, 1.0);

	float shadow = 1.0;
	
#ifdef SM_SHADOWS
	float distance = texture2DProj(u_shadow_map, v_spot_proj_coord).r + 0.01;
	shadow = float(dot(v_s_vec, v_s_vec) < distance * distance);
#endif

#ifdef VSM_SHADOWS
	float mom = texture2DProj(u_shadow_map, v_spot_proj_coord).r;

	float variance = 0.001;
	float distance = length(v_s_vec);
	float sigma = distance - mom;
	shadow = (sigma > 0.0) ? variance / (variance + sigma * sigma) : 1.0;
#endif

	if(dot(v_s_vec, u_light_direction) < 0.05) shadow = 0.0;
	
	vec4 baseColor = texture2D(u_texture_0, v_tex_coord);
	vec4 detColor = texture2D(u_texture_1, v_tex_coord * u_material_param_0.x);

	vec3 lVec = normalize(v_light_vec);
	float diffuse = clamp(dot(v_normal, lVec), 0.0, 1.0);

	gl_FragColor = baseColor * detColor * diffuse * vec4(u_light_color, 1.0) * atten * shadow * texture2DProj(u_spot_map, v_spot_proj_coord);
}

[GLSL_VERTEX_SHADER]

uniform mat4 u_mvp_transform;
uniform mat4 u_world_transform;
uniform vec3 u_view_position;

varying vec3 v_view_vec;
varying vec3 v_normal;
varying vec2 v_tex_coord;

void main() {
	gl_Position = u_mvp_transform * gl_Vertex;
	
	vec3 worldPos = u_world_transform * gl_Vertex;
	v_view_vec = worldPos - u_view_position;
	v_normal = normalize((u_world_transform * vec4(gl_Normal.xyz, 0)).xyz); 
	
	v_tex_coord = gl_MultiTexCoord0.xy;
}


[GLSL_FRAGMENT_SHADER]

varying vec2 v_tex_coord;
varying vec3 v_view_vec;
varying vec3 v_normal;

uniform vec3 u_light_color;
uniform sampler2D u_texture_1;
uniform samplerCube u_texture_2;

void main() {
	vec3 normal = 2.0 * texture2D(u_texture_1, v_tex_coord).xyz - 1.0;
	vec3 env_coord = reflect(v_view_vec, v_normal);
	gl_FragColor = textureCube(u_texture_2, env_coord + normal);
}
[GLSL_VERTEX_SHADER]

uniform mat4 u_mvp_transform;

varying vec3 v_sky_coord;

void main() {
	gl_Position = u_mvp_transform * gl_Vertex;
	v_sky_coord = gl_Vertex.xyz;	
}


[GLSL_FRAGMENT_SHADER]

varying vec3 v_sky_coord;
uniform samplerCube u_texture_0;

void main() {
	gl_FragColor = textureCube(u_texture_0, v_sky_coord);
}
[GLSL_VERTEX_SHADER]

uniform mat4 u_mvp_transform;
uniform mat4 u_viewport_transform;
uniform vec3 u_view_position;

varying vec3 v_view_vec;
varying vec2 v_tex_coord;
varying vec4 v_proj_coord;

void main() {
	gl_Position = u_mvp_transform * gl_Vertex; 
	v_tex_coord = gl_MultiTexCoord0.xy;
	
	v_view_vec = gl_Vertex.xyz - u_view_position;
	v_proj_coord = u_viewport_transform * gl_Vertex;
}


[GLSL_FRAGMENT_SHADER]

varying vec2 v_tex_coord;
varying vec4 v_proj_coord;
varying vec3 v_view_vec;

uniform sampler2D u_viewport_map;
uniform sampler2D u_reflection_map;
uniform sampler2D u_texture_0;
uniform sampler2D u_texture_1;

uniform float u_time;

void main() {
	vec3 noise = texture2D(u_texture_0, v_tex_coord + vec2(u_time, u_time) * 0.00005).xyz * 0.5;
	vec3 n = 2.0 * texture2D(u_texture_1, v_tex_coord + vec2(u_time, u_time) * 0.00005).xyz - 1.0;
	vec3 normal = vec3(n.x, n.z, n.y);
	
	vec4 refrColor = texture2DProj(u_viewport_map, v_proj_coord + vec4(noise, 0.0));
	
	vec4 reflColor = vec4(1.0, 1.0, 1.0, 1.0);
	float fresnel = 0.0;
	
#ifdef REFLECTIONS
	fresnel = 1.0 - abs(dot(normal, normalize(v_view_vec)));
	reflColor = texture2DProj(u_reflection_map, v_proj_coord + vec4(noise, 0.0));
#endif
	
	gl_FragColor = mix(refrColor, reflColor, fresnel);
}
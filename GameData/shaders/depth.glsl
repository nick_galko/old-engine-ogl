[GLSL_VERTEX_SHADER]

uniform mat4 u_mvp_transform;
uniform mat4 u_world_transform;
uniform vec3 u_light_position;
uniform float u_light_iradius;

varying vec3 v_light_vec;

void main() {
	gl_Position = u_mvp_transform * gl_Vertex; 
	
	vec4 worldPos = u_world_transform * gl_Vertex;
	v_light_vec = u_light_iradius * (u_light_position - worldPos.xyz);
}


[GLSL_FRAGMENT_SHADER]

varying vec3 v_light_vec;

void main() {
	float depth = length(v_light_vec);
	
	gl_FragColor = vec4(depth, 0, 0, 0);
}
